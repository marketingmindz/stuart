<?php

/* Template Name: Webhook */


/* +++++++++++++++++ Call API for job quote  +++++++++++++++++++++++ */


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.stuart.com/v1/jobs/quotes/types");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

curl_setopt($ch, CURLOPT_POST, TRUE);

//curl_setopt($ch, CURLOPT_POSTFIELDS, "transportTypeIds=2,3&destination=5 rue d'edimbourg 75008 paris&destinationContactCompany=John Doe Inc.&destinationContactEmail=vinod@marketingmindz.in&destinationContactFirstname=John&destinationContactLastname=Doe&destinationContactPhone=+33628046019&origin=29 rue de Rivoli 75004 Paris&originContactPhone=+33678374859");
curl_setopt($ch, CURLOPT_POSTFIELDS, "transportTypeIds=2&destination=1 Rue du Pont Neuf, 75001 Paris&destinationContactCompany=John Doe Inc.&destinationContactEmail=vinod@marketingmindz.in&destinationContactFirstname=John&destinationContactLastname=Doe&destinationContactPhone=+33628046019&origin=29 rue de Rivoli 75004 Paris&originContactPhone=+33678374859");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Content-Type: application/x-www-form-urlencoded",
  "Authorization: Bearer b76e7295dd8dca9c3b7b91d3a651a98ce74809c9a6d26e4a8f27595c378fadb2"
));

$quote_response = curl_exec($ch);
curl_close($ch);

$quote_response_decoded = json_decode($quote_response, true);

$i = 0;
$quote_id = '';
foreach ($quote_response_decoded as $key => $value) {
	# code...
	if($i == 0){
		$quote_id = $value['id'];
    $finalAmount = $value['finalAmount'];
    
	}

	$i++;
}
echo $quote_id . ' ' . $finalAmount;
/* echo '<pre>';
print_r($quote_response_decoded);
echo '</pre>'; */ 



/* +++++++++++++++++ Call API for job Create  +++++++++++++++++++++++ */

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.stuart.com/v1/jobs");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, "jobQuoteId=" . $quote_id);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Content-Type: application/x-www-form-urlencoded",
  "Authorization: Bearer b76e7295dd8dca9c3b7b91d3a651a98ce74809c9a6d26e4a8f27595c378fadb2"
));

$response = curl_exec($ch);
curl_close($ch);
$response_decoded = json_decode($response, true);

$job_id = $response_decoded['id'];
$job_createdAt = $response_decoded['createdAt'];
$job_updatedAt = $response_decoded['updatedAt'];
$job_finalJobPrice = $response_decoded['finalJobPrice']['finalAmount'];

echo $job_id . ' ---- ' . $job_createdAt . ' ---- ' . $job_updatedAt . ' ---- ' . $job_finalJobPrice;
echo "<br>";
echo $transportType = $response_decoded['transportType']['name'];
echo "<br>";
echo $distance = $response_decoded['finalJobPrice']['distance'];
echo "<br>";
echo $dest_street = $response_decoded['destinationPlace']['address']['street'];
echo "<br>";
echo $dest_postcode = $response_decoded['destinationPlace']['address']['postcode'];
echo "<br>";
echo $destination = $dest_street . ", " . $dest_postcode;
echo "<br>";
echo $trackingUrl = $response_decoded['trackingUrl'];
echo "<br>";

echo '<pre>';
print_r($response_decoded);
echo '</pre>';



/* +++++++++++++++++ Call API for job Place  +++++++++++++++++++++++ */

/*$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.stuart.com/v1/places");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

curl_setopt($ch, CURLOPT_POST, TRUE);

curl_setopt($ch, CURLOPT_POSTFIELDS, "placeTypeId=2&addressStreet=29 rue de Rivoli 75004 Paris&contactCompany=My Company&comment=code: 1234, 3rd floor, front door");

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Content-Type: application/x-www-form-urlencoded",
  "Authorization: Bearer b76e7295dd8dca9c3b7b91d3a651a98ce74809c9a6d26e4a8f27595c378fadb2"
));

$response = curl_exec($ch);
curl_close($ch);

$response_decoded = json_decode($response);
echo '<pre>';
print_r($response_decoded);
echo '</pre>';*/



/*stdClass Object
(
    [access_token] => b76e7295dd8dca9c3b7b91d3a651a98ce74809c9a6d26e4a8f27595c378fadb2
    [token_type] => bearer
    [expires_in] => 2592000
    [scope] => api
    [created_at] => 1490873528
)*/

/*$data = array(
"client_id"  => "ba8cd9d0353b561aef6a9d94750658c641d2784e4d27ac00ac4a28bdc3674983",
"client_secret" => "1c5ea3c191301881815c703986a255742bd397c19c7721dca9bedabd6912c23b",
"scope" => "api",
"grant_type" => "client_credentials",
);


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.stuart.com/oauth/token");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

curl_setopt($ch, CURLOPT_POST, TRUE);

curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

$response = curl_exec($ch);
curl_close($ch);

$response_decoded = json_decode($response);
echo '<pre>';
print_r($response_decoded);
echo '</pre>';*/



/*$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "https://sandbox-api.stuart.com/v1/clients/104701/jobs/46906/cancel");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

curl_setopt($ch, CURLOPT_POST, TRUE);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  "Authorization: Bearer b76e7295dd8dca9c3b7b91d3a651a98ce74809c9a6d26e4a8f27595c378fadb2"
));

$response = curl_exec($ch);
curl_close($ch);

var_dump($delete_response);
$delete_response_decoded = json_decode($delete_response);
echo '<pre>';
print_r($delete_response_decoded);
echo '</pre>';*/


