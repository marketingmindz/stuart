<?php
/**
 * Template Name: Speakers
 * Description: Page template with a content container and right sidebar.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
$request = parse_url($_SERVER['REQUEST_URI']);
$path = $request["path"];
$result = rtrim(str_replace(basename($_SERVER['SCRIPT_NAME']), '', $path), '/');

header("Location: " . get_bloginfo('wpurl') . '/#' . $result . '/');
die();
// EOF