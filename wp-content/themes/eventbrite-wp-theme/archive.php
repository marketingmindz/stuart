<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>

    <?php if (have_posts()) :
    // Queue the first post.
    the_post(); ?>

    <div class="container">

        <div class="row content">
            <div class="col-sm-12">

					<h1 class="page-title"><?php
					if (is_day()) {
						printf(__('Daily Archives: %s', 'eventbrite_wp'), '<span>' . get_the_date() . '</span>');
					} elseif (is_month()) {
						printf(
							__('Monthly Archives: %s', 'eventbrite_wp'),
							'<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'eventbrite_wp')) . '</span>'
							);
					} elseif (is_year()) {
						printf(
							__('Yearly Archives: %s', 'eventbrite_wp'),
							'<span>' . get_the_date(_x('Y', 'yearly archives date format', 'eventbrite_wp')) . '</span>'
						);
					} elseif (is_tag()) {
						printf(__('Tag Archives: %s', 'eventbrite_wp'), '<span>' . single_tag_title('', false) . '</span>');
						// Show an optional tag description
						$tag_description = tag_description();
						if ($tag_description) {
							echo apply_filters(
								'tag_archive_meta',
								'<div class="tag-archive-meta">' . $tag_description . '</div>'
							);
						}
					} elseif (is_category()) {
						printf(
							__('Category Archives: %s', 'eventbrite_wp'),
							'<span>' . single_cat_title('', false) . '</span>'
						);
						// Show an optional category description
						$category_description = category_description();
						if ($category_description) {
							echo apply_filters(
								'category_archive_meta',
								'<div class="category-archive-meta">' . $category_description . '</div>'
								);
						}
					} else {
					_e('Blog Archives', 'eventbrite_wp');
					}
					?></h1>
                <?php
                // Rewind the loop back
                    rewind_posts();
                ?>

                <?php while (have_posts()) : the_post(); ?>
                    <div <?php post_class(); ?>>
                        <h3 class="index-card__title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>

                        <p class="meta"><?php echo eventbrite_wp_posted_on();?></p>

                        <div class="row">
                            <?php // Post thumbnail conditional display.
                            if ( eventbrite_wp_autoset_featured_img() !== false ) : ?>
                                <div class="col-sm-4">
                                    <a href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">
                                        <?php echo eventbrite_wp_autoset_featured_img(); ?>
                                    </a>
                                </div>
                                <div class="col-sm-8">
                            <?php else : ?>
                                <div class="col-sm-12">
                            <?php endif; ?>
                                    <?php the_excerpt(); ?>
                       				 <p><a class="btn btn-primary btn-md" href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">Read More</a></p>
                                </div>
                        </div><!-- /.row -->

                        <hr/>
                    </div><!-- /.post_class -->
                <?php endwhile; ?>

				<?php if (function_exists('wp_pagenavi')): ?>
					<?php wp_pagenavi(); ?>
				<?php else: ?>
					<?php eventbrite_wp_content_nav('nav-below');?>
				<?php endif ?>

           	</div>
        </div>
    </div>
<?php endif; ?>

<?php get_footer();










