<?php
/**
 * Default Post Template
 * Description: Post template with a content container and right sidebar.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>
<?php while (have_posts()) : the_post(); ?>

	<div class="container">

	    <div class="row content">
	        <div class="col-sm-10 col-sm-offset-1">	
	          	<h1 class="single-blog__title"><?php the_title();?></h1>

	            <p class="meta"><?php echo eventbrite_wp_posted_on();?></p>
	            <?php the_content(); ?>
	            <?php the_tags('<p>Tags: ', ', ', '</p>'); ?>
	            <hr/>

				<?php if (function_exists('wp_pagenavi')): ?>
					<?php wp_pagenavi(); ?>
				<?php else: ?>
					<?php eventbrite_wp_content_nav('nav-below');?>
				<?php endif ?>
	            
	            <?php comments_template(); ?>

	        </div><!-- /.col-sm-8 -->
	   
	   		<div class="col-sm-4">
	   			<?php get_sidebar('blog'); ?>
	   		</div>

	    </div><!-- /.row .content -->

	</div>

<?php endwhile; // end of the loop. ?>

<?php 

get_footer();

// EOF


