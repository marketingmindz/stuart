<?php
/**
 * The template for displaying Author Archive pages.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>

<?php if (have_posts()) :
    // Queue the first post.
    the_post(); ?>

	<div class="container">

		<div class="row content">
			<div class="col-sm-12">
				<h1 class="page-title author"><?php printf(
					__('Author Archives: %s', 'eventbrite_wp'),
					'<span class="vcard"><a class="url fn n" href="' . get_author_posts_url(
					get_the_author_meta("ID")
					) . '" title="' . esc_attr(get_the_author()) . '" rel="me">' . get_the_author() . '</a></span>'
				); ?></h1>
                <?php
                // Rewind the loop back
                    rewind_posts();
                ?>

                <?php while (have_posts()) : the_post(); ?>
                    <div <?php post_class(); ?>>
                       <h3 class="index-card__title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>

                        <p class="meta"><?php echo eventbrite_wp_posted_on();?></p>

                        <div class="row">
                            <?php // Post thumbnail conditional display.
                            if ( eventbrite_wp_autoset_featured_img() !== false ) : ?>
                                <div class="col-sm-4">
                                    <a href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">
                                        <?php echo eventbrite_wp_autoset_featured_img(); ?>
                                    </a>
                                </div>
                                <div class="col-sm-8">
                            <?php else : ?>
                                <div class="col-sm-12">
                            <?php endif; ?>
                                    <?php the_excerpt(); ?>
                                    <p><a class="btn btn-primary btn-md" href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">Read More</a></p>

                                </div>
                        </div><!-- /.row -->

                        <hr/>
                    </div><!-- /.post_class -->
                   <?php endwhile; ?>

					<?php if (function_exists('wp_pagenavi')): ?>
						<?php wp_pagenavi(); ?>
					<?php else: ?>
						<?php eventbrite_wp_content_nav('nav-below');?>
					<?php endif ?>
			</div>
		</div>
	</div><!-- /.container -->
<?php endif; ?>
<?php get_footer(); 



