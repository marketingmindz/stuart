<?php
/**
 * Right Sidebar displayed on post and blog templates.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
?>
<?php
if (function_exists('dynamic_sidebar')) {
    dynamic_sidebar("sidebar-posts");
} ?>
