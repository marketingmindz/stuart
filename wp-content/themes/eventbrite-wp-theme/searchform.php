<form role="search" method="get" id="searchform" class="searchform" action="<?php bloginfo('wpurl'); ?>">
	<div class="form-group">
		<label class="hide screen-reader-text" for="s">Search for:</label>
		<input placeholder="Search" class="form-control" type="text" value="" name="s" id="s">
	</div>

		<input class="btn btn-secondary" type="submit" id="searchsubmit" value="Search">
</form>