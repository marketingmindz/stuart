<?php
/**
 * Search Results Template
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>
<div class="container">

		<?php if (have_posts()) : ?>
			<h1 class="post-title"><?php printf( __('Search Results for: %s', 'eventbrite_wp'),'<span>' . get_search_query() . '</span>'); ?></h1>


			<?php while (have_posts()) : the_post(); ?>
            <div <?php post_class(); ?>>
                
				<h3><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>

                <p class="meta"><?php echo eventbrite_wp_posted_on();?></p>

                <div class="row">
                    <?php // Post thumbnail conditional display.
                    if ( eventbrite_wp_autoset_featured_img() !== false ) : ?>
                        <div class="col-sm-4">
                            <a href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">
                                <?php echo eventbrite_wp_autoset_featured_img(); ?>
                            </a>
                        </div>
                        <div class="col-sm-8">
                    <?php else : ?>
                        <div class="col-sm-12">
                    <?php endif; ?>
                            <?php the_excerpt(); ?>
                        </div>
                </div><!-- /.row -->

                <hr/>
            </div><!-- /.post_class -->
			<?php endwhile; ?>

		<?php else : ?>
			<h1 class="post-title"><?php _e('No Results Found', 'eventbrite_wp'); ?></h1>
            <p class="lead">
                <?php _e(
                    'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps you should try again with a different search term.',
                    'eventbrite_wp'); ?>
            </p>
            <div class="well">
                <?php get_search_form(); ?>
            </div><!--/.well -->
		<?php endif;?>

		<?php eventbrite_wp_content_nav('nav-below'); ?>

</div>

<?php get_footer();

// EOF





