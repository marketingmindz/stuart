<?php
/**
 * Description: Page template with a content container and right sidebar.
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>
<?php while (have_posts()) : the_post(); 
	// Get all custom fields
	// $acf_fields = get_fields();
?>
<div class="container">
	<div class="page-content">
		<h1 class="page-title"><?php the_title();?></h1>
		<?php the_content(); ?>
	</div>
</div><!--/.container -->
<?php endwhile; // end of the loop.

get_footer();

// EOF