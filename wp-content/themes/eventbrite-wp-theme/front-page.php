<?php
/**
 * Description: Home Page
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header();

while (have_posts()) : the_post();


# Get child pages of home
$children_of_home = get_pages( 
	array( 
		'child_of' => $post->ID,
		'sort_column' => 'menu_order',
	)
);

# If there are children
if ( !empty( $children_of_home)) {

	$section_i = 0;

	# Loop through child pages
	foreach ($children_of_home as $page){

		$page_id = $page->ID;
		# Get the template for this page
		$page_template = get_page_template_slug( $page_id );

			# Get the ACF fields for this page
			$page_fields = get_fields( $page_id);

			# Output content for the correct template
			switch ( $page_template) {

				# Countdown Section
				case 'page-templates/temp-countdown.php': 
					include('includes/snippets/snip-countdown.php');
					break;

				# Gallery Section
				case 'page-templates/temp-gallery.php': 
					include('includes/snippets/snip-gallery.php');
					break;

				# Venue Section
				case 'page-templates/temp-venue.php': 
					include('includes/snippets/snip-venue.php');
					break;

				# Sponsors Section
				case 'page-templates/temp-sponsors.php': 
					include('includes/snippets/snip-sponsors.php');
					break;

				# Contact Section
				case 'page-templates/temp-contact.php': 
					include('includes/snippets/snip-contact.php');
					break;

				# Register (default) Section
				case 'page-templates/temp-register.php': 
					include('includes/snippets/snip-register.php');
					break;

				# Speakers Section
				case 'page-templates/temp-speakers.php': 
					include('includes/snippets/snip-speakers.php');
					break;

				# Speakers Section
				case 'page-templates/temp-agenda.php': 
					include('includes/snippets/snip-agenda.php');
					break;

				# Default content
				default:
					include('includes/snippets/snip-default.php');
					break;
			}

		$section_i++;
	}
}

endwhile; // end of the loop.

get_footer();

// EOF