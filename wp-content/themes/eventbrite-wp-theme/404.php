<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>
<div class="container">

	<h1 class="page-title"><?php _e('This is Embarrassing', 'eventbrite_wp'); ?></h1>

	<p class="lead" style="text-align: center;"><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'eventbrite_wp' ); ?></p>

</div><!-- /.container -->

<?php get_footer();




