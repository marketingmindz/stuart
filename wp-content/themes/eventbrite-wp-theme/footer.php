<?php
/**
 * Default Footer
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
?>       
        <footer class="site-footer">
            <div class="container">
                <p>&copy; <?php bloginfo('name'); ?> <?php echo date('Y') ?></p>
                <?php
                if (function_exists('dynamic_sidebar')) {
                    dynamic_sidebar("footer-content");
                } ?>
            </div><!-- /container -->
            <a class="eventbrite-credit" href="http://www.eventbrite.co.uk/blog/event-wordpress-theme/" target="_blank" title="A theme by Eventbrite">A theme by Eventbrite <img src="<?php echo get_template_directory_uri();?>/assets/images/eb-logo.png" class="img-responsive"></a>
            <a class="developer-credit sr-only" href="http://whoisandywhite.com" title="WordPress development by @whoisandywhite">WordPress Theme development by @whoisandywhite</a>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>