<?php
/*
** A home for all theme specific functions
** @whoisandywhite
*/


# Hide ACF options
define( 'ACF_LITE', true );

# embed required plugins
include('require-plugins/plugins.php');

# Activation functions
include('theme-setup.php');

# Customiser functions
// include('customise/font-pairs.php');



/*
** GENERAL WORDPRESS FUNCTIONS
*/



	# filter the embed shortcode output to wrap it in an embed container
	function tdd_oembed_filter($html, $url, $attr, $post_ID) {
	    $return = '<div class="embed-container">'.$html.'</div>';
	    return $return;
	}
	add_filter( 'embed_oembed_html', 'tdd_oembed_filter', 10, 4 ) ;
	


	# is page_id a child of the "front page"
	if ( !function_exists('is_child_of_home')) {
		function is_child_of_home( $post_id){
			if ( empty( $post_id)) {
				return '';
			}
			if ( wp_get_post_parent_id( $post_id) == get_option( 'page_on_front')) {
				return true;
			} else {
				return false;
			}
		}
	}


	# Converts timestamp to human readable countdown
	if ( !function_exists('secondsToTime')) {
		function secondsToTime($seconds) {
		    $dtF = new DateTime("@0");
		    $dtT = new DateTime("@$seconds");
		    return $dtF->diff($dtT)->format('
		    	<div class="count count__days"><span>%a</span> days</div>
		    	<div class="count count__hours"><span>%h</span> hours</div>
		    	<div class="count count__minutes"><span>%i</span> minutes</div>
		    	<div class="count count__seconds"><span>%s</span> seconds</div>');
		}
	}




	#Filter available pages for the call to action link (home)
	function eb_filter_available_page_links( $field ) {

		$field['choices'] = array();

		$children_of_home = get_pages( 
			array(
				'child_of' => get_option( 'page_on_front'),
				'sort_column' => 'menu_order',
				'sort_order' => 'ASC'
			)
		);

		if( !empty( $children_of_home)) {
			foreach ($children_of_home as $child) {
				$field['choices'][$child->ID] = $child->post_title;
			}
		}

		return $field;
    
	}
	add_filter('acf/load_field/key=field_57051852f30fb', 'eb_filter_available_page_links');



	# Change placeholder text on custom post types
	function eb_change_default_title( $title ){
	    $screen = get_current_screen();

	    switch ( $screen->post_type) {
	    	case 'speaker':
	    		$title = 'Enter speaker name here';
	    		break;

	    	case 'sponsor':
	    		$title = 'Enter sponsor name here';
	    		break;
	    }
	    return $title;
	}
	add_filter( 'enter_title_here', 'eb_change_default_title' );


	# Change the "featured image" title on sponsors
	add_action('do_meta_boxes', 'eb_change_featured_image_title');
	function eb_change_featured_image_title()
	{
		# Sponsor post type
		remove_meta_box( 'postimagediv', 'sponsor', 'side' );
		add_meta_box('postimagediv', __('Sponsor Logo'), 'post_thumbnail_meta_box', 'sponsor', 'normal', 'high');
		
		# Speaker post type
		remove_meta_box( 'postimagediv', 'speaker', 'side' );
		add_meta_box('postimagediv', __('Speaker Image'), 'post_thumbnail_meta_box', 'speaker', 'normal', 'high');

	}








// EOF