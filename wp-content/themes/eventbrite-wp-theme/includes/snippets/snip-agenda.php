<?php 
# $page_fields
# $page_id 

if ( !empty( $page_fields['background_image'])) {
	$section_bg = true;
} else {
	$section_bg = false;
}
?>
<section 
	class="section section__agenda <?php if( $section_bg) echo 'has-background' ?>" 
	data-href="<?php echo get_permalink($page_id) ?>"
	<?php if ( $section_bg): ?>
		style="background-image:url(<?php echo $page_fields['background_image']['sizes']['large'] ?>);"
	<?php endif ?>>
	<div class="container">
		<div class="section__overlay section__overlay--light">
			<h2 class="section__title"><?php echo get_the_title( $page_id) ?></h2>

			<?php if ( $page_fields['group_agenda_items_by_day']): ?>
			<!-- Nav tabs -->
			<ul class="agenda-days list list-inline list-unstyled" role="tablist">
				<li role="presentation" class="active"><a class="agenda-day__button btn btn-primary btn-md" href="#day1" aria-controls="day1" role="tab" data-toggle="tab"><?php echo $page_fields['day_1_title'] ?></a></li>
				<li role="presentation"><a class="agenda-day__button btn btn-primary btn-md" href="#day2" aria-controls="day2" role="tab" data-toggle="tab"><?php echo $page_fields['day_2_title'] ?></a></li>
			</ul>
			<?php endif; ?>

			<?php 
			$args = array(
					'post_type' => 'agenda',
					'posts_per_page' => -1,
					'orderby' => 'meta_value_num',
					'meta_key' => 'start_time',
					'order' => 'asc'
				);

			# Are we displaying items with days?
			if ( $page_fields['group_agenda_items_by_day']) {

				# build an empty array for event days
				$agenda_days = array(); 

				# get the items for day 1
				$args['meta_query'] = array(
					array(
						'key'     => 'select_day',
						'value'   => '1',
					)
				);
				$agenda_days[] = get_posts( $args);

				# get the items for day 2
				$args['meta_query'] = array(
					array(
						'key'     => 'select_day',
						'value'   => '2',
					)
				);
				$agenda_days[] = get_posts( $args);

			} else {

				# get all items, ignore days
				$agenda_days = get_posts( $args);
			}


			# if there are any agenda items
			if( !empty( $agenda_days)):

				# Are we displaying by day?
				if ( $page_fields['group_agenda_items_by_day']): ?>
				<!-- Tab panes -->
				<div class="tab-content"><?php 

					$day_i = 1;
					foreach ($agenda_days as $agenda_day): ?>
						<div role="tabpanel" class="tab-pane <?php if( $day_i==1) echo 'active' ?>" id="day<?php echo $day_i ?>"><?php 

							# Include agenda list as snippet
							$list_agenda_items = $agenda_day; 

							include( 'snip-agenda-list.php');

						?></div><?php 
						$day_i++;
					endforeach; ?>

				</div><?php 

				else: # It's a single day ?>

				<div class="single-agenda"><?php 

					# Include agenda list as snippet
					$list_agenda_items = $agenda_days; 

					include( 'snip-agenda-list.php');

				?></div><?php

				endif;
			endif; ?>
		</div>
	</div>
</section>











