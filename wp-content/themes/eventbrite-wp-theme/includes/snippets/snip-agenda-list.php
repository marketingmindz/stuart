<?php 
# $list_agenda_items: array() ?>

<?php 
# If there are agenda items in this list
if ( !empty( $list_agenda_items)): ?>
<ol class="list-agenda-items list list-unstyled">
<?php 
# loop through agenda items
foreach ($list_agenda_items as $item): 
	# get agenda fields
	$item_fields = get_fields( $item);
	?>
	<li class="agenda-item">

		<?php if ( !empty( $item_fields['description'])){ ?>	
			<a href="#" class="agenda-item__link">
		<?php } ?>

			<i class="fa fa-caret-down"></i>
			<strong><?php 
				if( !empty( $item_fields['start_time'])) echo date( 'H:i A', $item_fields['start_time']) . ' - ';
				if( !empty( $item_fields['end_time'])) echo date( 'H:i A', $item_fields['end_time']) . ' : ';
				echo $item->post_title; 
			?></strong>

		<?php if ( !empty( $item_fields['description'])){ ?>	
			</a>
		<?php } ?>

		<div class="agenda-item__description"><?php echo wpautop( $item_fields['description']) ?></div>
	</li>
<?php 
endforeach; ?>
</ol>
<?php 
endif; ?>