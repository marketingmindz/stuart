<?php 
# $page_fields
# $page_id 

if ( !empty( $page_fields['venue_image'])) {
	$section_bg = true;
} else {
	$section_bg = false;
}
?>
<section 
	class="section section__venue <?php if ( $section_bg) echo "has-background"; ?>" 
	data-href="<?php echo get_permalink($page_id) ?>"
	<?php if ( $section_bg): ?>
		style="background-image:url(<?php echo $page_fields['venue_image']['sizes']['large'] ?>);"
	<?php endif ?>>
	<div class="container">
		<div class="section__overlay">
			<h2 class="section__title"><?php echo get_the_title( $page_id) ?></h2>

			<?php if ( !empty( $page_fields['venue_address'])): ?>
			<p><i class="fa fa-map-marker"></i> <?php 
				if( !empty( $page_fields['venue_name'])) {
					echo $page_fields['venue_name'] . ', ';
				}

				echo $page_fields['venue_address']['address']; ?></p>
			<?php endif; ?>

			<?php if ( !empty( $page_fields['venue_website'])
						|| !empty( $page_fields['venue_telephone_number'])): ?>
				<p>
					<?php if ( !empty( $page_fields['venue_website'])): ?>
						<i class="fa fa-link"></i> <a href="<?php echo $page_fields['venue_website'] ?>" title="<?php echo $page_fields['venue_name'] ?> website"><?php echo str_replace('http://', '', $page_fields['venue_website']) ?></a>
					<?php endif ?>

					<?php if ( !empty( $page_fields['venue_website'])
						&& !empty( $page_fields['venue_telephone_number'])) echo '&nbsp; | &nbsp; '  ?>

					<?php if ( !empty( $page_fields['venue_telephone_number'])): ?>
						<i class="fa fa-phone"></i> <?php echo $page_fields['venue_telephone_number'] ?>
					<?php endif ?>
				</p>
			<?php endif ?>

			<?php if ( !empty( $page_fields['venue_address'])): ?>
			<div class="venue__map">
				<div class="marker"
					data-lat="<?php echo $page_fields['venue_address']['lat'] ?>"
					data-lng="<?php echo $page_fields['venue_address']['lng'] ?>"></div>
			</div>
			<?php endif ?>


		</div>
	</div>
</section>
