<?php 
# $page_fields
# $page_id
?>
<section 
	class="section section__sponsors section--padding" 
	data-href="<?php echo get_permalink($page_id) ?>">
	<div class="container">
		
		<h2 class="section__title"><?php echo get_the_title( $page_id) ?></h2>

		<?php 
		# Get the sponsors
		$sponsors = get_posts( 
			array(
				'post_type' => 'sponsor',
				'posts_per_page' => -1,
			)
		);

		# if there are any sponsors
		if( !empty( $sponsors)): ?>
		<div class="sponsors">
		<?php # loop through sponsors
		foreach ($sponsors as $sponsor): 
			 # If the sponsor has a logo
            if ( has_post_thumbnail( $sponsor->ID)):
                # Get the post thumbnail
                $thumb_id = get_post_thumbnail_id( $sponsor->ID);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);

				# Get the ACF fields for this sponsor
				$sponsor_fields = get_fields( $sponsor->ID);
				?>
				<div class="sponsor__container">

					<a class="sponsor__link"
						href="#" 
						data-logo="<?php echo $thumb_url_array[0] ?>" 
						style="background-image: url(<?php echo $thumb_url_array[0] ?>);" 
						title="<?php echo $sponsor->post_title ?>"><?php 
							echo $sponsor->post_title; ?></a>

					<div class="sponsor__copy">
						<?php echo wpautop( $sponsor->post_content) ?>
					</div>

					<div class="sponsor__social-links">
						<ul class="sponsor__social list list-unstyled list-inline">
						<?php 
							if ( !empty( $sponsor_fields['website_url'])) {
								echo '<li><a href="'.$sponsor_fields['website_url'].'" title="Website"><i class="fa fa-globe"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['facebook_url'])) {
								echo '<li><a href="'.$sponsor_fields['facebook_url'].'" title="Facebook"><i class="fa fa-facebook"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['twitter_url'])) {
								echo '<li><a href="'.$sponsor_fields['twitter_url'].'" title="Twitter"><i class="fa fa-twitter"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['linked_in_url'])) {
								echo '<li><a href="'.$sponsor_fields['linked_in_url'].'" title="Linked In"><i class="fa fa-linkedin"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['instagram_url'])) {
								echo '<li><a href="'.$sponsor_fields['instagram_url'].'" title="Instagram"><i class="fa fa-instagram"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['google_plus_url'])) {
								echo '<li><a href="'.$sponsor_fields['google_plus_url'].'" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['blogger_url'])) {
								echo '<li><a href="'.$sponsor_fields['blogger_url'].'" title="Blogger"><i class="fa fa-link"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['pinterest_url'])) {
								echo '<li><a href="'.$sponsor_fields['pinterest_url'].'" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>';	
							}

							if ( !empty( $sponsor_fields['tumblr_url'])) {
								echo '<li><a href="'.$sponsor_fields['tumblr_url'].'" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>';	
							}

						?>
						</ul>
					</div>

				</div><?php 
			endif;
		endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</section>



<div id="sponsor-modal" class="sponsor-modal mfp-hide">
	<div class="modal-loading">
		<img class="modal-loading__gif" src="<?php echo get_template_directory_uri();?>/assets/images/ajax-loader.svg">
	</div>
	<div class="sponsor-modal__content">
		<div class="row">
			<div class="col-xs-3">
				<img class="sponsor-modal__logo img-responsive" src="http://placehold.it/500x400" />
				<div class="sponsor-modal__social-links"></div>
			</div>
			<div class="col-xs-9">
				<h3 class="sponsor-modal__name"></h3>
				<div class="sponsor-modal__copy"></div>
			</div>
		</div>
	</div>
</div>



