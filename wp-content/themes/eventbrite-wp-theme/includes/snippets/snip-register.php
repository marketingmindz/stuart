<?php 
# $page_fields
# $page_id

$page = get_pages( array(
		'include' => $page_id
	)
);
?>
<section 
	class="section section__register section--padding" 
	data-href="<?php echo get_permalink($page_id) ?>">
	<div class="container">
		<h2 class="section__title"><?php echo $page[0]->post_title ?></h2>
		<div class="register__copy">
			<?php echo wpautop( $page[0]->post_content) ?>
		</div>

		<?php if ( $page_fields['registration_type'] === 'eventbrite'): ?>
			<?php echo $page_fields['ticket_widget_code']; ?>
		<?php else: ?>
			<?php if ( !empty( $page_fields['price_point'])
						|| !empty( $page_fields['description'])
						|| !empty( $page_fields['call_to_action_button_title'])
						|| !empty( $page_fields['call_to_action_link'])): ?>
				<div class="register-unit">
					<?php if ( !empty( $page_fields['price_point'])): ?>
						<div class="register-unit__header">

							<h4 class="register__price-point"><?php 
								switch ( $page_fields['currency']) {
									case 'pound':
										echo '&pound;';
										break;
									case 'dollar':
										echo '&#36;';
										break;
								}

								echo $page_fields['price_point'];
							?></h4>
						</div>
					<?php endif ?>

					<?php if ( !empty( $page_fields['description'])
						|| !empty( $page_fields['call_to_action_button_title'])
						|| !empty( $page_fields['call_to_action_link'])): ?>
						<div class="register-unit__body">
							<?php echo wpautop( $page_fields['description']) ?>
							<?php if ( !empty( $page_fields['call_to_action_button_title'])
										&& !empty( $page_fields['call_to_action_link'])): ?>
								<a class="register__button btn btn-block btn-md" href="<?php echo $page_fields['call_to_action_link'] ?>" title="<?php echo $page_fields['call_to_action_button_title'] ?>"><?php echo $page_fields['call_to_action_button_title'] ?></a>
							<?php endif ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endif ?>
		<?php endif ?>
	</div>
</section>
