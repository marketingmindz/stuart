<?php # <section> is opened and closed in header.php ?>
<div class="container">
    <div class="hero__container">

        <?php 
        # Hero Logo
        if ( !empty( $acf_fields['event_logo'])): ?>
            <img class="hero__logo" src="<?php echo $acf_fields['event_logo']['sizes']['medium'] ?>">
        <?php endif ?>

        <h1 class="hero__title"><?php the_title() ?></h1>
        <div class="hero__copy"><?php echo wpautop( $post->post_content) ?></div>

        <?php 
        # Call to action button
        if ( $acf_fields['call_to_action_button']):

            # switch the link type
            switch ( $acf_fields['call_to_action_link_type']) {

                # external link
                case 'external': ?>
                <a class="hero__btn btn btn-lg" href="<?php echo $acf_fields['call_to_action_button_url'] ?>" title="<?php echo $acf_fields['call_to_action_button_title'] ?>"><?php echo $acf_fields['call_to_action_button_title'] ?></a>
                <?php
                    break;
                
                # scroll to section on page
                case 'internal': ?>
                <a class="hero__btn btn btn-lg" href="<?php echo get_the_permalink( $acf_fields['scroll_to_section']) ?>" title="<?php echo $acf_fields['call_to_action_button_title'] ?>"><?php echo $acf_fields['call_to_action_button_title'] ?></a>
                <?php
                    break;
            } // end switch
        endif; // endif ?>

        <?php 
        # Lead sponsor
        if ( $acf_fields['lead_sponsor']): 
            $lead_sponsor = $acf_fields['select_lead_sponsor'];
            ?>
            <?php 
            # Intro sentence
            if (!empty( $acf_fields['lead_sponsor_title'])): ?>
                <p class="hero__lead-sponsor-title"><?php echo $acf_fields['lead_sponsor_title'] ?></p>
            <?php endif ?>
            <?php 
            # If the selected lead sponsor has a logo
            if ( has_post_thumbnail( $lead_sponsor)) {
                # Get the post thumbnail
                $thumb_id = get_post_thumbnail_id( $lead_sponsor->ID);

                if ( !empty( $thumb_id)) {
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
                    echo '<p><img src="'.$thumb_url_array['0'].'" class="img-responsive hero__lead-sponsor-logo" /></p>';
                }
            } else { 
                # Else show the post title
                echo '<h5 class="hero__lead-sponsor-name">' . $lead_sponsor->post_title . '</h5>';
            } ?>
        <?php endif ?>
    </div>
</div>