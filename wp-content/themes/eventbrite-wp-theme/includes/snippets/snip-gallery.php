<?php 
# $page_fields
# $page_id 
?>
<section 
	class="section section__gallery" 
	data-href="<?php echo get_permalink($page_id) ?>">

	<div class="gallery__container">
		<div class="gallery__row">
			<?php
			# if there are any images
			if ( !empty( $page_fields)){
				foreach ($page_fields as $image) {
					# if there is an image in this field
					if ( !empty( $image)) { ?>
						<div class="gallery__col">
							<a class="gallery__image" 
								href="<?php echo $image['sizes']['large'] ?>"
								style="background-image:url(<?php echo $image['sizes']['medium'] ?>)"></a>
						</div>
						<?php
					}
				}
			} ?>

		</div>
	</div>
</section>
