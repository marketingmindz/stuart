<?php 
# $page_fields
# $page_id
?>
<section 
	class="section section__speakers section--padding" 
	data-href="<?php echo get_permalink($page_id) ?>">
	<div class="container">
		
		<h2 class="section__title"><?php echo get_the_title( $page_id) ?></h2>

		<?php 
		# Get the speakers
		$speakers = get_posts( 
			array(
				'post_type' => 'speaker',
				'posts_per_page' => -1,
			)
		);

		# if there are any sponsors
		if( !empty( $speakers)): ?>
		<div class="speakers">
		<?php # loop through speakers
		foreach ($speakers as $speaker): 

			$speaker_fields = get_fields( $speaker->ID);

			 # If the speaker has a logo
            if ( has_post_thumbnail( $speaker->ID)) {
                # Get the post thumbnail
                $thumb_id = get_post_thumbnail_id( $speaker->ID);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
				$speaker_image = $thumb_url_array[0];
			} else {
				$speaker_image = get_template_directory_uri() .'/assets/images/anon.gif';
			}
			?>
			<div class="speaker__container">
				<a class="speaker__link"
					href="#" 
					title="<?php echo $speaker->post_title ?>">
				
					<img src="<?php echo $speaker_image ?>" class="speaker__image img-responsive">
					
					<p><strong class="speaker__name"><?php echo $speaker->post_title ?></strong></p>
				</a>

				<p><i class="fa fa-angle-down"></i></p>

				<?php if ( !empty( $speaker_fields['job_title'])): ?>
					<p class="speaker__job-title"><strong><?php echo $speaker_fields['job_title']; ?></strong></p>
				<?php endif; ?>

				<?php if ( !empty( $speaker_fields['company_name'])): ?>
					<p class="speaker__company-name"><?php echo $speaker_fields['company_name']; ?></p>
				<?php endif; ?>

				<ul class="speaker__social list list-unstyled list-inline">
				<?php 

					$social_i = 0;

					if ( !empty( $speaker_fields['facebook_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['facebook_url'].'" title="Facebook"><i class="fa fa-facebook"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['twitter_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['twitter_url'].'" title="Twitter"><i class="fa fa-twitter"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['linked_in_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['linked_in_url'].'" title="Linked In"><i class="fa fa-linkedin"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['instagram_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['instagram_url'].'" title="Instagram"><i class="fa fa-instagram"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['google_plus_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['google_plus_url'].'" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['blogger_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['blogger_url'].'" title="Blogger"><i class="fa fa-link"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['pinterest_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['pinterest_url'].'" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>';
						}
					}

					if ( !empty( $speaker_fields['tumblr_url'])) {
						$social_i++;
						if ( $social_i < 4) {
							echo '<li><a href="'.$speaker_fields['tumblr_url'].'" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>';
						}
					}

				?>
				</ul>

				<div class="speaker__copy"><?php echo wpautop( $speaker->post_content) ?></div>

			</div><?php 
		endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
</section>



<div id="speaker-modal" class="speaker-modal mfp-hide">
	<div class="modal-loading">
		<img class="modal-loading__gif" src="<?php echo get_template_directory_uri();?>/assets/images/ajax-loader.svg">
	</div>
	<div class="speaker-modal__content">
		<div class="row">
			<div class="col-xs-3">
				<img class="speaker-modal__logo img-responsive" src="http://placehold.it/500x400" />
			</div>
			<div class="col-xs-9">
				<h3 class="speaker-modal__name"></h3>
				<p><span class="speaker-modal__role"></span> / <span class="speaker-modal__company"></span></p>
				<div class="speaker-modal__copy"></div>
			</div>
		</div>
	</div>
</div>









