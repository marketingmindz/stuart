<?php 
# $page_fields
# $page_id

$page = get_pages( array(
		'include' => $page_id
	)
);
?>
<section 
	class="section section__default section--padding" 
	data-href="<?php echo get_permalink($page_id) ?>">
	<div class="container">
		<h2 class="section__title"><?php echo $page[0]->post_title ?></h2>
		<div class="register__copy">
			<?php echo wpautop( $page[0]->post_content) ?>
		</div>
	</div>
</section>