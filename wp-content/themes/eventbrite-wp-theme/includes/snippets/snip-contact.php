<?php 
# $page_fields
# $page_id 

if ( !empty( $page_fields['background_image'])) {
	$section_bg = true;
} else {
	$section_bg = false;
}
?>
<section 
	class="section section__contact <?php if ( $section_bg) echo 'has-background' ?>" 
	data-href="<?php echo get_permalink($page_id) ?>"
	<?php if ( $section_bg): ?>
		style="background-image:url(<?php echo $page_fields['background_image']['sizes']['large'] ?>);"
	<?php endif ?>>
	<div class="container">
		<div class="section__overlay">
			<h2 class="section__title"><?php echo get_the_title( $page_id) ?></h2>

			<?php if ( has_post_thumbnail( $page_id)):
                # Get the post thumbnail
                $thumb_id = get_post_thumbnail_id( $page_id);
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'theme-square', true); ?>
				<p><img class="img-responsive img-circle contact__image" src="<?php echo $thumb_url_array[0] ?>" class="img-responsive" /></p>
			<?php endif; ?>

			<?php if ( !empty( $page_fields['contact_name'])): ?>
				<h3 class="contact__name"><?php echo $page_fields['contact_name'] ?></h3>
			<?php endif ?>

			<?php if ( !empty( $page_fields['contact_address'])): ?>
				<p class="contact__address"><?php echo $page_fields['contact_address'] ?></p>
			<?php endif ?>

			<?php if ( !empty( $page_fields['email_address'])
					|| !empty( $page_fields['website_address'])
					|| !empty( $page_fields['telephone_number'])): ?>
				<p class="contact__details"><?php 
				if ( !empty( $page_fields['email_address'])) {
					echo '<i class="fa fa-envelope"></i> <a href="mailto:'.$page_fields['email_address'].'" target="_blank">'.$page_fields['email_address'].'</a> ';
				}

				echo '<span class="clearfix visible-xs"></span>';

				if ( !empty( $page_fields['website_address'])) {
					echo '<i class="fa fa-link"></i> <a href="'.$page_fields['website_address'].'">'.$page_fields['website_address'].'</a> ';
				}

				echo '<span class="clearfix visible-xs"></span>';

				if ( !empty( $page_fields['telephone_number'])) {
					echo '<i class="fa fa-phone"></i> '.$page_fields['telephone_number'];
				}
				?></p>
			<?php endif ?>


			<ul class="contact__social list list-unstyled list-inline">
			<?php 

				if ( !empty( $page_fields['facebook_url'])) {
					echo '<li><a href="'.$page_fields['facebook_url'].'" title="Facebook"><i class="fa fa-facebook"></i></a></li>';
				}

				if ( !empty( $page_fields['twitter_url'])) {
					echo '<li><a href="'.$page_fields['twitter_url'].'" title="Twitter"><i class="fa fa-twitter"></i></a></li>';
				}

				if ( !empty( $page_fields['linked_in_url'])) {
					echo '<li><a href="'.$page_fields['linked_in_url'].'" title="Linked In"><i class="fa fa-linkedin"></i></a></li>';
				}

				if ( !empty( $page_fields['instagram_url'])) {
					echo '<li><a href="'.$page_fields['instagram_url'].'" title="Instagram"><i class="fa fa-instagram"></i></a></li>';
				}

				if ( !empty( $page_fields['google_plus_url'])) {
					echo '<li><a href="'.$page_fields['google_plus_url'].'" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>';
				}

				if ( !empty( $page_fields['blogger_url'])) {
					echo '<li><a href="'.$page_fields['blogger_url'].'" title="Blogger"><i class="fa fa-link"></i></a></li>';
				}

				if ( !empty( $page_fields['pinterest_url'])) {
					echo '<li><a href="'.$page_fields['pinterest_url'].'" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>';
				}

				if ( !empty( $page_fields['tumblr_url'])) {
					echo '<li><a href="'.$page_fields['tumblr_url'].'" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>';
				}

			?>
			</ul>


		</div>
	</div>
</section>



