<?php 
# calculate difference
$date_diff = $page_fields['event_date_and_time'] - time(); ?>
<?php if ($date_diff > 0): ?>	
<section 
	class="section section__countdown" 
	data-href="<?php echo get_permalink($page_id) ?>">

	<div class="countdown">
		<?php echo secondsToTime( $date_diff) ?>
	</div>
</section>
<?php endif ?>