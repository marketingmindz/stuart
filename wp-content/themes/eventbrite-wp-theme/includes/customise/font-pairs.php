<?php 
function eventbrite_wp_register_customiser( $wp_customize ) {
  //All our sections, settings, and controls will be added here

	$colors = array();
	$colors[] = array(
	  'slug'=>'content_text_color', 
	  'default' => '#333',
	  'label' => __('Content Text Color', 'Ari')
	);
	$colors[] = array(
	  'slug'=>'content_link_color', 
	  'default' => '#88C34B',
	  'label' => __('Content Link Color', 'Ari')
	);
	foreach( $colors as $color ) {
	  // SETTINGS
	  $wp_customize->add_setting(
	    $color['slug'], array(
	      'default' => $color['default'],
	      'type' => 'option', 
	      'capability' => 
	      'edit_theme_options'
	    )
	  );
	  // CONTROLS
	  $wp_customize->add_control(
	    new WP_Customize_Color_Control(
	      $wp_customize,
	      $color['slug'], 
	      array('label' => $color['label'], 
	      'section' => 'colors',
	      'settings' => $color['slug'])
	    )
	  );
	}
}
add_action( 'customize_register', 'eventbrite_wp_register_customiser' );