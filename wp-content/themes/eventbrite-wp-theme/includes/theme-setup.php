<?php 
# Include function files
include( 'theme-setup/create-pages.php');
include( 'theme-setup/create-menus.php');

# Include custom post types
include( 'theme-setup/cpt.speakers.php');
include( 'theme-setup/cpt.sponsors.php');
include( 'theme-setup/cpt.agenda.php');

# Include ACF fields
include( 'theme-setup/acf.fields.php');

# Theme Setup
add_action('after_switch_theme', 'eb_theme_setup');
function eb_theme_setup(){

	# Create required pages
	eb_create_pages_on_activation();

	# Create Main Menu
	eb_create_main_menu_on_activation();

	# Create Footer Menu
	eb_create_footer_menu_on_activation();

	global $pagenow;

	if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {

		wp_redirect(admin_url("edit.php?post_type=page")); // Your admin page URL
		
	}
}





