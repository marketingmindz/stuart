<?php 
# Create Eventbrite theme pages on activation
function eb_create_pages_on_activation(){


	# set all pages to draft
	$all_pages = get_posts( 
		array(
			'post_type' => 'page',
			'posts_per_page' => -1
		)
	);

	foreach ($all_pages as $page) {
		$args = array( 'ID' => $page->ID, 'post_status' => 'draft' );
		wp_update_post($args);
	}



	
	# If the front page is posts
	$show_on_front = get_option('show_on_front');
	if ( $show_on_front == 'posts') {

		# Change the "show on front" setting to page
		update_option( 'show_on_front', 'page');

	}




	# Check if a page called "home" exists
	$page_called_home = get_page_by_title( __('Home', 'eventbrite_wp'));
	if ( !empty( $page_called_home)) {
		$page_called_home = $page_called_home->ID;
	}

	# If it doesn't; create it
	if ( empty($page_called_home)) {
		$page = array(
				'post_title' =>  __('Home', 'eventbrite_wp'),
				'post_type' => 'page',
				'post_status' => 'publish',
				'post_content' => '',
				// 'page_template' => ''
			);
		$page_called_home = wp_insert_post($page); // returns ID
	} else {
		$args = array( 'ID' => $page_called_home, 'post_status' => 'publish' );
		wp_update_post($args);
	}

	# Set the page called "home" to the front page
	update_option( 'page_on_front', $page_called_home);




	# Set up pages to be created
	$pages_created = array();
	$site_pages = array(
			array(
				'title' => 'Countdown',
				'template' => 'page-templates/temp-countdown.php',
				'menu_order' => 30,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Gallery',
				'template' => 'page-templates/temp-gallery.php',
				'menu_order' => 31,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Speakers',
				'template' => 'page-templates/temp-speakers.php',
				'menu_order' => 32,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Venue',
				'template' => 'page-templates/temp-venue.php',
				'menu_order' => 33,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Sponsors',
				'template' => 'page-templates/temp-sponsors.php',
				'menu_order' => 34,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Agenda',
				'template' => 'page-templates/temp-agenda.php',
				'menu_order' => 35,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Register',
				'template' => 'page-templates/temp-register.php',
				'menu_order' => 36,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Contact',
				'template' => 'page-templates/temp-contact.php',
				'menu_order' => 37,
				'post_parent' => $page_called_home
				),
			array(
				'title' => 'Terms &amp; Conditions',
				'template' => '',
				'menu_order' => 98,
				'post_parent' => 0
				),
			array(
				'title' => 'FAQS',
				'template' => '',
				'menu_order' => 99,
				'post_parent' => 0
				),
		);

	# Create the site pages
	foreach ($site_pages as $site_page) {

		# Check if the page title exists
		$page_title_exists = get_page_by_title( $site_page['title']);

		if ( !$page_title_exists) { # Page doesn't exist
				
			$page = array(
					'post_title' =>  __( $site_page['title'], 'eventbrite_wp'),
					'post_type' => 'page',
					'post_status' => 'publish',
					'post_content' => '',
					'post_parent' => $site_page['post_parent'],
					'menu_order' => $site_page['menu_order']
				);
			$this_page_id = wp_insert_post($page); // returns ID

		} else { # Page does exist

			$this_page_id = $page_title_exists->ID;

			# Update page settings
				// menu_order
				// post_status
				// post_parent
			$args = array( 
					'ID' => $this_page_id,
					'post_status' => 'publish',
					'menu_order' => $site_page['menu_order'],
					'post_parent' => $site_page['post_parent'],
				);
			wp_update_post($args);

		}


		# Set the page template
		if ( !empty( $site_page['template'])) {
	        update_post_meta( $this_page_id, '_wp_page_template', $site_page['template'] );
		}

        # Add the ID to the $pages_created array
        $pages_created[] = $this_page_id;

	}

    # output notices
    if ( !empty( $pages_created)) {
		add_action( 'admin_notices', 'eb_pages_created_notice' );

    } else {
		add_action( 'admin_notices', 'eb_pages_error_notice' );
    }
}

# success notification
function eb_pages_created_notice() {
	?>
	<div class="updated notice">
		<p><strong><?php _e('Eventbrite', 'eventbrite_wp') ?>:</strong> <?php _e( 'The required pages have been created.', 'eventbrite_wp' ); ?></p>
	</div>
	<?php
}

# error notification
function eb_pages_error_notice() {
	?>
	<div class="error notice">
	    <p><strong><?php _e('Eventbrite', 'eventbrite_wp') ?>:</strong> <?php _e( 'There has been an error creating the required pages, please de-activate the Eventbrite theme and try again.', 'eventbrite_wp' ); ?></p>
	</div>
	<?php
}
