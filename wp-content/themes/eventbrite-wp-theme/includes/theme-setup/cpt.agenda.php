<?php 
// let's create the function for the custom type
function eb_register_agenda() { 
	// creating (registering) the custom type 
	register_post_type( 'agenda', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Agenda', 'eventbrite_wp'), /* This is the Title of the Group */
			'singular_name' => __('Agenda Item', 'eventbrite_wp'), /* This is the individual type */
			'all_items' => __('All Agenda Items', 'eventbrite_wp'), /* the all items menu item */
			'add_new' => __('Add New', 'eventbrite_wp'), /* The add new menu item */
			'add_new_item' => __('Add New Agenda Item', 'eventbrite_wp'), /* Add New Display Title */
			'edit' => __( 'Edit', 'eventbrite_wp' ), /* Edit Dialog */
			'edit_item' => __('Edit Agenda Item', 'eventbrite_wp'), /* Edit Display Title */
			'new_item' => __('New Agenda Item', 'eventbrite_wp'), /* New Display Title */
			'view_item' => __('View Agenda Item', 'eventbrite_wp'), /* View Display Title */
			'search_items' => __('Search Agenda Items', 'eventbrite_wp'), /* Search Custom Type Title */ 
			'not_found' =>  __('No Agenda Items found in the Database.', 'eventbrite_wp'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('No Agenda Items found in Trash', 'eventbrite_wp'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the section for Agenda Items', 'eventbrite_wp' ), /* Custom Type Description */
			'public' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_in_nav_menus' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'query_var' => true,
			'menu_position' => 23, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon'   => 'dashicons-schedule',
			'rewrite'	=> array( 'slug' => 'agenda-item', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title')
	 	) /* end of options */
	); /* end of register post type */
	
} 

// adding the function to the Wordpress init
add_action( 'init', 'eb_register_agenda');	
