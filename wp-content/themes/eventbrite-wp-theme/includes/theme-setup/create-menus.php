<?php
# Create main menu 
function eb_create_main_menu_on_activation(){

	# Check if the menu exists
	$menu_name = 'Eventbrite Main Menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	# If it doesn't exist, let's create it.
	if( !$menu_exists){

	    $menu_id = wp_create_nav_menu($menu_name);

		# Set up home menu item
	    $home_page_menu_id = wp_update_nav_menu_item( $menu_id, 0, array(
		        'menu-item-title' =>  __('Home'),
		        'menu-item-classes' => 'home',
		        'menu-item-url' => home_url( '/' ), 
		        'menu-item-status' => 'publish'
	        )
	    );

		# Get published page
		$home_page_id = get_option( 'page_on_front');
		$published_pages = get_pages( array(
				'sort_column' => 'menu_order'
			)
		);

		# Create menu items for each published page
		foreach ($published_pages as $menu_page) {

			# Don't add the homepage
			if ( $menu_page->ID != $home_page_id) {
				
				if ( $menu_page->post_parent == $home_page_id) {
					$menu_item_parent_id = $home_page_menu_id;
				} else {
					$menu_item_parent_id = 0;
				}

			    wp_update_nav_menu_item( $menu_id, 0, array(    
			    	'menu-item-object-id' => $menu_page->ID,
					//'menu-item-parent-id' => $menu_item_parent_id,
					'menu-item-object' => 'page',
					'menu-item-type' => 'post_type',
					'menu-item-status' => 'publish'
			        )
			    );
			}

		}
	} else {

		$menu_id = $menu_exists->term_id;

	}

	# Set the menu as the main-menu
	eb_set_menu_location( $menu_id, 'eb-main-menu');

}

# Create footer menu
function eb_create_footer_menu_on_activation(){

		# Check if the menu exists
	$menu_name = 'Eventbrite Footer Menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	# If it doesn't exist, let's create it.
	if( !$menu_exists){

	    $menu_id = wp_create_nav_menu($menu_name);

		# Get published pages, excluding homepage and children
		$home_page_id = get_option( 'page_on_front');
		$published_pages = get_pages( array(
				'sort_column' => 'menu_order',
				'exclude' => $home_page_id
			)
		);

		# Create menu items for each published page
		foreach ($published_pages as $menu_page) {

		    wp_update_nav_menu_item( $menu_id, 0, array(    
		    	'menu-item-object-id' => $menu_page->ID,
				'menu-item-object' => 'page',
				'menu-item-type' => 'post_type',
				'menu-item-status' => 'publish'
		        )
		    );
		}
	} else {

		$menu_id = $menu_exists->term_id;

	}

	# Set the menu as the main-menu
	eb_set_menu_location( $menu_id, 'eb-footer-menu');
}


# Set menu location
function eb_set_menu_location( $menu_id = '', $menu_location = ''){

	if( empty($menu_id) || empty( $menu_location))
		return;

	# Get registered menu locations
	$locations = get_theme_mod( 'nav_menu_locations' );

	if(!empty($locations)) # Locations are already set
	{

		$menu_found = false;

		# Loop through locations
	    foreach($locations as $location_id => $menu_value) 
	    {
	    	# when we find the menu we want to update
	    	if ( $menu_location == $location_id) {

	    		$menu_found = true;

	    		# Over-write the entry in the array
	    		$locations[$location_id] = $menu_id;
	    	}
	    }

	    # If we didn't find the menu we wanted in the array, add it
	    if ( !$menu_found) {
	    	$locations[$menu_location] = $menu_id;
	    }

	} else { # No locations are set

		$locations = array();
		$locations[$menu_location] = $menu_id;
	}

	# update the nav_menu_locations with the new $locations
	set_theme_mod('nav_menu_locations', $locations);

}




