<?php

# Advanced Custom Fields
include('acf.php');

# ACF time picker plugin
if ( class_exists('acf')) {
	include_once( get_template_directory() . '/includes/require-plugins/plugins/acf-date_time_picker/acf-date_time_picker.php');
}

# Search ACF plugin
include('acf-search.php');