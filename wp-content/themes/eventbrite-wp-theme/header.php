<?php
/**
 * Default Page Header
 *
 * @package WP-Bootstrap
 * @subpackage WP-Bootstrap
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/ico/favicon.png">

    <?php wp_head(); ?>

    <link href='https://fonts.googleapis.com/css?family=Oxygen:400,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Dosis:400,600,200' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<?php if (is_child_of_home( get_the_ID())) {
    $body_class = 'is-child-of-home';
} else {
    $body_class = '';
} ?>
<body <?php body_class( $body_class); ?>>

    <?php 
    # Front page only
    if ( is_front_page()): 
        # Get the ACF fields for home
        $acf_fields = get_fields(); 

        if ( !empty( $acf_fields['hero_background_image'])) {
            $hero_bg = true;
        } else {
            $hero_bg = false;
        }

    ?>
    <section 
        class="section section__intro <?php if ( $hero_bg) echo 'has-background' ?>" 
        data-href="<?php echo get_permalink( get_option('page_on_front')) ?>"
        <?php if ($hero_bg): ?>
            style="background-image:url(<?php echo $acf_fields['hero_background_image']['sizes']['large'] ?>);"
        <?php endif ?>>
    <?php endif; ?>

    	<div class="navbar navbar-default navbar-relative-top">
    		<div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
    			<div id="navbar" class="collapse navbar-collapse">
    				 <?php wp_nav_menu(
                            array(
                                'theme_location' => 'eb-main-menu',
                                'menu_class' => 'nav navbar-nav',
                                'fallback_cb' => '',
                                'menu_id' => 'main-menu',
                                'walker' => new Whoisandywhite_Walker_Nav_Menu()
                            )
                        ); ?>
    			</div><!--/.nav-collapse -->
    		</div>
    	</div>
        <!-- End Header. Begin Template Content -->


    <?php 
    # Front page only
    if ( is_front_page()): ?>
        <?php include('includes/snippets/snip-intro.php') ?>
    </section>
    <?php endif; ?>






