<?php
/**
 * Description: Default Index template to display loop of blog posts
 *
 * @package WordPress
 * @subpackage eventbrite_wp
 */
get_header(); ?>
<div class="container">
    <?php if (have_posts()) : while (have_posts()) : the_post(); 
        // get the url of the featured image
        //$thumb_id = get_post_thumbnail_id( $post->ID);
        //$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
    ?>
        <div <?php post_class(); ?>>
            <h3 class="index-card__title"><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
            
            <p class="meta">
                <?php echo eventbrite_wp_posted_on();?>
            </p>

            <div class="row">
                <?php // Post thumbnail conditional display.
                if ( eventbrite_wp_autoset_featured_img() !== false ) : ?>
                    <div class="col-sm-4">
                        <a href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">
                            <?php echo eventbrite_wp_autoset_featured_img(); ?>
                        </a>
                    </div>
                    <div class="col-sm-8">
                <?php else : ?>
                    <div class="col-sm-12">
                <?php endif; ?>
                        <?php the_excerpt(); ?>
                        <p><a class="btn btn-primary btn-md" href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">Read More</a></p>
                    </div>
            </div><!-- /.row -->

            <hr/>
        </div><!-- /.post_class -->
    <?php endwhile; endif; ?>

	<?php if (function_exists('wp_pagenavi')): ?>
		<?php wp_pagenavi(); ?>
	<?php else: ?>
		<?php eventbrite_wp_content_nav('nav-below');?>
	<?php endif ?>
</div>
</div><!--/.container -->
<?php 

get_footer();

