<div class="wrap about-wrap buddyapp">

    <?php sq_panel_header( 'changelog' ); ?>

    <div class="changelog">
        <h3><span class="dashicons dashicons-admin-tools"></span> <?php esc_html_e( "Latest features & updates:", "buddyapp" );?></h3>
        <div class="feature-section col one-col">
            <br>
            <div>
                <p><b>27 Oct 2015:</b></p>
                - initial release with love from the team

                <br>
                <p><b>30 Oct 2015:</b></p>
                - Updated the import data for the Knowledge base page<br>
                - Small compatibility for different php settings<br>
                - Fixed BuddyPress members page blurry avatar<br>
            </div>
        </div>
    </div>
    <div class="return-to-dashboard">
        <a href="http://seventhqueen.com" target="_blank">&copy; SeventhQueen</a>
    </div>
</div>