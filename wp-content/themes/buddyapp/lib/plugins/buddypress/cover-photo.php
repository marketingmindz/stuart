<?php

function kleo_bp_get_member_cover_attr( $user_id = false )
{
    if ( ! $user_id ) {
        $user_id = bp_get_member_user_id();
    }

    $cover_url = bp_get_user_meta( $user_id, 'profile_cover', true );
    $cover_url = apply_filters( 'bpcp_get_image', $cover_url, $user_id );

    if ( ! $cover_url ) {
        if ( bp_get_option( 'bpcp-profile-default' ) ) {
            $cover_url =  bp_get_option( 'bpcp-profile-default' );
        }
        else {
            return 'class="item-cover"';
        }
    }

    $current_position = bp_get_user_meta( $user_id, 'profile_cover_pos', true );
    if ( ! $current_position ) {
        $current_position = 'center';
    }

    $cover = 'class="item-cover has-cover" style=\'background-image: url("' . $cover_url . '"); background-position: '. $current_position . '; ' .
       'background-repeat: no-repeat; background-size: cover;\'';

    return $cover;
}


function kleo_bp_get_group_cover_attr( $group_id = false ) {
    if ( ! $group_id ) {
        $group_id = bp_get_group_id();
    }

    $cover_url = groups_get_groupmeta( $group_id, 'bpcp_group_cover' );
    $cover_url = apply_filters( 'bpcp_get_group_image', $cover_url, $group_id );

    if ( ! $cover_url ) {
        if ( bp_get_option( 'bpcp-group-default' ) ) {
            $cover_url =  bp_get_option('bpcp-group-default');
        }
        else {
            return 'class="item-cover"';
        }
    }

    $current_position = groups_get_groupmeta( $group_id, 'bpcp_group_cover_pos' );
    if ( ! $current_position ) {
        $current_position = 'center';
    }

    $cover = 'class="item-cover has-cover" style=\'background-image: url("' . $cover_url . '"); background-position: '. $current_position . '; ' .
        'background-repeat: no-repeat; background-size: cover;\'';

    return $cover;
}
