<?php
/* Don't show the page title if is set to off */
if ( ! sq_option( 'page_title_enable', true, true ) ) {
    return false;
}

?>

<!-- Page Title
============================================= -->
<section id="page-title" <?php echo kleo_page_title_class(); ?>>

    <div class="<?php echo Kleo::get_config('container_class');?> clearfix">

        <?php if (sq_option('page_title_title', true)): ?>

            <h1><?php echo kleo_title(); ?></h1>

        <?php endif; ?>

        <?php  if ( sq_option('page_title_tagline') ) : ?>

            <span><?php echo sq_option('page_title_tagline'); ?></span>

        <?php endif; ?>

        <?php if (sq_option('page_title_breadcrumb', true)): ?>

            <?php kleo_breadcrumb( array( 'container' => 'ol', 'separator' => '', 'show_browse' => false ) ); ?>

        <?php endif;?>

    </div>

</section><!-- #page-title end -->