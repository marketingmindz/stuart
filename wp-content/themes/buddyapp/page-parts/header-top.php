<!-- Header
============================================= -->

<?php
//white or dark logo
$logo_mobile = Kleo::get_config( 'logo_mobile' );
$logo_attr = sq_option( $logo_mobile . '_retina', '' ) ? 'data-retina="' . esc_attr( sq_option( $logo_mobile . '_retina' ) ) . '"' : '';
$logo_link = home_url();

?>

<header id="header" <?php kleo_header_class('header-colors'); ?>>

    <div id="header-wrap">

        <div class="container-fluid clearfix">

            <div class="logo">
                <!--mobile logo - only for mobile resolution-->
                <?php if ( sq_option( $logo_mobile, Kleo::get_config($logo_mobile . '_default') ) ) : ?>

                    <a href="<?php echo esc_url( $logo_link ); ?>" class="mobile-logo standard-logo" <?php echo $logo_attr;?>>
                        <img src="<?php echo sq_option( $logo_mobile, Kleo::get_config($logo_mobile . '_default') ); ?>" alt="<?php bloginfo('name'); ?>">
                    </a>

                <?php endif; ?>

            </div>

            <div class="header-left">
                <a href="#" class="sidemenu-trigger sidemenu-icon-wrapper">
                    <span class="sidemenu-icon">
                        <span><i></i><b></b></span>
                        <span><i></i><b></b></span>
                        <span><i></i><b></b></span>
                    </span>
                </a>
            </div>
            <div class="header-right">
                <a href="#" class="second-menu-trigger second-menu-icon-wrapper">
                    <span class="second-menu-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                    
                    <!--<i class="icon-storage"></i>-->
                </a>
            </div>

            <div class="second-menu">

                <div class="second-menu-inner">
                    <div class="second-menu-header">
                        <?php echo kleo_search_form(); ?>
                    </div>
                    
                    <div class="second-menu-main">
                        <div class="scroll-container-wrapper">
                            <div class="scroll-container">
                                <div class="second-menu-section">
                                    <div class="my-profile-wrapper has-submenu">
                                    
                                            <?php if (is_user_logged_in()) : ?>
                                    
                                                <a href="#">
                                                    <em class="my-profile-image">
                                                        <span>
                                                            <?php echo kleo_get_avatar(); ?>
                                                        </span>
                                                    </em>
                                                    <span class="my-profile-name">
                                                        <b><?php
                                                            $current_user = wp_get_current_user();
                                                            echo esc_html( $current_user->display_name );
                                                            ?>
                                                        </b>
                                                    </span>
                                                </a>
                                    
                    
                                                <?php
                                                // Top right navigation menu.
                                                wp_nav_menu( array(
                                                    'theme_location' => 'top-right kleo-nav-menu',
                                                    'menu_class'     => 'submenu',
                                                    'container'      => false,
                                                    'link_before'    => '<span>',
                                                    'link_after'     => '</span>',
                                                    'depth'          => -1,
                                                    'walker'         => new kleo_walker_nav_menu(),
                                                    'fallback_cb'    => 'kleo_bp_menu',
                                                ) );
                                                ?>
                                    
                    
                                            <?php else : ?>
                    
                                                <a href="#" class="show-login">
                                                    <em class="my-profile-image">
                                                        <i class="icon-lock3"></i>
                                                    </em>
                                                    <span class="my-profile-name">
                                                        <b><?php esc_html_e( "Login", 'buddyapp' ); ?></b>
                                                    </span>
                                                </a>
                                    
                                            <?php endif; ?>
                                    
                                    </div>
                    
                    
                                    <?php
                                    // Top left navigation menu.
                                    wp_nav_menu( array(
                                        'theme_location' => 'top-left',
                                        'menu_class'     => 'header-icons kleo-nav-menu',
                                        'container' => false,
                                        'link_before'       => '<span>',
                                        'link_after'        => '</span>',
                                        'depth' => 2,
                                        'max_elements' => 5,
                                        'walker' => new kleo_walker_nav_menu(),
                                        'fallback_cb' => 'kleo_header_icons_menu'
                                    ) );
                                    ?>
                               
                                </div>
                                
                                
                                
                            </div>
                        </div>
                        
                        
                       
                    </div>
                    
                    <div class="second-menu-footer">

                        <?php
                        /**
                         * If WPML plugin is active here the language submenu will show
                         */

                        do_action( 'kleo_header_language' );
                        ?>

                    </div>
                    
                </div>

            </div>


        </div>

    </div>

</header><!-- #header end -->
