<?php
/**
 * Setting section of leadcapture Plugin.
 * 
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    if (isset($_POST['crmsettingSubmit'])) {
       $lc_crm_database_name = $_REQUEST['lc_crm_database_name'];
       update_option('lc_crm_database_name',$lc_crm_database_name);

       $lc_crm_database_username = $_REQUEST['lc_crm_database_username'];
       update_option('lc_crm_database_username',$lc_crm_database_username);

       $lc_crm_database_password = $_REQUEST['lc_crm_database_password'];
       update_option('lc_crm_database_password',$lc_crm_database_password);

       $lc_crm_database_host = $_REQUEST['lc_crm_database_host'];
       update_option('lc_crm_database_host',$lc_crm_database_host);

       $lc_form_field_name = $_REQUEST['lc_form_field_name'];
       update_option('lc_form_field_name',$lc_form_field_name);

       $lc_form_field_phone = $_REQUEST['lc_form_field_phone'];
       update_option('lc_form_field_phone',$lc_form_field_phone);

       $lc_form_field_email = $_REQUEST['lc_form_field_email'];
       update_option('lc_form_field_email',$lc_form_field_email);

         if(($_REQUEST['lc_crm_database_name']!= null || $_REQUEST['lc_crm_database_name']!= '') && ($_REQUEST['lc_crm_database_username']!= null || $_REQUEST['lc_crm_database_username']!= '') && ($_REQUEST['lc_crm_database_password']!= null || $_REQUEST['lc_crm_database_password']!= '') && ($_REQUEST['lc_crm_database_host']!= null || $_REQUEST['lc_crm_database_host']!= ''))
           {
           $lc_massege = '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> <p><strong>Settings saved.</strong></p><p> Add shortcode in footer.php file of theme "[crmLeadcapture]".</p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

             }
         else{
          $lc_massege = '<div class="error settings-error notice is-dismissible" id="setting-error-invalid_admin_email"><p><strong>Please fill the all fields.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
         } 
    }
?>
 <div id="wpwrap wrap">
    <div id="wpbody-content" tabindex="0" aria-label="Main content">
        <div class="wrap">          
            <h2><?php echo __('CRM Settings', 'leadcapture'); ?></h2>
            <?php echo $lc_massege; ?>
            <form novalidate="novalidate" action="" method="post" id="crm_form">
                <table class="form-table" width="800">
                    <tr>
                        <th><?php echo __('CRM Database Host Name', 'leadcapture'); ?>
                         </th>
                        <td><input type="text" placeholder="crm database host name" class="regular-text" name="lc_crm_database_host" value="<?php echo get_option('lc_crm_database_host') ?>">
                        </td>
                    </tr>
                   <tr>
                        <th><?php echo __('CRM Database Name', 'leadcapture'); ?>
                         </th>
                        <td><input type="text" placeholder="crm database name" class="regular-text" name="lc_crm_database_name" value="<?php echo get_option('lc_crm_database_name') ?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo __('CRM Database Username', 'leadcapture'); ?>
                        </th>
                        <td class="input-icon"><input type="text" placeholder="crm database username" name="lc_crm_database_username" class="regular-text" value="<?php echo get_option('lc_crm_database_username') ?>"></td>
                    </tr>
                    <tr>
                        <th><?php echo __('CRM Database Password', 'leadcapture'); ?>                         </th>
                        <td><input type="text" placeholder="crm database Password" class="regular-text" name="lc_crm_database_password" value="<?php echo get_option('lc_crm_database_password') ?>">
                        </td>
                    </tr>   
                    <tr>
                        <th><?php echo __('Form Field Name', 'leadcapture'); ?> 
                        </th>
                        <td id="append"><input type="text" placeholder="Enter Form Fields" class="regular-text" name="lc_form_field_name" value="<?php echo get_option('lc_form_field_name') ?>">
                        <!-- <input type="button" onclick="AddMore();" id="lc_add_more_fields" name="lc_add_more_fields" value="Add More Fields">  -->
                        </td>                    
                    </tr>
                     <tr>
                        <th><?php echo __('Form Field Phone', 'leadcapture'); ?> 
                        </th>
                        <td><input type="text" placeholder="Enter Form Fields" class="regular-text" name="lc_form_field_phone" value="<?php echo get_option('lc_form_field_phone') ?>">
                        <!-- <input type="button" onclick="AddMore();" id="lc_add_more_fields" name="lc_add_more_fields" value="Add More Fields"> -->
                        </td>                    
                    </tr> 
                    <tr>
                        <th><?php echo __('Form Field Email', 'leadcapture'); ?> 
                        </th>
                        <td><input type="text" placeholder="Enter Form Fields" class="regular-text" name="lc_form_field_email" value="<?php echo get_option('lc_form_field_email') ?>">
                        <!-- <input type="button" onclick="AddMore();" id="lc_add_more_fields" name="lc_add_more_fields" value="Add More Fields"> -->
                        </td>                    
                    </tr>               
                    <tr>
                        <td>
                        <?php wp_nonce_field(); ?>
                        <input type="submit" value="Save Setting" class="button button-primary" id="submitbtnform" name="crmsettingSubmit" >
                        <input type="hidden" name="action" value="crm_setting_submit" >
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>