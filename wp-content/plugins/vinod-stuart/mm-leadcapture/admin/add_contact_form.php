<?php require_once(LEAD_CAPTURE_DIR.'/classes/contact_form_template.php');
if(isset($_POST["cf8-save"])){
	$form_title=$_POST["cf8-title"];
	$form_fields=$_POST["cf8-form"];
	$border_px=$_POST["css_border_px"];
    $border_type=$_POST["css_border_type"];
    $border_cl=$_POST["css_border_color"];
    $bg_color=$_POST["css_bg_color"];
    $bg_position=$_POST["css_position"];
    $pd_top=$_POST["css_pd_top"];
    $pd_btm=$_POST["css_pd_btm"];
    $pd_left=$_POST["css_pd_left"];
    $pd_right=$_POST["css_pd_right"];
    $text_color=$_POST["css_text_color"];
    $contact_test_text2=$_POST["contact_test_text2"];

    $button_border_px=$_POST["button_border_px"];
    $button_border_type=$_POST["button_border_type"];
    $button_border_color=$_POST["button_border_color"];
    $button_bg_color=$_POST["button_bg_color"];
    $button_position=$_POST["button_position"];
    $button_pd_top=$_POST["button_pd_top"];
    $button_pd_btm=$_POST["button_pd_btm"];
    $button_pd_left=$_POST["button_pd_left"];
    $button_pd_right=$_POST["button_pd_right"];
    $button_text_color=$_POST["button_text_color"];
   
    
    $output_style="";
    
    if($border_px && $border_type && $border_cl){
         $output_style.="border:".$border_px."px"." ".$border_type." ".$border_cl."; ";
    }else{
        $output_style.="border:0px"." ".$border_type." ".$border_cl."; ";
    }
        
    if($bg_color){
         $output_style.="background-color:".$bg_color."; ";
    }
    if($bg_position){
         $output_style.="text-align:".$bg_position."; ";
    }
    if($text_color){
         $output_style.="color:".$text_color."; ";
    }
    if($pd_top || $pd_btm || $pd_left || $pd_right){
        if($pd_top==''){$pd_top=0;}
        if($pd_btm==''){$pd_btm=0;}
        if($pd_left==''){$pd_left=0;}
        if($pd_right==''){$pd_right=0;}
        $output_style.="padding:".$pd_top."px ".$pd_right."px ".$pd_btm."px ".$pd_left."px"."; ";
    }

    $button_output_style="";
    if($button_border_px && $button_border_type && $button_border_color){
         $button_output_style.="border:".$button_border_px."px"." ".$button_border_type." ".$button_border_color."; ";
    }else{
        $button_output_style.="border:0px"." ".$button_border_type." ".$button_border_color."; ";
    }
        
    if($button_bg_color){
         $button_output_style.="background-color:".$button_bg_color."; ";
    }
    if($button_position){
         $button_output_style.="text-align:".$button_position."; ";
    }
    if($button_text_color){
         $button_output_style.="color:".$button_text_color."; ";
    }
    if($button_pd_top || $button_pd_btm || $button_pd_left || $button_pd_right){
        if($button_pd_top==''){$button_pd_top=0;}
        if($button_pd_btm==''){$button_pd_btm=0;}
        if($button_pd_left==''){$button_pd_left=0;}
        if($button_pd_right==''){$button_pd_right=0;}
        $button_output_style.="padding:".$button_pd_top."px ".$button_pd_right."px ".$button_pd_btm."px ".$button_pd_left."px"."; ";
    }

   

	$form_mail=array();
	$form_mail['mail_recipient']=$_POST["mail_recipient"];
	$form_mail['mail_sender']=$_POST["mail_sender"];
	$form_mail['mail_subject']=$_POST["mail_subject"];
	$form_mail['mail_additional-headers']=$_POST["mail_additional-headers"];
	$form_mail['mail_attachments']=$_POST["mail_attachments"];
	if($_POST["mail_use-html"]==""){
		$form_mail['mail_use-html']=0;
	}else{
		$form_mail['mail_use-html']=$_POST["mail_use-html"];
	}
	$form_mail['mail_body']=$_POST["mail_body"];
	$form_mail_str=implode('<br>', $form_mail);
	$form_msg=array();
	foreach ( cf8_messages() as $key => $arr ) {
		$form_msg[$key]=$_POST[$key];	
	}
	$form_msg_str=implode('<br>', $form_msg);
	$form_post_content=$form_fields.'<br><br>'.$form_mail_str.'<br><br>'.$form_mail_str;

	if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
		$postid=$_REQUEST['post'];
		//updating the form fields into a post...
		$form_post = array(
			'ID'=>$postid,
			'post_title'    => $form_title,
			'post_content'  => $form_post_content,
			'post_status'   => 'publish',
			'post_type' => LC_CUSTOM_POST_TYPE
		);
		// update the post into the database
		wp_update_post( $form_post );
		//update the related post meta to above post id...
		update_post_meta( $postid, '_form', $form_fields );
		update_post_meta( $postid, '_mail', serialize($form_mail) );
		update_post_meta( $postid, '_message', serialize($form_msg) );
		update_post_meta( $postid, '_styleform', $output_style );
		update_post_meta( $postid, '_buttonstyleform', $button_output_style );

		//echo '<b style="color:green;">Form Updated successfully</b><br>[contact-form-8 id="'.$postid.'" title="'.$form_title.'" ]';
		echo '<div class="savemsg"><span><b>Form Updated successfully.</b></span><br>ShortCode is &nbsp;&nbsp;[lc_contact-form id="'.$postid.'" title="'.$form_title.'" ] </div>';
	}else{
		//making the form fields into a post...
		$form_post = array(
			'post_title'    => $form_title,
			'post_content'  => $form_post_content,
			'post_status'   => 'publish',
			'post_type' => LC_CUSTOM_POST_TYPE
		);
		// Insert the post into the database
		$form_post_id=wp_insert_post( $form_post );
		//save the related post meta to above post id...
		update_post_meta( $form_post_id, '_form', $form_fields );
		update_post_meta( $form_post_id, '_mail', serialize($form_mail) );
		update_post_meta( $form_post_id, '_message', serialize($form_msg) );
		update_post_meta( $form_post_id, '_styleform', $output_style );
		update_post_meta( $form_post_id, '_buttonstyleform', $button_output_style );
		if($form_post_id){
			echo '<p class="savemsg"><b style="color:green;">Form submitted successfully:)</b><br>[contact-form id="'.$form_post_id.'" title="'.$form_title.'" ]</p>';
		}
	}		
}
?>
<div class="wrap">
    <h2><?php
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			echo esc_html( __( 'Edit Contact Form') );
			$form_maintitle=get_the_title( $_REQUEST['post'] );
		}else{
			echo esc_html( __( 'Add New Contact Form') );
			$form_maintitle='';
		}
    ?></h2>
</div>

<form method="post" action="">
    <div id="titlediv">
        <input type="text" value="<?php echo $form_maintitle;?>" size="80" name="cf8-title" id="cf8-title" class="border_style">
        <div class="save-contact-form">
            <input type="submit" value="Save" name="cf8-save" class="button-primary" onclick="return add_form_validation();">
        </div>
    </div>
    <?php
    adding_metaboxes();
    do_meta_boxes( null,'form', $post );
    do_meta_boxes( null, 'mail', $post );
    do_meta_boxes( null, 'style_form', $post );
    do_meta_boxes( null, 'buttonstyle_form', $post );
    ?>
</form>

<?php
function adding_metaboxes(){
	require_once(LEAD_CAPTURE_DIR.'/metabox/meta_functions.php');
	add_meta_box( 'formdiv', __( 'Form' ),
		'form_meta_box', null, 'form', 'core' );
	add_meta_box( 'maildiv', __( 'Mail' ),
		'mail_meta_box', null, 'mail', 'core' );
	add_meta_box( 'messagesdiv', __( 'Messages' ),
		'messages_meta_box', null, 'messages', 'core' );
	add_meta_box( 'style_formdiv', __( 'Style Form' ),
		'style_form_meta_box', null, 'style_form', 'core' );
	add_meta_box( 'buttonstyle_formdiv', __( 'Button Style Form' ),
		'buttonstyle_meta_box', null, 'buttonstyle_form', 'core' );
	//do_action( 'adding_metaboxes', $post_id );
}
?>

