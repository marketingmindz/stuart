<?php
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
function wpdocs_register_my_custom_menu_page() {
    add_menu_page('LC Setting', 'LC Setting', 'manage_options', 'lc_setting', 'lc_setting_callback');
    
    add_submenu_page('lc_setting', 'Contact Form', 'Contact Form', 'manage_options', 'contact_form', 'lc_contact_form_callback');

    add_submenu_page('lc_setting', 'Add Contact Form', ' Add Contact Form', 'manage_options', 'add_contact_form', 'lc_add_contact_form_callback');

    add_submenu_page('lc_setting', 'SMTP Setting', ' SMTP Setting', 'manage_options', 'lc_smtp_setting', 'lc_smtp_setting_callback');
    
    add_submenu_page('lc_setting', 'CRM Setting', 'CRM Setting', 'manage_options', 'lc_crm_setting', 'lc_crm_setting_callback');
    
    add_submenu_page('lc_setting', 'Email Setting', 'Email Setting', 'manage_options', 'email_setting', 'lc_email_setting_callback');


    remove_submenu_page('lc_setting', 'lc_setting');
            
}

function lc_smtp_setting_callback() {

    require_once( LEAD_CAPTURE_ADMIN . '/set_smtp.php');
}
function lc_contact_form_callback() {

    require_once( LEAD_CAPTURE_ADMIN . '/contact_form.php');
}
function lc_add_contact_form_callback() {

    require_once( LEAD_CAPTURE_ADMIN . '/add_contact_form.php');
}
function lc_crm_setting_callback() {

    require_once( LEAD_CAPTURE_ADMIN . '/crm_setting.php');
}

function lc_email_setting_callback() {

    require_once( LEAD_CAPTURE_ADMIN . '/email_setting.php');
}

add_shortcode( 'crmLeadcapture', 'LC_add_shortcodes' );
function LC_add_shortcodes() {
    require_once( LEAD_CAPTURE_SHORTCODE . '/shortcode.php');
}
