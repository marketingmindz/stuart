<?php
/* Form */
function form_meta_box( $post ) {
	$form_obj=ContactForm8Template::form();
	?>
	<div class="half-left"><textarea id="cf8-form" name="cf8-form" cols="58" rows="24" class="border_style" ><?php echo $form_obj; ?></textarea></div>
    <div class="half-right">
        <div id="taggenerator" >
            <select id="tag_generator" >
            <option value="0" selected="selected">Add Field</option>
            <option value="text">Text Field</option>
            <option value="email">Email</option>
            <option value="number">Number</option>
            <option value="checkbox">Checkbox</option>
            <option value="radio">Radio Buttons</option>
            <option value="submit">Submit Button</option>
            <option value="select">Drop-Down Menu</option>
            <option value="textarea">TextArea</option>
            <option value="file">File Upload</option>
            <option value="captcha">Captcha</option>
            </select>
        </div>
    	<!--showing add fields content...-->
        <div id="add_extra_fields" style="display:none;" >
            <h2 id="extra_label"></h2><br />
            <div id="req">
                <input type="checkbox" id="required">Required Field?</input><br><br />
            </div>
            <label>Name (Required)</label>
            <input type="text" id="extra_name" name="extra_name" /><br><br />
            <label>ID (Optional)</label>
            <input type="text" id="extra_id" name="extra_id" /><br><br />
            <label>Class (Optional)</label>
            <input type="text" id="extra_class" name="extra_class" /><br /><br />
            <label id="label_val">Default Value(Optional)</label>
            <div id="extra_div_val" style="display:none">
                <input type="text" id="extra_val" name="extra_val" />
                <div id="placeholder_div">
                    <input type="checkbox" id="placeholder">Placeholder</input>
                </div>
            </div><br>
            <textarea id="extra_ta_val" name="extra_ta_val" style="display:none"></textarea><br /><br /><br />
            <b style="color:#03C; font-size:14px">Click Below TextBoxes to get shortcode values...</b>
            <div class="tg-tag">Copy this code and paste it into the form left.<br>
                <input type="text" readonly="readonly" id="extra_shortcodefield_output" name="extra_text" style="width:100%">
            </div>
            <div class="tg-mail-tag">And, put this code into the Mail fields below.<br>
                <input type="text" readonly="readonly" id="extra_shortcodemail_output" style="width:60%">
            </div>
        </div>
    </div>
	<br class="clear" />
	<?php
}

/* Mail */

function mail_meta_box( $post, $box ) {
	$mail_obj=ContactForm8Template::mail();
	?>
    <div class="mail-fields">
        <div class="half-left">
            <div class="mail-field">
                <label for="mail_recipient"><?php echo esc_html( __( 'To:' ) ); ?></label><br />
                <input type="text" id="mail_recipient" name="mail_recipient" class="border_style" size="60" value="<?php echo esc_attr( $mail_obj['mail_recipient'] ); ?>" />
            </div>
            <div class="mail-field">
                <label for="mail_sender"><?php echo esc_html( __( 'From:' ) ); ?></label><br />
                <input type="text" id="mail_sender" name="mail_sender" class="border_style" size="60" value="<?php echo esc_attr( $mail_obj['mail_sender'] ); ?>" />
            </div>
            <div class="mail-field">
                <label for="mail_subject"><?php echo esc_html( __( 'Subject:' ) ); ?></label><br />
                <input type="text" id="mail_subject" name="mail_subject" class="border_style" size="60" value="<?php echo esc_attr( $mail_obj['mail_subject'] ); ?>" />
            </div>
            <div class="pseudo-hr"></div>
            <div class="mail-field">
                <label for="mail_additional-headers"><?php echo esc_html( __( 'Additional headers:' ) ); ?></label><br />
                <textarea id="mail_additional-headers" name="mail_additional-headers" class="border_style" cols="58" rows="2"><?php echo esc_textarea( $mail_obj['mail_additional-headers'] ); ?></textarea>
            </div>
            <div class="mail-field">
                <label for="mail_attachments"><?php echo esc_html( __( 'File attachments:' ) ); ?></label><br />
                <textarea id="mail_attachments" name="mail_attachments" class="border_style" cols="58" rows="2"><?php echo esc_textarea( $mail_obj['mail_attachments'] ); ?></textarea>
            </div>
            <div class="pseudo-hr"></div>
            <div class="mail-field">
                <input type="checkbox" id="mail_use-html" name="mail_use-html" value="1"<?php echo ( $mail_obj['mail_use-html'] ) ? ' checked="checked"' : ''; ?> />
                <label for="mail_use-html"><?php echo esc_html( __( 'Use HTML content type' ) ); ?></label>
            </div>
        </div>
        <div class="half-right">
            <div class="mail-field">
                <label for="mail_body"><?php echo esc_html( __( 'Message body:' ) ); ?></label><br />
                <textarea id="mail_body" name="mail_body" class="border_style" cols="58" rows="18"><?php echo esc_textarea( $mail_obj['mail_body'] ); ?></textarea>
            </div>
        </div>
        <br class="clear" />
    </div>
	<?php
}

 function messages_meta_box( $post ) {
	$msg_obj=ContactForm8Template::messages();
	foreach ( cf8_messages() as $key => $arr ) :
		$field_name = $key;
		if(isset($_REQUEST['action']) && $_REQUEST['action']=='edit'){
			$arr['default']=$msg_obj[$key];
		}
	?>
	<div class="message-field">
		<label for="<?php echo $field_name; ?>"><em># <?php echo esc_html( $arr['description'] ); ?></em></label><br />
		<input type="text" id="<?php echo $field_name; ?>" name="<?php echo $field_name; ?>" class="border_style" size="70" value="<?php echo esc_attr( $arr['default'] ); ?>" />
	</div>
	<?php
	endforeach;
} 

function style_form_meta_box( $post ) {

//$get_style1=get_option("cf8_form_styles");
    $postId=$_REQUEST["post"];
    $get_style1 = get_post_meta($postId, '_styleform', true);
    $get_style1=explode(";",$get_style1);
    $all_stl=array();
    foreach($get_style1 as $style){
        $stl = preg_split('/[\s]+/', $style);
        foreach($stl as $s){
            if(strpos($s, ":")!==false){
                $s1=explode(":",$s);
                $s=$s1[1];
            }
            if($s!=''){
                array_push($all_stl,$s);}
        }
        
    }
    ?>
    <div id="add_design_layout" >

        <label>Border:</label><br>
        <input type="number" id="css_border_px" name="css_border_px" placeholder="PX" value="<?php echo str_replace("px","",$all_stl[0]);?>" />
        <select id="css_border_type" name="css_border_type" >
            <option value="none" <?php if($all_stl[1]=="none"){?>selected="selected"<?php }?>>none</option>
            <option value="thin" <?php if($all_stl[1]=="thin"){?>selected="selected"<?php }?>>thin</option>
            <option value="thick" <?php if($all_stl[1]=="thick"){?>selected="selected"<?php }?>>thick</option>
            <option value="dashed" <?php if($all_stl[1]=="dashed"){?>selected="selected"<?php }?>>dashed</option>
            <option value="double" <?php if($all_stl[1]=="double"){?>selected="selected"<?php }?>>double</option>
            <option value="dotted" <?php if($all_stl[1]=="dotted"){?>selected="selected"<?php }?>>dotted</option>
            <option value="groove" <?php if($all_stl[1]=="groove"){?>selected="selected"<?php }?>>groove</option>
            <option value="hidden" <?php if($all_stl[1]=="hidden"){?>selected="selected"<?php }?>>hidden</option>
            <option value="inset" <?php if($all_stl[1]=="inset"){?>selected="selected"<?php }?>>inset</option>
            <option value="outset" <?php if($all_stl[1]=="outset"){?>selected="selected"<?php }?>>outset</option>
            <option value="ridge" <?php if($all_stl[1]=="ridge"){?>selected="selected"<?php }?>>ridge</option>
            <option value="solid" <?php if($all_stl[1]=="solid"){?>selected="selected"<?php }?>>solid</option>
        </select>
        <input type="color" id="css_border_color" name="css_border_color" placeholder="Color" value="<?php echo $all_stl[2];?>">
        <br /><br />
        <label>Background:</label><br>
        <input type="color" id="css_bg_color" name="css_bg_color" placeholder="Color" value="<?php echo $all_stl[3];?>">
        <br  /><br  />
        <label>Text Color:</label><br>
        <input type="color" id="css_text_color" name="css_text_color" placeholder="Color" value="<?php echo $all_stl[5];?>">
        <br  /><br  />
        <label>Content Position:</label><br>
        <select id="css_position" name="css_position" >
            <option value="left" <?php if($all_stl[4]=="left"){?>selected="selected"<?php }?>>left</option>
            <option value="center" <?php if($all_stl[4]=="center"){?>selected="selected"<?php }?>>center</option>
            <option value="right" <?php if($all_stl[4]=="right"){?>selected="selected"<?php }?>>right</option>
        </select>
        <br /><br  />
        <label>Padding:</label><br>
        Top<input type="number" id="css_pd_top" name="css_pd_top" placeholder="Top(PX)" value="<?php echo str_replace("px","",$all_stl[6]);?>">
        Right<input type="number" id="css_pd_right" name="css_pd_right" placeholder="Right(PX)" value="<?php echo str_replace("px","",$all_stl[7]);?>">
        Bottom<input type="number" id="css_pd_btm" name="css_pd_btm" placeholder="Bottom(PX)" value="<?php echo str_replace("px","",$all_stl[8]);?>" >
        Left<input type="number" id="css_pd_left" name="css_pd_left" placeholder="Left(PX)" value="<?php echo str_replace("px","",$all_stl[9]);?>">
        <br  /><br  />
    
    </div>
    <?php
}

function buttonstyle_meta_box( $post ) {

//$get_style1=get_option("cf8_form_styles");
    $postId=$_REQUEST["post"];
    $get_buttonstyle = get_post_meta($postId, '_buttonstyleform', true);
    $get_buttonstyle=explode(";",$get_buttonstyle);
    $all_button=array();
    foreach($get_buttonstyle as $buttonstyle){
        $buttonstl = preg_split('/[\s]+/', $buttonstyle);
        foreach($buttonstl as $buttons){
            if(strpos($buttons, ":")!==false){
                $buttons1=explode(":",$buttons);
                $buttons=$buttons1[1];
            }
            if($buttons!=''){
                array_push($all_button,$buttons);}
        }
       
    }
    ?>
    <div id="add_design_layout1" >

        <label>Border:</label><br>
        <input type="number" id="button_border_px" name="button_border_px" placeholder="PX" value="<?php echo str_replace("px","",$all_button[0]);?>" />
        <select id="button_border_type" name="button_border_type" >
            <option value="none" <?php if($all_button[1]=="none"){?>selected="selected"<?php }?>>none</option>
            <option value="thin" <?php if($all_button[1]=="thin"){?>selected="selected"<?php }?>>thin</option>
            <option value="thick" <?php if($all_button[1]=="thick"){?>selected="selected"<?php }?>>thick</option>
            <option value="dashed" <?php if($all_button[1]=="dashed"){?>selected="selected"<?php }?>>dashed</option>
            <option value="double" <?php if($all_button[1]=="double"){?>selected="selected"<?php }?>>double</option>
            <option value="dotted" <?php if($all_button[1]=="dotted"){?>selected="selected"<?php }?>>dotted</option>
            <option value="groove" <?php if($all_button[1]=="groove"){?>selected="selected"<?php }?>>groove</option>
            <option value="hidden" <?php if($all_button[1]=="hidden"){?>selected="selected"<?php }?>>hidden</option>
            <option value="inset" <?php if($all_button[1]=="inset"){?>selected="selected"<?php }?>>inset</option>
            <option value="outset" <?php if($all_button[1]=="outset"){?>selected="selected"<?php }?>>outset</option>
            <option value="ridge" <?php if($all_button[1]=="ridge"){?>selected="selected"<?php }?>>ridge</option>
            <option value="solid" <?php if($all_button[1]=="solid"){?>selected="selected"<?php }?>>solid</option>
        </select>
        <input type="color" id="button_border_color" name="button_border_color" placeholder="Color" value="<?php echo $all_button[2];?>">
        <br /><br />
        <label>Background:</label><br>
        <input type="color" id="button_bg_color" name="button_bg_color" placeholder="Color" value="<?php echo $all_button[3];?>">
        <br  /><br  />
        <label>Text Color:</label><br>
        <input type="color" id="button_text_color" name="button_text_color" placeholder="Color" value="<?php echo $all_button[5];?>">
        <br  /><br  />
        <label>Content Position:</label><br>
        <select id="button_position" name="button_position" >
            <option value="left" <?php if($all_button[4]=="left"){?>selected="selected"<?php }?>>left</option>
            <option value="center" <?php if($all_button[4]=="center"){?>selected="selected"<?php }?>>center</option>
            <option value="right" <?php if($all_button[4]=="right"){?>selected="selected"<?php }?>>right</option>
        </select>
        <br /><br  />
        <label>Padding:</label><br>
        Top<input type="number" id="button_pd_top" name="button_pd_top" placeholder="Top(PX)" value="<?php echo str_replace("px","",$all_button[6]);?>">
        Right<input type="number" id="button_pd_right" name="button_pd_right" placeholder="Right(PX)" value="<?php echo str_replace("px","",$all_button[7]);?>">
        Bottom<input type="number" id="button_pd_btm" name="button_pd_btm" placeholder="Bottom(PX)" value="<?php echo str_replace("px","",$all_button[8]);?>" >
        Left<input type="number" id="button_pd_left" name="button_pd_left" placeholder="Left(PX)" value="<?php echo str_replace("px","",$all_button[9]);?>">
        <br  /><br  />
   
    </div>
    <?php
}


function additional_settings_meta_box( $post ) {
	?>
		<textarea id="wpcf7-additional-settings" name="wpcf7-additional-settings" cols="100" rows="8"><?php echo esc_textarea( $post->prop( 'additional_settings' ) ); ?></textarea>
	<?php
}
?>