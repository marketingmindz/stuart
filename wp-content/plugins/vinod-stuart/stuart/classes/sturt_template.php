<div id="wpbody" role="main">

<div id="wpbody-content" aria-label="Main content" tabindex="0">
		<!-- <div id="screen-meta" class="metabox-prefs">

			<div id="contextual-help-wrap" class="hidden" tabindex="-1" aria-label="Contextual Help Tab">
				<div id="contextual-help-back"></div>
				<div id="contextual-help-columns">
					<div class="contextual-help-tabs">
						<ul>
						
							<li id="tab-link-overview" class="active">
								<a href="#tab-panel-overview" aria-controls="tab-panel-overview">
									Overview								</a>
							</li>
												</ul>
					</div>

										<div class="contextual-help-sidebar">
						<p><strong>For more information:</strong></p><p><a href="https://codex.wordpress.org/Settings_General_Screen">Documentation on General Settings</a></p><p><a href="https://wordpress.org/support/">Support Forums</a></p>					</div>
					
					<div class="contextual-help-tabs-wrap">
						
							<div id="tab-panel-overview" class="help-tab-content active">
								<p>The fields on this screen determine some of the basics of your site setup.</p><p>Most themes display the site title at the top of every page, in the title bar of the browser, and as the identifying name for syndicated feeds. The tagline is also displayed by many themes.</p><p>The WordPress URL and the Site URL can be the same (example.com) or different; for example, having the WordPress core files (example.com/wordpress) in a subdirectory instead of the root directory.</p><p>If you want site visitors to be able to register themselves, as opposed to by the site administrator, check the membership box. A default user role can be set for all new users, whether self-registered or registered by the site admin.</p><p>You can set the language, and the translation files will be automatically downloaded and installed (available if your filesystem is writable).</p><p>UTC means Coordinated Universal Time.</p><p>You must click the Save Changes button at the bottom of the screen for new settings to take effect.</p>							</div>
											</div>
				</div>
			</div>
				</div> -->
			<!-- 	<div id="screen-meta-links">
					<div id="contextual-help-link-wrap" class="hide-if-no-js screen-meta-toggle">
			<button type="button" id="contextual-help-link" class="button show-settings" aria-controls="contextual-help-wrap" aria-expanded="false">Help</button>
			</div>
				</div> -->
		
<div class="wrap">
<h1>Stuart Woocommerce Genral Setting</h1>

<form method="post" action="" novalidate="novalidate">
<input type="hidden" name="option_page" value="general"><input type="hidden" name="action" value="update"><input type="hidden" id="_wpnonce" name="_wpnonce" value="40e2108430"><input type="hidden" name="_wp_http_referer" value="/wpthemes/stuart/wp-admin/options-general.php">
<table class="form-table">
<tbody><tr>
<th scope="row"><label for="min_amount">Minimum Amount</label></th>
<td><input name="min_amount" type="text" id="min_amount" value="" class="regular-text">
<p class="description" id="tagline-description">Define minimum amount of Order allowing to choose Stuart Delivery Method for Free</p></td>
</tr>
<tr>
<th scope="row"><label for="min_order_time">Minimum order prepration time</label></th>
<td><input name="min_order_time" type="text" id="min_order_time"  value="" class="regular-text">
<p class="description" id="tagline-description">Define minimum amount of time necessary to prepare the Order.</p></td>
</tr>
<tr>
<th scope="row"><label for="stuart_fee">Stuart fee</label></th>
<td><input name="stuart_fee" type="text" id="stuart_fee" value="" class="regular-text code"></td>
</tr>
<tr>
<th scope="row"><label for="address">Address Location</label></th>
<td><input name="address" type="url" id="address" value="" class="regular-text code">
</td>
</tr>
<tr>
<th scope="row"><label for="stu-checkout">Stuart method on the checkout page</label></th>
<td><input name="stu-checkout" type="text" id="stu-checkout" value="" class="regular-text ltr">
<p class="description" id="stu-checkout">Stuart - Livraison premium”, others as “Stuart - Delivery Method</p></td>
</tr>

</tbody></table>


<p class="save"><input type="submit" name="save" id="save" class="btn btn-primary" value="Save Changes"></p></form>

</div>


<div class="clear"></div></div><!-- wpbody-content -->
<div class="clear"></div></div>