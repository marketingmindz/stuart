<?php
add_action( 'admin_menu', 'stu_register_my_custom_menu_page' );
function stu_register_my_custom_menu_page() {
    add_menu_page('SW Setting', 'Stuart Setting', 'manage_options', 'sw_setting', 'sw_setting_callback');
    add_submenu_page('sw_setting', 'General', 'General', 'manage_options', 'add_stuart', 'sw_add_stuart_callback');
    //add_submenu_page('sw_setting', 'Advance setting', 'Advance setting', 'manage_options', 'advance_stuart', 'sw_advance_stuart_callback');
   

    remove_submenu_page('sw_setting', 'sw_setting');
            
}


function sw_add_stuart_callback() {

    require_once( STUART_WOO_ADMIN . '/add_stuart.php');
}

function woocommerce_apply_stuart_shipping(){

	require_once( STUART_WOO_ADMIN . '/apply_stuart_shpping.php');
}

add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_apply_stuart_shipping', 10 );