<?php
/**
Plugin Name: Stuart woo
Description: Side bar Stuart Woo form. Simple but flexible.
Author: Marketingmindz
Author URI: http://marketingmindz.com/
Version: 1.0.0
**/
if ( ! defined( 'ABSPATH' ) ) exit;	// Exit if accessed directly

if(!class_exists('ClassStuartwoo')) {
	class ClassStuartwoo{   		/**
		 * Construct The Plugin Object
		 *
		*/
 		public function __construct() {
			/* Set the constants needed by the plugin. */
			add_action( 'plugins_loaded', array( &$this, 'stuart_woo_constants' ), 1 );
			
			/* Load the admin files. */
			add_action( 'plugins_loaded', array( &$this, 'stuart_woo_admin' ), 2 );	
			
			/* Add style for front-end */
			add_action( 'wp_enqueue_scripts', array(&$this, 'stuart_woo_front_style'));

			/* Activation hook */
			register_activation_hook(__FILE__, array($this, 'stuart_woo_activate'));
			
			/* Deactivation hook */
			register_deactivation_hook(__FILE__, array(&$this, 'stuart_woo_deactivate'));
			
			/* Uninstall hook */
			register_uninstall_hook(__FILE__, array($this, 'stuart_woo_uninstall'));
		}
		
		
		/**
		 * Defines constants used by the plugin.
		 *
		*/
		function stuart_woo_constants(){			
			
			/* Set constant path to the SW plugin directory. */
			define( 'STUART_WOO_DIR', trailingslashit(plugin_dir_path( __FILE__ )));
	
			/* Set constant path to the SW plugin URL. */
			define( 'STUART_WOO_URL', trailingslashit(plugin_dir_url( __FILE__ )));			
	
			/* Set the constant path to the SW admin directory. */
			define( 'STUART_WOO_ADMIN', STUART_WOO_DIR . trailingslashit( 'admin' ) );				
			
			/* Set the constant path to the SW admin directory. */
			define( 'STUART_WOO_ADMIN_URL', STUART_WOO_URL . trailingslashit( 'admin' ) );
			
			/* Set the constant path to the SW stylesheet directory. */
			define( 'STUART_WOO_CSS', STUART_WOO_URL . trailingslashit( 'css' ));

			/* Set the constant path to the SW script directory. */
			define( 'STUART_WOO_JS', STUART_WOO_URL . trailingslashit( 'js' ));
			
			/* Set the constant path to the SW Image directory. */
			define( 'STUART_WOO_IMAGE', STUART_WOO_URL . trailingslashit( 'images' ));
			/*Custom Post type*/
			//define('LC_CUSTOM_POST_TYPE', 'lc_setting' );

			/* Set the constant path to the SW Image directory. */
			define( 'STUART_WOO_SHORTCODE', STUART_WOO_DIR . trailingslashit( 'shortcode' ));
				
		}
		/**
		 * Loads the admin section files needed by the plugin.
		 *
		**/
		function stuart_woo_admin() {
			/* Load the Files in Admin Section. */			
			require_once( STUART_WOO_ADMIN . 'functions.php');
			//require_once(STUART_WOO_SHORTCODE.'contactformshortcode.php');
			wp_enqueue_style('sw_admin_style',STUART_WOO_CSS.'styles.css');						
			//wp_enqueue_script('sw_admin_script',STUART_WOO_JS.'scripts.js');
					
		}
		
		/**
		 *  Enqueue stylesheet For Front End
		 *
		**/
		function stuart_woo_front_style() {
			wp_enqueue_style('sw_front_style',STUART_WOO_CSS.'slider.css');			
			wp_enqueue_script('sw_front_script',STUART_WOO_JS.'slider.js');


		}
		
		/**
		 *  Activate the plugin
		 *
		**/
		function stuart_woo_activate() {
			// Create post object
		if ( ! function_exists( 'WC' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            deactivate_plugins( plugin_basename( __FILE__ ) );

            wp_die( '<div class="error"><p>' . sprintf( __( '<b>Stuart woo</b> requires %sWoocommerce%s to be installed & activated!', 'Stuart woo' ), '<a target="_blank" href="https://wordpress.org/plugins/woocommerce/">', '</a>' ) . '</p></div>' );
        }


		}

		/**
		 *   deactivate Hook Of The Plugin
		 *
		**/		
		function stuart_woo_deactivate() {
			 
		}
		/**
		 *   Uninstall Hook Of The Plugin
		 *
		**/
		function stuart_woo_uninstall() {
			  
			  //if( ! defined( 'WP_UNINSTALL_PLUGIN' ) )
			  //exit();
		}
	}
}
/* Instantiate the STUART Woo class */
$ObSotre = new ClassStuartwoo();