<?php
/**
Plugin Name: Stuart Delivery
Description: Woocommerce - Stuart Integration Plugin.
Author: Marketingmindz
Author URI: http://marketingmindz.com
Version: 1.0.0
**/

if ( ! defined( 'ABSPATH' ) ) exit;	// Exit if accessed directly

// Check if WooCommerce is active
if ( ! ClassStuartDelivery::is_woocommerce_active() ) {

	add_action( 'admin_notices', 'stuart_delivery_render_wc_inactive_notice' );
	return;
}

/**
 * Renders a notice when WooCommerce in not activate
 *
 */
function stuart_delivery_render_wc_inactive_notice() {

	$message = sprintf(
		/* translators: %1$s and %2$s are <strong> tags. %3$s and %4$s are <a> tags */
		__( '%1$sStuart Delivery is inactive%2$s as it requires WooCommerce.', 'stuart-delivery' ),
		'<strong>',
		'</strong>'
	);
	printf( '<div class="error"><p>%s</p></div>', $message );
}


class ClassStuartDelivery{   		
	
	protected static $instance;
	/**
	 * Construct The Plugin Object
	 *
	*/
	public function __construct() {
		/* Set the constants needed by the plugin. */
		add_action( 'plugins_loaded', array( &$this, 'stuart_delivery_constants' ), 1 );
		
		/* Load the admin files. */
		add_action( 'plugins_loaded', array( &$this, 'stuart_delivery_admin' ), 2 );	
		
		/* Add style for front-end */
		add_action( 'wp_enqueue_scripts', array(&$this, 'stuart_delivery_front_style'), 20);

		/* Activation hook */
		register_activation_hook(__FILE__, array($this, 'stuart_delivery_activate'));
		
		/* Deactivation hook */
		register_deactivation_hook(__FILE__, array(&$this, 'stuart_delivery_deactivate'));
		
		/* Uninstall hook */
		//register_uninstall_hook(__FILE__, array($this, 'stuart_delivery_uninstall'));
	}

		
	
	/**
	 * Defines constants used by the plugin.
	 *
	*/
	public function stuart_delivery_constants(){			
		
		/* Set constant path to the SD plugin directory. */
		define( 'STUART_DELIVERY_DIR', trailingslashit(plugin_dir_path( __FILE__ )));

		/* Set constant path to the SD plugin URL. */
		define( 'STUART_DELIVERY_URL', trailingslashit(plugin_dir_url( __FILE__ )));			

		/* Set the constant path to the SD admin directory. */
		define( 'STUART_DELIVERY_ADMIN_DIR', STUART_DELIVERY_DIR . trailingslashit( 'admin' ) );
		define( 'STUART_DELIVERY_ADMIN_URL', STUART_DELIVERY_URL . trailingslashit( 'admin' ) );				
		/* Set the constant URL to the SD stylesheet directory. */
		define( 'STUART_DELIVERY_CSS_URL', STUART_DELIVERY_URL . trailingslashit( 'css' ));

		/* Set the constant path to the SD script directory. */
		define( 'STUART_DELIVERY_JS_URL', STUART_DELIVERY_URL . trailingslashit( 'js' ));
		
		/* Set the constant path to the SD includes directory. */
		define( 'STUART_DELIVERY_INCLUDES_URL', STUART_DELIVERY_URL . trailingslashit( 'includes' ));
		
	}

	/**
	 * Checks if WooCommerce is active
	 */
	public static function is_woocommerce_active() {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
	}
	/**
	 * Loads the admin section files needed by the plugin.
	 *
	**/
	public function stuart_delivery_admin() {
		/* Load the Files in Admin Section. */			
		require_once( STUART_DELIVERY_ADMIN_DIR . 'functions.php');
		
		//wp_enqueue_style('sd_admin_style',STUART_DELIVERY_CSS.'styles.css');					
		
				
	}
	
	/**
	 *  Enqueue stylesheet For Front End
	 *
	**/
	public function stuart_delivery_front_style() {
		wp_enqueue_style('sd_time_picker_style',STUART_DELIVERY_CSS_URL.'timepicker.css');			
		wp_enqueue_script('sd_custom_script',STUART_DELIVERY_JS_URL.'custom.js');
		wp_enqueue_script('sd_time_picker_script', STUART_DELIVERY_JS_URL.'timepicker.js');


	}
	
	/**
	 *  Activate the plugin
	 *
	**/
	public function stuart_delivery_activate() {
		// Create post object
	

	}

	/**
	 *   Deactivate Hook Of The Plugin
	 *
	**/		
	public function stuart_delivery_deactivate() {
		 
	}
	/**
	 *   Uninstall Hook Of The Plugin
	 *
	**/
	public function stuart_delivery_uninstall() {
		  
		  //if( ! defined( 'WP_UNINSTALL_PLUGIN' ) )
		  //exit();
	}

	/**
	 * Main Stuart Instance, ensures only one instance is/can be loaded

	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

}

/**
 * Returns the One True Instance of Stuart
 */
function Stuart_Delivery() {
	return ClassStuartDelivery::instance();
}


/**
 * The Stuart_Delivery global object
 */
$GLOBALS['Stuart_Delivery'] = Stuart_Delivery();


/* Instantiate the Stuart Delivery class */
//$ObSotre = new ClassStuartDelivery();



