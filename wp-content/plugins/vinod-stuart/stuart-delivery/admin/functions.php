<?php

add_action( 'admin_menu', 'sd_register_custom_menu_page' );




function sd_register_custom_menu_page() {
    add_menu_page('Stuart Delivery', 'Stuart Delivery', 'manage_options', 'stuart_delivery', 'stuart_delivery_callback','', 50);
    add_submenu_page('stuart_delivery', 'Settings', 'Settings', 'manage_options', 'settings', 'settings_callback');
    //add_submenu_page('sw_setting', 'Advance setting', 'Advance setting', 'manage_options', 'advance_stuart', 'sw_advance_stuart_callback');
   

    //remove_submenu_page('sw_setting', 'stuart_delivery');
            
}

function stuart_delivery_callback() {
	echo '<br><br><br><br><br>========= Some content goes here =========';
    //require_once( STUART_WOO_ADMIN . '/add_stuart.php');
}

function settings_callback() {
    require_once( STUART_DELIVERY_ADMIN_DIR . '/settings.php');
}



add_action( 'init', 'post_type_city' );

/**
 * Register a post type for Time slot.
 *
 */
function post_type_city() {
	$labels = array(
		'name'               => _x( 'Cities', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'City', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Cities', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'City', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New City', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New City', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit City', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View City', 'your-plugin-textdomain' ),
		'all_items'          => __( 'Cities', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Cities', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Cities:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No cities found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No cities found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'stuart_delivery',
		'show_in_nav_menus'   => true,
    	'show_in_admin_bar'   => true,
    	'menu_position'       => null,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'city' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' )
	);

	register_post_type( 'city', $args );
}

/**
 * Create Meta Boxes For city Post Type
**/
function add_custom_meta_box_city() {
	add_meta_box("sd_city_start_time", "Start time", "custom_meta_box_for_city_start_time", "city", "normal", "high", null);
	add_meta_box("sd_city_end_time", "End time", "custom_meta_box_for_city_end_time", "city", "normal", "high", null);

}
add_action("add_meta_boxes", "add_custom_meta_box_city");



/**
 * Add Custom meta box for city
**/
function custom_meta_box_for_city_start_time($object){
	global $post;
	$city_start_time_unix = get_post_meta($post->ID, 'city_start_time', true);
	$city_start_time = $city_start_time_unix != "" ? retrieve_formatted_time($city_start_time_unix) : '';
?>
	
	<input type="text" name="city_start_time" id="city_start_time" placeholder="From" value="<?php echo $city_start_time; ?>" />
<?php }

/**
 * Add Custom meta box for city
**/
function custom_meta_box_for_city_end_time($object){
	global $post;
	$city_end_time_unix = get_post_meta($post->ID, 'city_end_time', true);
	$city_end_time = $city_end_time_unix != "" ? retrieve_formatted_time($city_end_time_unix) : '';

?>
	
	<input type="text" name="city_end_time" id="city_end_time" placeholder="To" value="<?php echo $city_end_time; ?>" />
<?php }


/**
 * Save city post hook
**/
function save_post_type_meta($post_id, $post) {
	$post_type = $post->post_type;
	$post_id = $post->ID;
	//prd($_REQUEST);
	
	#Save the Metabox Data for city post type
	if( $post_type == 'city' ){
		if(isset($_REQUEST['city_start_time'])){
			update_post_meta($post_id, 'city_start_time', strtotime($_REQUEST['city_start_time']));
		}
		if(isset($_REQUEST['city_end_time'])){
			update_post_meta($post_id, 'city_end_time', strtotime($_REQUEST['city_end_time']));
		}
		
	}

}
add_action('save_post', 'save_post_type_meta', 1, 2); // save the custom fields


/**
 * Display custom columns on city listing page
**/
/* Add action for custom column addition  */			
add_filter('manage_city_posts_columns',  'sd_city_column');

/* Add action for manage custom column */	
add_action('manage_city_posts_custom_column', 'sd_city_custom_column', 10, 2 );

/* Add action for manage sortable custom column */
add_filter('manage_edit-city_sortable_columns', 'sd_city_register_sortable');

function sd_city_column($columns) {
	$date = $columns['date'];
	unset($columns['date']);
	$columns['sd_time']  = 'Stuart Time';
	$columns['date'] = $date;
	return $columns;
}
		
function sd_city_custom_column($column_name,$post_id ) {
	if($column_name == 'sd_time') {
		$city_start_time_unix = get_post_meta( $post_id, 'city_start_time', true );
		$city_start_time = $city_start_time_unix != "" ? retrieve_formatted_time($city_start_time_unix) : '';
		$city_end_time_unix = get_post_meta( $post_id, 'city_end_time', true );
		$city_end_time = $city_end_time_unix != "" ? retrieve_formatted_time($city_end_time_unix) : '';
		echo $city_start_time . ' - ' . $city_end_time ;
	}

}

function sd_city_register_sortable($columns) {
		$columns['sd_time']  = 'rsp_employees';
		return $columns;

}



/**
 * Add the field to the checkout
 */
//add_action( 'woocommerce_checkout_after_customer_details', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {
	$city_args = array(
	    'post_type'      => 'city',
	    'posts_per_page' => -1
	);
	$citiesArray = get_posts( $city_args );
	$cities = array();
	foreach ($citiesArray as $citiesArr) {
		$cities[] = strtolower($citiesArr->post_title);
	}
	$encoded_cities = json_encode($cities);


	?>

	<script>
	
	jQuery( document ).ready(function() {
		var cities = '<?php echo $encoded_cities; ?>'; // DND
		//var citiesArray = ["jaipur", "london", "madrid", "barcelona", "lyon", "paris"];
		var citiesArray = jQuery.parseJSON(cities);
		
	    jQuery("#sd_custom_checkout_field").hide();
		jQuery("#billing_city").blur(function(){
			var billing_city = jQuery("#billing_city").val().toLowerCase();
			//console.log( billing_city );
			if (jQuery.inArray(billing_city, citiesArray) != -1){
				jQuery("#sd_custom_checkout_field").show(500);
			} else {
				jQuery("#sd_custom_checkout_field").hide(500);
			}
        	
    	});

    	jQuery('#sd_delivery_time').timepicker().on( "hideTimepicker", function() {
		  //console.log( 'hiii' );
			var sd_delivery_time = jQuery("#sd_delivery_time").val();
			var billing_city = jQuery("#billing_city").val(); //DND
			//var billing_city = 'jaipur';
			
			if(sd_delivery_time != '' ){
			  	var ajax_source = '<?php echo STUART_DELIVERY_INCLUDES_URL . "sd_ajax.php"; ?>'; // DND 
			  	//var ajax_source = 'http://localhost/wpthemes/stuart/wp-content/plugins/stuart-delivery/includes/sd_ajax.php'; 
			  	jQuery.ajax({
		         type : "post",
		         dataType : "json",
		         url : ajax_source,
		         data : {action: "validate_sd_time", delivery_time: sd_delivery_time, city: billing_city},
		         success: function(response) {
		         	console.log(response); 
		            if(response.status == true && response.city_count > 0 ) {
		            	jQuery("#sd_delivery_method_container").show(500);
		            } else {
		            	jQuery("#sd_delivery_method_container").hide(500);
		            }
		            
		         }
		      })
		   	} else {
		  		jQuery("#sd_delivery_method_container").hide(500);
		  }
		});

		jQuery("#sd_delivery_time").blur(function(){
			var sd_delivery_time = jQuery("#sd_delivery_time").val();
			if(sd_delivery_time == '' ){
				jQuery("#sd_delivery_method_container").hide(500);
			}
        	
    	});


		



	    
	});
	</script>

	<?php
    


    echo '<div id="sd_custom_checkout_field"><h3>' . __('Delivery Option') . '</h3>';
    echo '<li class="wc_payment_method payment_method_bacs">';
    
    //$stu_checkout_label = 'Stuart - Delivery Method';
    $stu_checkout_label = get_option('stu_checkout_label');

    woocommerce_form_field("sd_delivery_time", array(
	    'type'              => 'text',
	    'class'             => array('form-row-wide'),
	    'label'             => 'Delivery Time',
	    'placeholder'       => 'Select delivery time',

	) );

echo '<div class="payment_box" id="sd_delivery_method_container" style="display: none">';
    woocommerce_form_field( 'my_field_name', array(
        'type'          => 'radio',
        'class'         => array('sd_delivery_method form-row-wide'),
        //'label'         => __('Stuart - Delivery Method'),
        'options'       => array( '1' => $stu_checkout_label ),
        ));

    
			
		echo '</div>';
    	
    echo '</li></div>';

}




/*function woocommerce_apply_stuart_shipping(){

	require_once( STUART_DELIVERY_ADMIN_DIR . '/apply_stuart_shpping.php');
}
*/
//add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_apply_stuart_shipping', 10 );

/* *********** Common Functions ****************  */

function retrieve_formatted_time($unixtime){

	$time = date("h:ia", $unixtime);
	return $time;

}




/* -------------Shipping Functionality------------------*/

function Stuart_Shipping_Method() {
    if ( ! class_exists( 'Stuart_Shipping_Method' ) ) {
        class Stuart_Shipping_Method extends WC_Shipping_Method {
            /**
             * Constructor for your shipping class
             *
             * @access public
             * @return void
             */
            public function __construct() {
                $this->id                 = 'stuart'; 
                $this->method_title       = __( 'Stuart Shipping', 'stuart' );  
                $this->method_description = __( 'Custom Shipping Method for Stuart', 'stuart' ); 

                // Availability & Countries
                $this->availability = 'including';
                $this->countries = array(
                    'US', // Unites States of America
                    'CA', // Canada
                    'IN',
                    'DE', // Germany
                    'GB', // United Kingdom
                    'IT',   // Italy
                    'ES', // Spain
                    'HR'  // Croatia
                    );

                $this->init();

                $this->enabled = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'yes';
                $this->title = isset( $this->settings['title'] ) ? $this->settings['title'] : __( 'Stuart Shipping', 'stuart' );
            }

            /**
             * Init your settings
             *
             * @access public
             * @return void
             */
            function init() {
                // Load the settings API
                $this->init_form_fields(); 
                $this->init_settings(); 

                // Save settings in admin if you have any defined
                add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }

            /**
             * Define settings field for this shipping
             * @return void 
             */
            function init_form_fields() { 

                $this->form_fields = array(

                 'enabled' => array(
                      'title' => __( 'Enable', 'stuart' ),
                      'type' => 'checkbox',
                      'description' => __( 'Enable this shipping.', 'stuart' ),
                      'default' => 'yes'
                      ),

                 'title' => array(
                    'title' => __( 'Title', 'stuart' ),
                      'type' => 'text',
                      'description' => __( 'Title to be display on site', 'stuart' ),
                      'default' => __( 'Stuart Shipping', 'stuart' )
                      ),

                 'weight' => array(
                    'title' => __( 'Weight (kg)', 'stuart' ),
                      'type' => 'number',
                      'description' => __( 'Maximum allowed weight', 'stuart' ),
                      'default' => 100
                      ),

                 );

            }

            /**
             * This function is used to calculate the shipping cost. Within this function we can check for weights, dimensions and other parameters.
             *
             * @access public
             * @param mixed $package
             * @return void
             */
            public function calculate_shipping( $package=array() ) {
                
            	//pr($package);

                $weight = 0;
                $cost = 0;
                $country = $package["destination"]["country"];
                //$city = $package["destination"]["city"];

                $city = "jaipur";
                
                /*$countryZones = array(
                    'HR' => 0,
                    'IN' => 2,
                    'US' => 3,
                    'GB' => 2,
                    'CA' => 3,
                    'ES' => 2,
                    'DE' => 1,
                    'IT' => 1
                    );

                $zonePrices = array(
                    0 => 10,
                    1 => 30,
                    2 => 50,
                    3 => 70
                    );

                $zoneFromCountry = $countryZones[ $country ];
                $priceFromZone = $zonePrices[ $zoneFromCountry ];*/

                $cost = 55;

                $rate = array(
                    'id' => $this->id,
                    'label' => $this->title,
                    'cost' => $cost
                );
                if($city == "jaipur"){

                	$this->add_rate( $rate );
                }
                
            }
        }
    }
}

add_action( 'woocommerce_shipping_init', 'Stuart_Shipping_Method' );








function add_Stuart_Shipping_Method( $methods ) {
    $methods[] = 'Stuart_Shipping_Method';
    return $methods;
}
 
add_filter( 'woocommerce_shipping_methods', 'add_Stuart_Shipping_Method' );
















function stuart_validate_order( $posted )   {
 
        $packages = WC()->shipping->get_packages();
 
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
         
        if( is_array( $chosen_methods ) && in_array( 'stuart', $chosen_methods ) ) {
             
            foreach ( $packages as $i => $package ) {
 
                if ( $chosen_methods[ $i ] != "stuart" ) {
                             
                    continue;
                             
                }
 
                $Stuart_Shipping_Method = new Stuart_Shipping_Method();
                $weightLimit = (int) $Stuart_Shipping_Method->settings['weight'];
                $weight = 0;
 
                foreach ( $package['contents'] as $item_id => $values ) 
                { 
                    $_product = $values['data']; 
                    $weight = $weight + $_product->get_weight() * $values['quantity']; 
                }
 
                $weight = wc_get_weight( $weight, 'kg' );
                
                if( $weight > $weightLimit ) {
 
                        $message = sprintf( __( 'Sorry, %d kg exceeds the maximum weight of %d kg for %s', 'stuart' ), $weight, $weightLimit, $Stuart_Shipping_Method->title );
                             
                        $messageType = "error";
 
                        if( ! wc_has_notice( $message, $messageType ) ) {
                         
                            wc_add_notice( $message, $messageType );
                      
                        }
                }
            }       
        } 
    }
 
add_action( 'woocommerce_review_order_before_cart_contents', 'stuart_validate_order' , 10 );
add_action( 'woocommerce_after_checkout_validation', 'stuart_validate_order' , 10 );


