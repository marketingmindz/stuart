<?php
/**
 * Setting section of Stuart Plugin.
 * 
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
	$sd_massege = '';
    if (isset($_POST['setting_submit'])) {

       update_option('min_amount',$_REQUEST['min_amount']);
       update_option('min_order_time',$_REQUEST['min_order_time']);
       update_option('stuart_fee',$_REQUEST['stuart_fee']);
       update_option('address_location',$_REQUEST['address_location']);
       update_option('stu_checkout_label',$_REQUEST['stu_checkout_label']);
       update_option('sd_working_day',$_REQUEST['sd_working_day']);

       
       $sd_massege = '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> <p><strong>Settings saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

         
    }

	$min_amount = get_option('min_amount');
	$min_order_time = get_option('min_order_time');
	$stuart_fee = get_option('stuart_fee');
	$address_location = get_option('address_location');
	$stu_checkout_label = get_option('stu_checkout_label');
	$sd_working_day = get_option('sd_working_day');
?>

<div class="wrap">
<h1>Stuart Woocommerce Genral Setting</h1>
<?php echo $sd_massege; ?>
<form method="post" action="" novalidate="novalidate">

<table class="form-table">
	<tr>
	<th scope="row"><label for="min_amount">Minimum Amount</label></th>
	<td>
	<input name="min_amount" type="text" id="min_amount" value="<?php echo $min_amount; ?>" class="regular-text">
	<p class="description" id="tagline-description">
	Define minimum amount of Order allowing to choose Stuart Delivery Method for Free</p></td>
	</tr>

	<tr>
	<th scope="row"><label for="min_order_time">Minimum order prepration time <em>(In minute)</em></label></th>
	<td>
	<input name="min_order_time" type="text" id="min_order_time"  value="<?php echo $min_order_time; ?>" class="regular-text">
	<p class="description" id="tagline-description">Define minimum amount of time necessary to prepare the Order.</p></td>
	</tr>

	<tr>
	<th scope="row"><label for="stuart_fee">Stuart Fee</label></th>
	<td>
	<input name="stuart_fee" type="text" id="stuart_fee" value="<?php echo $stuart_fee; ?>" class="regular-text code">
	<p class="description" id="tagline-description">Stuart shipping price.</p>
	</td>
	</tr>

	<tr>
	<th scope="row"><label for="address_location">Address Location</label></th>
	<td>
	
	<textarea name="address_location" id="address_location" class="large-text code" rows="3"><?php echo $address_location; ?></textarea>
	<p class="description" id="tagline-description">Address location (warehouse) from where the order is shipped</p>
	
	</td>
	</tr>

	<tr>
	<th scope="row"><label for="stu_checkout_label">Label on the checkout page</label></th>
	<td>
	<input name="stu_checkout_label" type="text" id="stu_checkout_label" value="<?php echo $stu_checkout_label; ?>" class="regular-text ltr">
	<p class="description" id="stu-checkout">The “Label” of the Stuart method on the checkout page.</p></td>
	</tr>

	<tr>
	<th scope="row"><label for="stu-checkout">Working Day</label></th>
	<td>
	

	<select name="sd_working_day[]" id="sd_working_day" style="height: 132px;" multiple>
		<option value="Sunday" <?php echo !empty($sd_working_day) && in_array("Sunday", $sd_working_day) ? "selected='selected'" : ""; ?> >Sunday</option>
		<option value="Monday" <?php echo !empty($sd_working_day) && in_array("Monday", $sd_working_day) ? "selected='selected'" : ""; ?> >Monday</option>
		<option value="Tuesday" <?php echo !empty($sd_working_day) && in_array("Tuesday", $sd_working_day) ? "selected='selected'" : ""; ?> >Tuesday</option>
		<option value="Wednesday" <?php echo !empty($sd_working_day) && in_array("Wednesday", $sd_working_day) ? "selected='selected'" : ""; ?> >Wednesday</option>
		<option value="Thursday" <?php echo !empty($sd_working_day) && in_array("Thursday", $sd_working_day) ? "selected='selected'" : ""; ?> >Thursday</option>
		<option value="Friday" <?php echo !empty($sd_working_day) && in_array("Friday", $sd_working_day) ? "selected='selected'" : ""; ?> >Friday</option>
		<option value="Saturday" <?php echo !empty($sd_working_day) && in_array("Saturday", $sd_working_day) ? "selected='selected'" : ""; ?> >Saturday</option>
	</select>
	<p class="description" id="stu-checkout">Stuart delivery method will be available only during these days. You can select multiple using Ctrl button.</p></td>
	</tr>



</table>

<p class="submit"><input type="submit" name="setting_submit" id="setting_submit" class="button button-primary" value="Save Changes"></p>



</form>

</div>





