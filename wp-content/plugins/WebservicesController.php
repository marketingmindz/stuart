<?php

App::uses('AppController', 'Controller');

class WebservicesController extends AppController
{
    public $uses = array('User', 'Shift', 'Sale', 'ShiftRequest', "EmailTemplate");

    public function beforeFilter()
    {
        parent::beforeFilter();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: Authorization, Content-Type');
    }

    public function login()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {

            $userEmail = $this->request->data['email'];
            $checkUser = $this->User->find("first", array('conditions' => array("User.email" => $userEmail)));

            if (!empty($checkUser)) {
                if ($checkUser['User']['status'] == 1) {
                    $response = array("status" => true,
                        "userID"                   => $checkUser['User']['id'],
                        "message"                  => "Success",
                        "error"                    => false);
                } else {
                    $response = array("status" => false,
                        "message"                  => "Your account not activated.Plase active your account.",
                        "error"                    => false);
                }
            } else {
                $response = array("status" => false,
                    "message"                  => "Invalid your enter email address!.",
                    "error"                    => false);
            }
        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => false);
        }
        echo json_encode($response);
    }

    public function userSignup()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $email                                = $this->request->data['email'];
            $this->request->data['User']['email'] = $email;
            $this->request->data['User']['role']  = 2;
            $this->request->data['User']['token'] = md5($email);
            $link                                 = SITE_URL . '/webservices/activeAccount/' . md5($email);
            $checkUser                            = $this->User->find("first", array('conditions' => array("User.email" => $email)));
            if (!empty($checkUser)) {
                if ($checkUser['User']['status'] == 1) {
                    if (!empty($checkUser['User']['image'])) {
                        $imagePath = SITE_URL . '/uploads/' . $checkUser['User']['image'];
                    } else {
                        $imagePath = '/img/user-profile.png';
                    }
                    $response = array(
                        "status"    => true,
                        "userID"    => $checkUser['User']['id'],
                        "message"   => "Login successfull",
                        "error"     => false,
                        "login"     => "done",
                        "firstName" => $checkUser['User']['first_name'],
                        "lastName"  => $checkUser['User']['last_name'],
                        'url'       => $imagePath,
                    );
                } else {
                    $response = array(
                        "status"  => false,
                        "message" => "The email you provided is already registered, please verify email to join.",
                        "error"   => false,
                        "login"   => "pending",
                    );
                }
            } else {
                if ($this->User->save($this->request->data)) {
                    $admin_email = Configure::read('App.EmailFrom');
                    $template    = $this->EmailTemplate->findBySlug("user_registration");
                    $mailMessage = str_replace(array('{username}', '{admin_mail}', '{link}'), array($email, $admin_email, $link), $template['EmailTemplate']['description']);
                    $this->set("mailMessage", $mailMessage);
                    $this->Email->to      = $email;
                    $this->Email->subject = $template['EmailTemplate']['subject'];
                    $this->Email->from    = Configure::read('App.EmailFrom');
                    $this->Email->sendAs  = 'html';
                    $this->Email->send($mailMessage);

                    $response = array(
                        "status"       => true,
                        "message"      => "Thanks for registering. Please check your email inbox to verify your account.",
                        "error"        => false,
                        "registration" => "done",
                        "login"        => "registration",
                    );
                } else {
                    $response = array("status" => false,
                        "message"                  => "Something went wrong, please try again later.",
                        "error"                    => true,
                        "registration"             => "failed",
                        "login"                    => "registration",
                    );
                }
            }
        } else {
            $response = array(
                "status"  => false,
                "message" => "You are not authorized to access this api.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }

    public function verifiedEmail()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $email     = $this->request->data['email'];
            $checkUser = $this->User->find(
                "first",
                array(
                    'conditions' => array(
                        "User.email" => $email,
                    ),
                )
            );
            if (!empty($checkUser)) {
                if ($checkUser['User']['status'] == 1) {
                    if (!empty($checkUser['User']['image'])) {
                        $imagePath = SITE_URL . '/uploads/' . $checkUser['User']['image'];
                    } else {
                        $imagePath = '/img/user-profile.png';
                    }
                    $response = array(
                        "status"    => true,
                        "userID"    => $checkUser['User']['id'],
                        "message"   => "Login successfull",
                        "error"     => false,
                        "login"     => "done",
                        "firstName" => $checkUser['User']['first_name'],
                        "lastName"  => $checkUser['User']['last_name'],
                        "url"       => $imagePath,
                    );
                } else {
                    $response = array(
                        "status"  => false,
                        "message" => "Your email is not verified yet, please verify email to join.",
                        "error"   => true,
                    );
                }
            } else {
                $response = array(
                    "status"  => false,
                    "message" => "This email is not registerd yet. Please register.",
                    "error"   => true,
                );
            }
        } else {
            $response = array(
                "status"  => false,
                "message" => "You are not authorized to access this api.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }

    public function NotVeryfied()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $email      = $this->request->data['email'];
            $userdata   = array('status' => 2);
            $conditions = array(
                'User.email' => $email,
            );
            $this->User->updateAll(
                $userdata,
                $conditions
            );
            $link        = SITE_URL . '/webservices/activeAccount/' . md5($email);
            $admin_email = Configure::read('App.EmailFrom');
            $template    = $this->EmailTemplate->findBySlug("email_verify");
            $mailMessage = str_replace(array('{username}', '{admin_mail}', '{link}'), array($email, $admin_email, $link), $template['EmailTemplate']['description']);
            $this->set("mailMessage", $mailMessage);
            $this->Email->to      = $email;
            $this->Email->subject = $template['EmailTemplate']['subject'];
            $this->Email->from    = Configure::read('App.EmailFrom');
            $this->Email->sendAs  = 'html';
            $this->Email->send($mailMessage);
            $response = array(
                "status"  => true,
                "message" => "updated",
                "error"   => false,
            );
        } else {
            $response = array(
                "status"  => false,
                "message" => "You are not authorized to access this api.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }

    public function resendEmail()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $email       = $this->request->data['email'];
            $link        = SITE_URL . '/webservices/activeAccount/' . md5($email);
            $admin_email = Configure::read('App.EmailFrom');
            $template    = $this->EmailTemplate->findBySlug("user_registration");
            $mailMessage = str_replace(array('{username}', '{admin_mail}', '{link}'), array($email, $admin_email, $link), $template['EmailTemplate']['description']);
            $this->set("mailMessage", $mailMessage);
            $this->Email->to      = $email;
            $this->Email->subject = $template['EmailTemplate']['subject'];
            $this->Email->from    = Configure::read('App.EmailFrom');
            $this->Email->sendAs  = 'html';
            $this->Email->send($mailMessage);

            $response = array(
                "status"  => true,
                "message" => "Verification email has been sent, please check your inbox.",
                "error"   => false,
                "resend"  => "done",
            );
        } else {
            $response = array(
                "status"  => false,
                "message" => "You are not authorized to access this api.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }

    public function userDetail()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {

            $userID = $this->request->data['userID'];

            $userDetail = $this->User->find("first", array('conditions' => array("User.id" => $userID)));

            if (!empty($userDetail)) {
                 if(!empty($userDetail['User']['image'])) {
                      $imagePath = SITE_URL.'/uploads/'.$userDetail['User']['image'];
                    }else{
                      $imagePath = SITE_URL.'/img/user.png';
                    } 
                $response = array("status" => true,
                    "resp"                     => $userDetail['User'],
                    "userImage"                => $imagePath,
                    "message"                  => "Success",
                    "error"                    => false);
            } else {
                $response = array("status" => false,
                    "message"                  => "User Detail not found!.",
                    "error"                    => false);
            }
        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => false);
        }
        echo json_encode($response);
    }

    public function activeAccount($token = null)
    {
        $this->autoRender = false;
        if (!empty($token)) {

            $activeAccount = $this->User->find("first", array('conditions' => array("User.token" => $token)));

            if (!empty($activeAccount)) {
                $this->User->id = $activeAccount['User']['id'];
                $this->User->saveField('status', 1);
                echo "Your account has activated.";die;
            } else {
                $response = array("status" => false,
                    "message"                  => "You can't activet your account.",
                    "error"                    => false);
            }
        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => false);
        }
        echo json_encode($response);
    }

    public function userUpdate()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $userID         = $this->request->data['userID'];
            $this->User->id = $userID;
            if ($this->User->save($this->request->data)) {

                $response = array("status" => true,
                    "message"                  => "Your informastion successfully Updated.",
                    "error"                    => false,
                );
            } else {
                $response = array("status" => false,
                    "message"                  => "Your informastion could not updated.Please try again.",
                    "error"                    => true,
                );
            }

        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => true);
        }
        echo json_encode($response);
    }
    //=============shift list functions======================//
    public function shiftList()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            //pr($this->request->data);
            $userID      = $this->request->data['userID'];
            $currentDate = $this->request->data['currentDate'];
            $onSale      = $this->request->data['onSale'];
            //echo "dfasdfasf".$onSale."dgsdgfsa";
            $currentDateArray = explode("-", $currentDate);
            $month            = $currentDateArray[0];
            $year             = $currentDateArray[1];
            $myShift          = $this->monthShiftsData($userID, $month, $year);
            $myShiftOnSale    = $this->myShiftOnSale($userID, $month, $year);
            if ($onSale == "true") {
                $onSale = $this->othershiftonsaleMonth($userID, $month, $year);
                $data   = array(
                    "myShift"            => $myShift,
                    "myShiftOnSale"      => $myShiftOnSale,
                    "monthothersalelist" => $onSale,
                );
            } else {
                $data = array(
                    "myShift"       => $myShift,
                    "myShiftOnSale" => $myShiftOnSale,
                );
            }
            echo json_encode($data);
        }
    }

    public function othershiftonsaleMonth($userID, $month, $year)
    {
        $startDate = $year . "-" . $month . "-01";
        $endDate   = $year . "-" . $month . "-31";

        $this->Shift->bindModel(
            array(
                'belongsTo' => array(
                    'User' => array(
                        'className'  => 'User',
                        'foreignKey' => 'user_id',
                    ),
                ),
            )
        );

        $saleList = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    'NOT'        => array(
                        'Shift.user_id' => array($userID),
                    ),
                    "Shift.type" => 2,
                    'and'        => array(
                        array(
                            'Shift.shift_date <= ' => $endDate,
                            'Shift.shift_date >= ' => $startDate,
                        ),
                    ),
                ),
                'order'      => array(
                    'Shift.shift_date' => 'ASC',
                ),
            )
        );

        $myshiftlist = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    'Shift.user_id' => array($userID),
                    "Shift.type"    => 1,
                    'and'           => array(
                        array(
                            'Shift.shift_date <= ' => $endDate,
                            'Shift.shift_date >= ' => $startDate,
                        ),
                    ),
                ),
                'order'      => array(
                    'Shift.shift_date' => 'ASC',
                ),
            )
        );
        foreach ($myshiftlist as $i_index => $i_value) {
            $myshiftsdate_arr[] = $i_value['Shift']['shift_date'];
        }
        if (empty($saleList)) {
            $response = array(
                "sale_status" => false,
                "message"     => "No shift found.",
                "error"       => true,
            );
        } else {
            $response['sale_status'] = true;
            $response['message']     = 'success';
            $response['error']       = 'false';
            foreach ($saleList as $key => $val) {
                if (in_array(substr($val['Shift']['shift_date'], 0, 10), $myshiftsdate_arr)) {
                    $myshift_exist = true;
                } else {
                    $myshift_exist = false;
                }

                $this->ShiftRequest->bindModel(
                    array(
                        'belongsTo' => array(
                            'Shift' => array(
                                'className'  => 'Shift',
                                'foreignKey' => 'shift_id',
                            ),
                        ),
                    )
                );

                $checkRequest = $this->ShiftRequest->find(
                    "all",
                    array(
                        "conditions" => array(
                            "ShiftRequest.user_id"  => $userID,
                            "ShiftRequest.shift_id" => $val['Shift']['id'],
                        ),
                    )
                );
                $shiftType='';
                if (!empty($checkRequest) && $checkRequest[0]['Shift']['shift_date'] == $val['Shift']['shift_date']) {
                    $already_bought = true;
                    if($checkRequest[0]['ShiftRequest']['type']=='buy') {
                        $shiftType = 'buy';
                    }else{
                        $shiftType = 'exchange';
                    }
                }else{
                    $already_bought = false;
                }

                if (!empty($val['Shift']['id'])) {

                    if(!empty($val['User']['image'])) {
                         $imagePath = SITE_URL.'/uploads/'.$val['User']['image'];
                    }else{
                         $imagePath = SITE_URL.'/img/user.png';
                    } 
                    $date                    = date("M j D", strtotime($val['Shift']['shift_date']));
                    $dateFormat              = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                    $date_arr                = explode(" ", $date);
                    $response['status']      = true;
                    $response['sale_resp'][] = array(
                        "shiftID"       => $val['Shift']['id'],
                        "month"         => strtoupper($date_arr[0]),
                        "day"           => $date_arr[1],
                        "date"          => $date_arr[2],
                        "userID"        => $val['Shift']['user_id'],
                        "doctor_name"   => $val['User']['first_name'],
                        "doctor_last_name"   => $val['User']['last_name'],
                        "userImage"     => $imagePath,
                        "fullDate"      => substr($dateFormat, 0, 10),
                        "shift_date"    => substr($val['Shift']['shift_date'], 0, 10),
                        "myshift_exist" => $myshift_exist,
                        "already_bought"=> $already_bought,
                        "shiftType"     =>$shiftType
                    );
                }

            }
        }
        return $response;
    }

    public function monthShiftsData($userID, $month, $year)
    {
        $startDate        = $year . "-" . $month . "-01";
        $endDate          = $year . "-" . $month . "-31";
        $this->autoRender = false;
        $list             = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id" => $userID,
                    'and'           => array(
                        array(
                            'Shift.shift_date <= ' => $endDate,
                            'Shift.shift_date >= ' => $startDate,
                        ),
                    ),
                ),
                'order'      => array('Shift.shift_date' => 'ASC'),
            )
        );

        if (empty($list)) {
            $response = array(
                "status"  => false,
                "message" => "No shift found for this month.",
                "error"   => true,
            );
        } else {
            $response['status']  = true;
            $response['message'] = 'success';
            $response['error']   = 'false';
            foreach ($list as $key => $val) {
                if ($val['Shift']['type'] == 2) {
                    $checkRequest = $this->ShiftRequest->find(
                        "count",
                        array(
                            "conditions" => array(
                                "ShiftRequest.user_id"     => $userID,
                                "ShiftRequest.exchange_id" => $val['Shift']['id'],
                            ),
                        )
                    );
                }else{
                    $checkRequest = 0;
                }
                    
                if ($checkRequest < 1) {
                    $date                   = date("M j D", strtotime($val['Shift']['shift_date']));
                    $dateFormat             = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                    $date_arr               = explode(" ", $date);
                    $response['resp'][$key] = array("shiftID" => $val['Shift']['id'],
                        "month"                                   => strtoupper($date_arr[0]),
                        "day"                                     => $date_arr[1],
                        "date"                                    => $date_arr[2],
                        "userID"                                  => $val['Shift']['user_id'],
                        "fullDate"                                => substr($dateFormat, 0, 10),
                        "shift_date"                              => substr($val['Shift']['shift_date'], 0, 10),
                        "shift_type"                              => $val['Shift']['type'],
                    );
                }
            }
        }
        return $response;
    }

    public function myshift($userID, $month, $year)
    {
        $startDate        = $year . "-" . $month . "-01";
        $endDate          = $year . "-" . $month . "-31";
        $this->autoRender = false;
        $list             = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id" => $userID,
                    "Shift.type"    => 1,
                    'and'           => array(
                        array(
                            'Shift.shift_date <= ' => $endDate,
                            'Shift.shift_date >= ' => $startDate,
                        ),
                    ),

                ),
                'order'      => array('Shift.shift_date' => 'ASC'),
            )
        );

        if (empty($list)) {
            $response = array(
                "status"  => false,
                "message" => "No shift found for this month.",
                "error"   => true,
            );
        } else {
            $response['status']  = true;
            $response['message'] = 'success';
            $response['error']   = 'false';
            foreach ($list as $key => $val) {
                $checkRequest = $this->ShiftRequest->find(
                    "count",
                    array(
                        "conditions" => array(
                            "ShiftRequest.user_id"     => $userID,
                            "ShiftRequest.exchange_id" => $val['Shift']['id'],
                        ),
                    )
                );
                if ($checkRequest < 1) {
                    $date                   = date("M j D", strtotime($val['Shift']['shift_date']));
                    $dateFormat             = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                    $date_arr               = explode(" ", $date);
                    $response['resp'][$key] = array("shiftID" => $val['Shift']['id'],
                        "month"                                   => strtoupper($date_arr[0]),
                        "day"                                     => $date_arr[1],
                        "date"                                    => $date_arr[2],
                        "userID"                                  => $val['Shift']['user_id'],
                        "fullDate"                                => substr($dateFormat, 0, 10),
                        "shift_date"                              => substr($val['Shift']['shift_date'], 0, 10),
                    );
                }
            }
        }
        return $response;
    }

    public function myShiftOnSale($userID, $month, $year)
    {
        $startDate        = $year . "-" . $month . "-01";
        $endDate          = $year . "-" . $month . "-31";
        $this->autoRender = false;

        $saleList = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id" => $userID,
                    "Shift.type"    => 2,
                    'and'           => array(
                        array(
                            'Shift.shift_date <= ' => $endDate,
                            'Shift.shift_date >= ' => $startDate,
                        ),
                    ),
                ),
                'order'      => array('Shift.shift_date' => 'ASC'),
            )
        );

        $day   = '';
        $month = '';

        if (empty($saleList)) {
            $response = array(
                "my_sale_status" => false,
                "message"        => "No shift found.",
                "error"          => true,
            );
        } else {
            $response['my_sale_status'] = true;
            $response['message']        = 'success';
            $response['error']          = 'false';
            foreach ($saleList as $key => $val) {

                $this->ShiftRequest->bindModel(
                    array(
                        'belongsTo' => array(
                            'User'  => array(
                                'className'  => 'User',
                                'foreignKey' => 'user_id',
                            ),
                            'Shift' => array(
                                'className'  => 'Shift',
                                'foreignKey' => 'exchange_id',
                            ),
                        ),
                    )
                );

                $requestList = $this->ShiftRequest->find(
                    "all",
                    array(
                        "conditions" => array(
                            'ShiftRequest.shift_id' => $val['Shift']['id'],
                        ),

                    )
                );

                if(!empty($requestList)) {

                   $requestExist = true;
 
                }else{
                   $requestExist = false;
                }

                $resp = array();

                foreach ($requestList as $key => $valu) {
                    if ($valu['ShiftRequest']['type'] == 'exchange') {
                        $ExchDate = date("M j D", strtotime($valu['Shift']['shift_date']));
                        $dateArr  = explode(" ", $ExchDate);
                        $day      = $dateArr[1];
                        $month    = strtoupper($dateArr[0]);
                    } else {
                        $day   = '';
                        $month = '';
                    }
                    if(!empty($valu['User']['image'])) {
                             $imagePath = SITE_URL.'/uploads/'.$valu['User']['image'];
                        }else{
                             $imagePath = SITE_URL.'/img/user.png';
                        }
                    $resp[] = array("doctor_name" => $valu['User']['first_name'],
                         "doctor_last_name"   => $valu['User']['last_name'],
                        "userImage"                   =>$imagePath,
                        "type"                        => $valu['ShiftRequest']['type'],
                        "requestID"                   => $valu['ShiftRequest']['id'],
                        "month"                       => $month,
                        "day"                         => $day);
                }

                $date       = date("M j D", strtotime($val['Shift']['shift_date']));
                $dateFormat = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                $date_arr   = explode(" ", $date);

                $response['my_sale_resp'][] = array(
                    "shiftID"         => $val['Shift']['id'],
                    "month"           => strtoupper($date_arr[0]),
                    "day"             => $date_arr[1],
                    "date"            => $date_arr[2],
                    "fullDate"        => substr($dateFormat, 0, 10),
                    'is_request'      => $requestExist,
                    "mySaleShiftresp" => $resp,
                );
            }

        }
        return $response;
    }
    //================end shift list functions================//

    //====================================current date shifts============================//
    public function currentDateShiftList()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $userID           = $this->request->data['userID'];
            $currentDate      = $this->request->data['currentDate'];
            $todayDate        = date("Y-m-d", strtotime($currentDate));
            $currentDateArray = explode("-", $currentDate);
            $date             = $currentDateArray[0];
            $month            = $currentDateArray[1];
            $year             = $currentDateArray[2];
            $myShift          = $this->currentDatemyshift($userID, $date, $month, $year);
            $myShiftOnSale    = $this->currentDatemyShiftOnSale($userID, $date, $month, $year);
            $othershiftonsale = $this->currentDateOtherShiftOnSale($todayDate, $userID);
            $data             = array(
                "myShift"          => $myShift,
                "myShiftOnSale"    => $myShiftOnSale,
                "otherShiftOnSale" => $othershiftonsale,
            );
          //  pr($othershiftonsale);
           // die;
            echo json_encode($data);
        }
    }

    public function currentDatemyshift($userID, $date, $month, $year)
    {
        $todayDate        = $year . "-" . $month . "-" . $date;
        $this->autoRender = false;
        $list             = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id"    => $userID,
                    "Shift.type"       => 1,
                    'Shift.shift_date' => $todayDate,
                ),
            )
        );

        $list             = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id"    => $userID,
                    "Shift.type"       => 1,
                    'Shift.shift_date' => $todayDate,
                ),
            )
        );

        $this->ShiftRequest->bindModel(
            array(
                'belongsTo' => array(
                    'Shift' => array(
                        'className'  => 'Shift',
                        'foreignKey' => 'shift_id',
                    ),
                ),
            )
        );

        $pendingshifts = $this->ShiftRequest->find(
            "all",
            array(
                "conditions" => array(
                    "ShiftRequest.user_id"    => $userID,
                ),
            )
        );

        $we_have_pending_here = false;
        $type_of_pending_shift = null;

        foreach ($pendingshifts as $penKey => $penValue) {
            if ($penValue['Shift']['shift_date'] == $todayDate) {
                $we_have_pending_here = true;
                $type_of_pending_shift = $penValue['ShiftRequest']['type'];
                break;
            }else{
                $we_have_pending_here = false;
                $type_of_pending_shift = null;
            }
        }

        if (empty($list)) {
            $response = array(
                "status"  => false,
                "message" => "noShifts",
                "error"   => true,
            );
            $response["we_have_pending_here"] = $we_have_pending_here;
            $response["type_of_pending_shift"] = $type_of_pending_shift;
        } else {
            $response['status']  = true;
            $response['message'] = 'found shifts';
            $response['error']   = 'false';
            foreach ($list as $key => $val) {
                $date       = date("M j D", strtotime($val['Shift']['shift_date']));
                $dateFormat = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                $date_arr   = explode(" ", $date);

                $response['resp']["shiftID"]    = $val['Shift']['id'];
                $response['resp']["month"]      = strtoupper($date_arr[0]);
                $response['resp']["day"]        = $date_arr[1];
                $response['resp']["date"]       = $date_arr[2];
                $response['resp']["userID"]     = $val['Shift']['user_id'];
                $response['resp']["fullDate"]   = substr($dateFormat, 0, 10);
                $response['resp']["shift_date"] = substr($val['Shift']['shift_date'], 0, 10);
            }
        }
        return $response;
    }

    public function currentDatemyShiftOnSale($userID, $date, $month, $year)
    {
        $todayDate        = $year . "-" . $month . "-" . $date;
        $this->autoRender = false;

        $saleList = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    "Shift.user_id"    => $userID,
                    "Shift.type"       => 2,
                    'Shift.shift_date' => $todayDate,
                ),
            )
        );

        $day   = '';
        $month = '';

        if (empty($saleList)) {
            $response = array(
                "my_sale_status" => false,
                "message"        => "No shift found.",
                "error"          => true,
            );
        } else {
            $response['my_sale_status'] = true;
            $response['message']        = 'success';
            $response['error']          = 'false';
            foreach ($saleList as $key => $val) {

                $this->ShiftRequest->bindModel(
                    array(
                        'belongsTo' => array(
                            'User'  => array(
                                'className'  => 'User',
                                'foreignKey' => 'user_id',
                            ),
                            'Shift' => array(
                                'className'  => 'Shift',
                                'foreignKey' => 'exchange_id',
                            ),
                        ),
                    )
                );

                $requestList = $this->ShiftRequest->find(
                    "all",
                    array(
                        "conditions" => array(
                            'ShiftRequest.shift_id' => $val['Shift']['id'],
                        ),
                    )
                );

                $resp = array();

                foreach ($requestList as $key => $valu) {
                    if ($valu['ShiftRequest']['type'] == 'exchange') {
                        $ExchDate = date("M j D", strtotime($valu['Shift']['shift_date']));
                        $dateArr  = explode(" ", $ExchDate);
                        $day      = $dateArr[1];
                        $month    = strtoupper($dateArr[0]);
                    } else {
                        $day   = '';
                        $month = '';
                    }
                     if(!empty($valu['User']['image'])) {
                             $imagePath = SITE_URL.'/uploads/'.$valu['User']['image'];
                        }else{
                             $imagePath = SITE_URL.'/img/user.png';
                        }
                    $resp[] = array("doctor_name" => $valu['User']['first_name'],
                         "doctor_last_name"   => $valu['User']['last_name'],
                        "userImage"                   => $imagePath,
                        "type"                        => $valu['ShiftRequest']['type'],
                        "requestID"                   => $valu['ShiftRequest']['id'],
                        "month"                       => $month,
                        "day"                         => $day);
                }

                $date       = date("M j D", strtotime($val['Shift']['shift_date']));
                $dateFormat = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                $date_arr   = explode(" ", $date);

                $response['my_sale_resp'][] = array(
                    "shiftID"         => $val['Shift']['id'],
                    "month"           => strtoupper($date_arr[0]),
                    "day"             => $date_arr[1],
                    "date"            => $date_arr[2],
                    "fullDate"        => substr($dateFormat, 0, 10),
                    "mySaleShiftresp" => $resp,
                );
            }

        }
        return $response;
    }

    public function currentDateOtherShiftOnSale($todayDate, $userID)
    {
        $this->autoRender=false;

        $this->Shift->bindModel(
            array(
                'belongsTo' => array(
                    'User' => array(
                        'className'  => 'User',
                        'foreignKey' => 'user_id',
                    ),
                ),
            )
        );

        $saleList = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    'NOT'              => array(
                        'Shift.user_id' => $userID,
                    ),
                    "Shift.type"       => 2,
                    'Shift.shift_date' => $todayDate,
                ),
                'order'      => array(
                    'Shift.shift_date' => 'ASC',
                ),
            )
        );

        $myshiftlist = $this->Shift->find(
            "all",
            array(
                "conditions" => array(
                    'Shift.user_id'    => $userID,
                    "Shift.type"       => 1,
                    'Shift.shift_date' => $todayDate,
                ),
                'order'      => array(
                    'Shift.shift_date' => 'ASC',
                ),
            )
        );

        if (!empty($myshiftlist)) {
            foreach ($myshiftlist as $i_index => $i_value) {
                $myshiftsdate_arr[] = $i_value['Shift']['shift_date'];
            }
        }
        $req_type_exchange='';
        $req_exchange_date=''; 
        foreach ($saleList as $key => $val) {
            
            if (!empty($myshiftlist) && in_array(substr($val['Shift']['shift_date'], 0, 10), $myshiftsdate_arr)) {
                $myshift_exist = true;
            } else {
                $myshift_exist = false;
            }

            $this->ShiftRequest->bindModel(
                array(
                    'belongsTo' => array(
                        'Shift' => array(
                            'className'  => 'Shift',
                            'foreignKey' => 'shift_id',
                        ),
                    ),
                )
            );

            $checkRequest = $this->ShiftRequest->find(
                "all",
                array(
                    "conditions" => array(
                        "ShiftRequest.user_id"  => $userID,
                        "ShiftRequest.shift_id" => $val['Shift']['id'],
                    ),
                )
            );

            if (!empty($checkRequest) && $checkRequest[0]['Shift']['shift_date'] == $val['Shift']['shift_date']) {
                $already_bought = true;
                if ($checkRequest[0]['ShiftRequest']['type'] == "exchange") {
                    $req_type_exchange = true;
                    $req_exchange_id = $checkRequest[0]['ShiftRequest']['exchange_id'];
                    $req_exchange_date_arr = $this->Shift->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Shift.id' => $req_exchange_id
                            )
                        )
                    );
                    $time = strtotime($req_exchange_date_arr['Shift']['shift_date']);
                    $newformat = date('d M',$time);

                    $req_exchange_date = $newformat;
                }else{
                    $req_type_exchange = false;
                    $req_exchange_date = null;
                }
            }else{
                $already_bought = false;
            }

            if (!empty($val['Shift']['id'])) {
                if(!empty($val['User']['image'])) {
                         $imagePath = SITE_URL.'/uploads/'.$val['User']['image'];
                    }else{
                         $imagePath = SITE_URL.'/img/user.png';
                    }
                $date       = date("M j D", strtotime($val['Shift']['shift_date']));
                $dateFormat = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                $date_arr   = explode(" ", $date);
                $list[]     = array(
                    "shiftID"       => $val['Shift']['id'],
                    "month"         => strtoupper($date_arr[0]),
                    "day"           => $date_arr[1],
                    "date"          => $date_arr[2],
                    "userID"        => $val['Shift']['user_id'],
                    "doctor_name"   => $val['User']['first_name'],
                     "doctor_last_name"   => $val['User']['last_name'],
                    "userImage"     => $imagePath,
                    "fullDate"      => substr($dateFormat, 0, 10),
                    "shift_date"    => substr($val['Shift']['shift_date'], 0, 10),
                    "myshift_exist" => $myshift_exist,
                    "already_bought"=> $already_bought,
                    "req_type_exchange" => $req_type_exchange,
                    "req_exchange_date" => $req_exchange_date
                );
            }
        }
 
        if (!empty($list)) {
            
            return $list;
        } else {
            $list = array();
            return $list;
        }
    }
    //============================end current date shifts================================//

    public function shiftSaleList()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {

            $userID           = $this->request->data['userID'];
            $currentDate      = $this->request->data['currentDate'];
            $currentDateArray = explode("-", $currentDate);
            $month            = $currentDateArray[0];
            $year             = $currentDateArray[1];
            $startDate        = $year . "-" . $month . "-01";
            $endDate          = $year . "-" . $month . "-31";

            $this->Shift->bindModel(
                array(
                    'belongsTo' => array(
                        'User' => array(
                            'className'  => 'User',
                            'foreignKey' => 'user_id',
                        ),
                    ),
                )
            );

            $saleList = $this->Shift->find(
                "all",
                array(
                    "conditions" => array(
                        'NOT'        => array(
                            'Shift.user_id' => array($userID),
                        ),
                        "Shift.type" => 2,
                        'and'        => array(
                            array(
                                'Shift.shift_date <= ' => $endDate,
                                'Shift.shift_date >= ' => $startDate,
                            ),
                        ),
                    ),
                    'order'      => array(
                        'Shift.shift_date' => 'ASC',
                    ),
                )
            );

            $myshiftlist = $this->Shift->find(
                "all",
                array(
                    "conditions" => array(
                        'Shift.user_id' => array($userID),
                        "Shift.type"    => 1,
                        'and'           => array(
                            array(
                                'Shift.shift_date <= ' => $endDate,
                                'Shift.shift_date >= ' => $startDate,
                            ),
                        ),
                    ),
                    'order'      => array(
                        'Shift.shift_date' => 'ASC',
                    ),
                )
            );
            foreach ($myshiftlist as $i_index => $i_value) {
                $myshiftsdate_arr[] = $i_value['Shift']['shift_date'];
            }
            if (empty($saleList)) {
                $response = array(
                    "sale_status" => false,
                    "message"     => "No shift found.",
                    "error"       => true,
                );
            } else {
                $response['sale_status'] = true;
                $response['message']     = 'success';
                $response['error']       = 'false';
                foreach ($saleList as $key => $val) {
                    if (in_array(substr($val['Shift']['shift_date'], 0, 10), $myshiftsdate_arr)) {
                        $myshift_exist = true;
                    } else {
                        $myshift_exist = false;
                    }
                    $checkRequest = $this->ShiftRequest->find(
                        "count",
                        array(
                            "conditions" => array(
                                "ShiftRequest.user_id"  => $userID,
                                "ShiftRequest.shift_id" => $val['Shift']['id'],
                            ),
                        )
                    );
                    if ($checkRequest < 1) {
                        if (!empty($val['Shift']['id'])) {
                            if(!empty($val['User']['image'])) {
                             $imagePath = SITE_URL.'/uploads/'.$val['User']['image'];
                        }else{
                             $imagePath = SITE_URL.'/img/user.png';
                        }
                            $date                    = date("M j D", strtotime($val['Shift']['shift_date']));
                            $dateFormat              = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                            $date_arr                = explode(" ", $date);
                            $response['sale_resp'][] = array(
                                "shiftID"       => $val['Shift']['id'],
                                "month"         => strtoupper($date_arr[0]),
                                "day"           => $date_arr[1],
                                "date"          => $date_arr[2],
                                "userID"        => $val['Shift']['user_id'],
                                "userImage"     =>$imagePath,
                                "doctor_name"   => $val['User']['first_name'],
                                 "doctor_last_name"   => $val['User']['last_name'],
                                "fullDate"      => substr($dateFormat, 0, 10),
                                "shift_date"    => substr($val['Shift']['shift_date'], 0, 10),
                                "myshift_exist" => $myshift_exist,
                            );
                        }
                    }
                }
            }
            echo json_encode($response);
        }
    }

    public function shiftAdd()
    {
        $this->autoRender = false;

        if ($this->request->is("post")) {
            $dateFormat = date("Y-m-d", strtotime($this->request->data['shift_date']));
            $checkShift = $this->Shift->find("first", array("conditions" => array("Shift.shift_date" => $dateFormat, "Shift.user_id" => $this->request->data['user_id'])));
            if (empty($checkShift)) {

                $OnMyShift = $this->Shift->find("first", array("conditions" => array("Shift.shift_date" => $dateFormat, "Shift.user_id" => $this->request->data['user_id'], "Shift.type" => 2)));
                if (empty($OnMyShift)) {
                    $data = array("user_id" => $this->request->data['user_id'], "shift_date" => $dateFormat);
                    if ($this->Shift->save($data)) {
                        $response = array("status" => true,
                            "message"                  => "You shift saved successfully.",
                            "error"                    => false,
                        );
                    } else {
                        $response = array("status" => false,
                            "message"                  => "You informastion could not updated.Please try again.",
                            "error"                    => true,
                        );
                    }
                } else {
                    $response = array("status" => false,
                        "message"                  => "You have already given shift on this date.Please try another.",
                        "error"                    => true,
                    );
                }

            } else {
                $response = array("status" => false,
                    "message"                  => "You have already added shift on this date.Please try another.",
                    "error"                    => true,
                );

            }

        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => true);
        }
        echo json_encode($response);
    }

    public function shiftDelete()
    {
        $this->autoRender = false;

        if ($this->request->is("post")) {
            $id = $this->request->data['id'];
            if ($this->Shift->delete($id)) {
                $response = array(
                    "status"  => true,
                    "message" => "Shift deleted successfully.",
                    "error"   => false,
                );
            } else {
                $response = array(
                    "status"  => false,
                    "message" => "Shift cannot be deleted.Please try again.",
                    "error"   => true,
                );
            }
        } else {
            $response = array(
                "status"  => false,
                "message" => "You can't access!.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }

    public function saleShift()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $userID  = $this->request->data['userID'];
            $shiftID = $this->request->data['shiftID'];
            $shiftDATE = $this->Shift->field('shift_date',array('id'=>$shiftID));

            if (!empty($userID) && !empty($shiftID)) {
                $data = array("user_id" => $userID, "shift_id" => $shiftID);
                if ($this->Sale->save($data)) {
                    
                    $this->Shift->id = $shiftID;
                    $this->Shift->saveField('type', 2);
                    $response = array("status" => true,
                        "message"                  => "Your shift sale successfully.",
                        "error"                    => false,
                    );  
                    echo json_encode($response);
                   $this->sendPushForSaleShift($userID, $shiftDATE);  
                   exit;
                }  

            } else {
                $response = array("status" => false,
                    "message"                  => "shift not sale ",
                    "error"                    => false,
                ); 
            }
            echo json_encode($response);

        }
    }

    public function shiftBuyExchange()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $userID =  $this->request->data['user_id'];
            $shiftID =  $this->request->data['shift_id'];
            $type =  $this->request->data['type'];
            $shiftDATE = $this->Shift->field('shift_date',array('id'=>$shiftID));
            
            $check = $this->ShiftRequest->find("count", array("conditions" => array("ShiftRequest.user_id" =>$userID,"ShiftRequest.shift_id" =>$shiftID,"ShiftRequest.type" =>$type )));
            if ($check == 0) {
                if ($this->ShiftRequest->save($this->request->data)) {
                    $response = array("status" => true,"message"=> "success","error" => false);
                    echo json_encode($response);
                    $this->buyAndExchangeSendPush($userID,$shiftID,$type,$shiftDATE); die;
                } else {
                    $response = array("status" => false,"message"=> "error","error" => true);
                }

            } else {
                $response = array("status" => false,"message" => "error","error" => true);
            }

            echo json_encode($response);

        }

    }

    public function requestDelete()
    {
        $this->autoRender = false;

        if ($this->request->is("post")) {
            $id = $this->request->data['id'];
            if ($this->ShiftRequest->delete($id)) {
                $response = array("status" => true,
                    "message"                  => "Request deleted successfully.",
                    "error"                    => false,
                );
            } else {
                $response = array("status" => false,
                    "message"                  => "Request cannot be deleted.Please try again.",
                    "error"                    => true,
                );
            }
        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => true);
        }
        echo json_encode($response);
    }

    public function shiftDetail()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $type   = $this->request->data['stype'];
            $date   = $this->request->data['date'];
            $userID = $this->request->data['userID'];

            $shiftdate   = date("Y-m-d", strtotime($date));
            $shiftDetail = $this->Shift->find(
                "first",
                array(
                    'conditions' => array(
                        "Shift.type"       => 1,
                        "Shift.shift_date" => $shiftdate,
                        "Shift.user_id"    => $userID,
                    ),
                )
            );

            if (empty($shiftDetail)) {
                $response['myshift_today']['status'] = false;
                $response['myshift_today']['date']   = $date;
            } else {
                $response['myshift_today']['status']  = true;
                $response['myshift_today']['message'] = 'success';
                $response['myshift_today']['error']   = false;
                $date                                 = date("M j D", strtotime($shiftDetail['Shift']['shift_date']));
                $date_arr                             = explode(" ", $date);
                $response['myshift_today']["shiftID"] = $shiftDetail['Shift']['id'];
                $response['myshift_today']["month"]   = strtoupper($date_arr[0]);
                $response['myshift_today']["day"]     = $date_arr[1];
                $response['myshift_today']["date"]    = $date_arr[2];
            }

            $response['today_sale_list'] = $this->currentDateOtherShiftOnSale($shiftdate, $userID);
        }
        echo json_encode($response);
    }

    //===========//
    public function shiftDetail_old()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $type   = $this->request->data['stype'];
            $date   = $this->request->data['date'];
            $userID = $this->request->data['userID'];

            if ($type == 'myShift') {
                $response                    = $this->myshiftDetail($date, $userID);
                $shiftdate                   = date("Y-m-d", strtotime($date));
                $response['today_sale_list'] = $this->shifts_on_sale_today($shiftdate, $userID);
            }

            if ($type == 'otherSale') {
                $shiftdate = date("Y-m-d", strtotime($date));
                $this->Shift->bindModel(
                    array(
                        'belongsTo' => array(
                            'User' => array(
                                'className'  => 'User',
                                'foreignKey' => 'user_id',
                            ),
                        ),
                    )
                );

                $shiftDetail = $this->Shift->find(
                    "all",
                    array(
                        "conditions" => array(
                            'NOT'              => array(
                                'Shift.user_id' => array($userID),
                            ),
                            "Shift.shift_date" => $shiftdate,
                            "Shift.type"       => 2,
                        ),
                    )
                );

                if (empty($shiftDetail)) {
                    $response = array(
                        "sale_status" => false,
                        "message"     => "No shift found.",
                        "error"       => true,
                    );
                } else {
                    $response['sale_status'] = true;
                    $response['message']     = 'success';
                    $response['error']       = 'false';
                    foreach ($shiftDetail as $key => $val) {
                        $date                    = date("M j D", strtotime($val['Shift']['shift_date']));
                        $dateFormat              = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                        $date_arr                = explode(" ", $date);
                        $response['sale_resp'][] = array(
                            "shiftID"     => $val['Shift']['id'],
                            "month"       => strtoupper($date_arr[0]),
                            "day"         => $date_arr[1],
                            "date"        => $date_arr[2],
                            "userID"      => $val['Shift']['user_id'],
                            "doctor_name" => $val['User']['first_name'],
                             "doctor_last_name"   => $val['User']['last_name'],
                            "fullDate"    => substr($dateFormat, 0, 10),
                            "shift_date"  => substr($val['Shift']['shift_date'], 0, 10),
                        );
                    }
                }
            }

            if ($type == 'mySale') {
                $shiftdate   = date("Y-m-d", strtotime($date));
                $shiftDetail = $this->Shift->find(
                    "first",
                    array(
                        'conditions' => array(
                            "Shift.shift_date" => $shiftdate,
                            "Shift.user_id"    => $userID,
                            'Shift.type'       => 2,
                        ),
                    )
                );
                $response = $this->myshiftSetailSingle($date, $userID);
            }

            if ($type == 'addShift') {
                $shiftdate   = date("Y-m-d", strtotime($date));
                $shiftDetail = $this->Shift->find(
                    "first",
                    array(
                        'conditions' => array(
                            "Shift.shift_date" => $shiftdate,
                            "Shift.user_id"    => $userID,
                        ),
                    )
                );
                $response['self_shift_list'] = $shiftDetail;
                $response['today_sale_list'] = $this->shifts_on_sale_today($shiftdate, $userID);
            }
            echo json_encode($response);
        }
    }
    //===========//

    public function myshiftSetailSingle($date, $userID)
    {
        $this->autoRender = false;
        $shiftID          = date("Y-m-d", strtotime($date));
        $shiftDetail      = $this->Shift->find(
            "first",
            array(
                'conditions' => array(
                    "Shift.type"       => 2,
                    "Shift.shift_date" => $shiftID,
                    "Shift.user_id"    => $userID,
                ),
            )
        );

        if (!empty($shiftDetail)) {
            $response['my_sales_tatus'] = true;
            $response['message']        = 'success';
            $response['error']          = 'false';
            $date                       = date("M j D", strtotime($shiftDetail['Shift']['shift_date']));
            $date_arr                   = explode(" ", $date);
            $response['singleData'][]   = array(
                "shiftID" => $shiftDetail['Shift']['id'],
                "month"   => strtoupper($date_arr[0]),
                "day"     => $date_arr[1],
                "date"    => $date_arr[2],
            );
        } else {
            $response = array("my_sales_tatus" => false,
                "message"                          => "Shift not found!.",
                "error"                            => false);
        }
        return $response;
    }

    public function myshiftDetail($date, $userID)
    {
        $this->autoRender = false;
        $shiftID          = date("Y-m-d", strtotime($date));
        $shiftDetail      = $this->Shift->find(
            "first",
            array(
                'conditions' => array(
                    "Shift.type"       => 1,
                    "Shift.shift_date" => $shiftID,
                    "Shift.user_id"    => $userID,
                ),
            )
        );

        if (!empty($shiftDetail)) {
            $response['status']       = true;
            $response['message']      = 'success';
            $response['error']        = 'false';
            $date                     = date("M j D", strtotime($shiftDetail['Shift']['shift_date']));
            $date_arr                 = explode(" ", $date);
            $response['singleData'][] = array(
                "shiftID" => $shiftDetail['Shift']['id'],
                "month"   => strtoupper($date_arr[0]),
                "day"     => $date_arr[1],
                "date"    => $date_arr[2],
            );
        } else {
            $response = array("status" => false,
                "message"                  => "Shift not found!.",
                "error"                    => false);
        }
        return $response;
    }

    public function acceptRequest()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $requestID = $this->request->data['requestID'];
            $userID    = $this->request->data['userID'];

            $getData = $this->ShiftRequest->find("first", array("conditions" => array("ShiftRequest.id" => $requestID)));

            $shiftID = $getData['ShiftRequest']['shift_id'];
            $shiftDATE = $this->Shift->field('shift_date',array('id'=>$shiftID));
            $old_date_timestamp = strtotime($shiftDATE);
            $newPushDATE = date('F jS l', $old_date_timestamp);  
            $newShiftDATE = date('j-n-Y', $old_date_timestamp);

            if ($getData['ShiftRequest']['type'] == 'exchange') {

                
                /*** login user shift from exchange date  */
                $this->Shift->updateAll(array('Shift.user_id' => $userID, 'Shift.type' => 1), array('Shift.id' => $getData['ShiftRequest']['exchange_id']));

                /*** exchange user shift to other shift  */
                $this->Shift->updateAll(array('Shift.user_id' => $getData['ShiftRequest']['user_id'], 'Shift.type' => 1), array('Shift.id' => $getData['ShiftRequest']['shift_id']));
                $response['status']  = true;
                $response['message'] = 'success';
                $response['error']   = 'false';
                $conditions          = array('shift_id' => $getData['ShiftRequest']['shift_id']);
                $this->ShiftRequest->deleteAll($conditions);
           
                /*Notifcation send to user */
                $loginUserDetail = $this->getDetailAccount($userID);
                $otherUserDetail = $this->getDetailAccount($getData['ShiftRequest']['user_id']);
                $message = array ('title'=>'Circulo Medico',
                            'body' => $loginUserDetail['User']['first_name']." has accepted your exchange request on ".$newPushDATE,
                            'sound'=> 'default',
                            'click_action' => 'FCM_PLUGIN_ACTIVITY',
                            'icon' => 'fcm_push_icon' 
                            );
                $extraData = array ("date"=> $newShiftDATE);
                if(!empty($otherUserDetail['User']['device_token'])) {
                    $this->sendPN($otherUserDetail['User']['device_token'],$message, $extraData);
                } 

            } else {

                

                $this->Shift->updateAll(array('Shift.user_id' => $getData['ShiftRequest']['user_id'], 'Shift.type' => 1), array('Shift.id' => $getData['ShiftRequest']['shift_id']));
                $response['status']  = true;
                $response['message'] = 'success';
                $response['error']   = 'false';
                $conditions          = array('shift_id' => $getData['ShiftRequest']['shift_id']);
                $this->ShiftRequest->deleteAll($conditions);
                  /*Notifcation send to user */
                $loginUserDetail = $this->getDetailAccount($userID);
                $otherUserDetail = $this->getDetailAccount($getData['ShiftRequest']['user_id']);
                $message = array ('title'=>'Circulo Medico',
                            'body' => $loginUserDetail['User']['first_name']." has accepted your buy request on ".$newPushDATE,
                            'sound'=> 'default',
                            'click_action' => 'FCM_PLUGIN_ACTIVITY',
                            'icon' => 'fcm_push_icon' 
                            );
                $extraData = array ("date"=> $newShiftDATE);
                if(!empty($otherUserDetail['User']['device_token'])) {
                    $this->sendPN($otherUserDetail['User']['device_token'],$message, $extraData);
                }
            }
            echo json_encode($response);

        }
    }

    public function exchangeList()
    {

        $this->autoRender = false;
        if ($this->request->is("post")) {
            $userID = $this->request->data['userID'];

            $list = $this->Shift->find(
                "all",
                array(
                    "conditions" => array(
                        "Shift.user_id" => $userID,
                        "Shift.type"    => 1,
                    ),
                    'order' => array(
                        'Shift.shift_date' => 'ASC', 
                    ),
                )
            );

            if (empty($list)) {
                $response = array(
                    "status"  => false,
                    "message" => "No shift found.",
                    "error"   => true,
                );
            } else {
                $response['status']  = true;
                $response['message'] = 'success';
                $response['error']   = 'false';
                foreach ($list as $key => $val) {
                    $checkRequest = $this->ShiftRequest->find("count", array("conditions" => array("ShiftRequest.user_id" => $userID, "ShiftRequest.exchange_id" => $val['Shift']['id'])));
                    if ($checkRequest < 1) {
                        $date                   = date("M j D", strtotime($val['Shift']['shift_date']));
                        $dateFormat             = date("d-m-Y", strtotime($val['Shift']['shift_date']));
                        $date_arr               = explode(" ", $date);
                        $response['resp'][$key] = array("shiftID" => $val['Shift']['id'],
                            "month"                                   => strtoupper($date_arr[0]),
                            "day"                                     => $date_arr[1],
                            "date"                                    => $date_arr[2],
                            "userID"                                  => $val['Shift']['user_id'],
                            "fullDate"                                => substr($dateFormat, 0, 10),
                            "shift_date"                              => substr($val['Shift']['shift_date'], 0, 10),
                        );
                    }
                }
            }
        }

        echo json_encode($response);

    }

    public function myOnSaleRequestDelete()
    {
        $this->autoRender = false;
        if ($this->request->is("post")) {
            $shiftID = $this->request->data['shiftID'];

            $conditions = array('shift_id' => $shiftID);
            $this->ShiftRequest->deleteAll($conditions);
            if ($this->Shift->updateAll(array('Shift.type' => 1), array('Shift.id' => $shiftID))) {
                $response['status']  = true;
                $response['message'] = 'success';
                $response['error']   = 'false';
            } else {
                $response['status']  = fale;
                $response['message'] = 'success';
                $response['error']   = 'false';
            }
            echo json_encode($response);

        }
    }

    public function imageUpload()
    {
        $this->autoRender = false;

        if ($this->request->is("post")) {
            $userID      = $this->request->data['userID'];
            $target_path = WWW_ROOT . 'uploads' . DS . basename($_FILES['file']['name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

                $data           = array("image" => $_FILES['file']['name']);
                $this->User->id = $userID;
                if ($this->User->save($data)) {
                    $outputjson['message'] = "Video Uploaded";
                    $outputjson['data']    = $_FILES['file']['name'];
                    echo SITE_URL.'/uploads/'.$_FILES['file']['name'];

                   // echo json_encode($outputjson);

                }

            } else {
                $outputjson['message'] = "Video Failed";
                $outputjson['data']    = $_FILES['file']['name'];
                $temp_outputjson       = ($outputjson);
                echo '1';
            }
        }

    }

    public function cancelBuy()
    {
        $this->autoRender = false;

        if ($this->request->is("post")) {
            
            $shift_id = $this->request->data['shift_id'];
            $user_id = $this->request->data['user_id'];
            
            $requests = $this->ShiftRequest->find(
                "all",
                array(
                    "conditions" => array(
                        "ShiftRequest.user_id"  => $user_id,
                        "ShiftRequest.shift_id" => $shift_id,
                    ),
                )
            );

            $id = $requests[0]['ShiftRequest']['id'];

            if($this->ShiftRequest->delete($id)){
                $response = array(
                    "status"  => true,
                    "error"  => false,
                );
            }else{
                $response = array(
                    "status"  => false,
                    "message" => "Request cannot be canceled. Please try again.",
                    "error"   => true,
                );
            }            
        } else {
            $response = array(
                "status"  => false,
                "message" => "You can't access!.",
                "error"   => true,
            );
        }
        echo json_encode($response);
    }


    public function contactQuery() {
        $this->loadModel("Contact");
        $this->autoRender = false;
        if ($this->request->is("post")) { 
            if ($this->Contact->save($this->request->data)) {

                $admin_email = Configure::read('App.EmailFrom');
                $template    = $this->EmailTemplate->findBySlug("query");
                $mailMessage = str_replace(array('{name}','{email}','{message}', '{admin_mail}'), array($this->request->data['name'],$this->request->data['email'],$this->request->data['messgae'], $admin_email), $template['EmailTemplate']['description']);
                $this->set("mailMessage", $mailMessage);
                $this->Email->to      ="info@marketingmindz.com";
                $this->Email->subject = $template['EmailTemplate']['subject'];
                $this->Email->from    ="inquiry@technologymindz.com";
                $this->Email->sendAs  = 'html';
                $this->Email->send($mailMessage);

                $response = array("status"  => true,
                                  "message" => "Sent! Thank you",
                                    "error" => false,
                                );
            } else {
                $response = array("status" => false,
                                 "message" => "Your query could not updated.Please try again.",
                                "error"    => true,
                                );
            }

        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => true);
        }
        echo json_encode($response);
    }

    public function supportDetail(){
        $this->autoRender = false;
        if ($this->request->is("post")) { 
           // $appPkg = $this->request->data['package'];
            $supportDetail = $this->Sptconfig->find("first", array('conditions' => array("Sptconfig.slug" => 'support')));
           /* $response = $this->get_app_detail("https://androidquery.appspot.com/api/market?app=com.ionicframework.accountingSTFree");
                    $resArr = array();
                    $resArr = json_decode($response,true);*/
                    
            if (!empty($supportDetail)) {

                    
                 
                $response = array("status"  => true,
                                  "resp"    => $supportDetail['Sptconfig'], 
                                  "message" => "Success",
                                  "error"   => false);
            } else {
                $response = array("status" => false,
                                 "message" => "Support Detail not found!.",
                                 "error"   => false);
            }
        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => false);
        }
         echo json_encode($response);
       /* $response['appDetail']=$resArr;
        if(!empty($resArr)) {
            echo json_encode($response);
        }*/
        
    }
    
    public function deviceToken() {
        $this->autoRender = false;
        if ($this->request->is("post")) {
           // pr($this->request->data); die;
            $userID  = $this->request->data['userID'];
            $this->User->id = $userID;
            if ($this->User->save($this->request->data)) {

                $response = array("status" => true,
                    "message"                  => "Your informastion successfully Updated.",
                    "error"                    => false,
                );
            } else {
                $response = array("status" => false,
                    "message"                  => "Your informastion could not updated.Please try again.",
                    "error"                    => true,
                );
            }

        } else {
            $response = array("status" => false,
                "message"                  => "You can't access!.",
                "error"                    => true);
        }
        echo json_encode($response);
    }


    function sendPushForSaleShift($userID, $shiftDATE) {

        $old_date_timestamp = strtotime($shiftDATE);
        $newPushDATE = date('F jS l', $old_date_timestamp);  
        $newShiftDATE = date('j-n-Y', $old_date_timestamp);
        $this->autoRender = false;
        $loginUser = $this->User->find("first",array("conditions"=>array("User.id"=>$userID)));
        $UserList = $this->User->find("all",array("conditions"=>array("User.status"=>1,"User.role"=>2,"User.id !="=>$userID,
            "User.device_token !="=>''))); 
        if(!empty($UserList)) {
            
            foreach ($UserList as $key => $value) {
                
                $message = array ('title'=>'Circulo Medico',
                                  /*'body' => $loginUser['User']['first_name']." has sold our shift",*/
                                  'body' => $newPushDATE." for sale by ".$loginUser['User']['first_name'],
                              'sound'=> 'default',
                              'click_action'    => 'FCM_PLUGIN_ACTIVITY',
                              'icon'   => 'fcm_push_icon'
                            );

                $extraData = array ("date"=> $newShiftDATE); 
                if(!empty($value['User']['device_token'])) { 
                    
                    $this->sendPN($value['User']['device_token'],$message, $extraData);
                }
                
            }
        }

    }

    function buyAndExchangeSendPush($userID,$shiftID,$type,$shiftDATE) {
        $old_date_timestamp = strtotime($shiftDATE);
        $newPushDATE = date('F jS l', $old_date_timestamp);  
        $newShiftDATE = date('j-n-Y', $old_date_timestamp);

            $shiftDetail = $this->Shift->find("first",array("conditions"=>array("Shift.id"=>$shiftID))); 
            $shiftUser   = $this->getDetailAccount($shiftDetail['Shift']['user_id']);           
            $loginUser   = $this->getDetailAccount($userID);

          $message = array ('title'=>'Circulo Medico',
                            'body' => $loginUser['User']['first_name']." want to ".$type." your shift on ".$newPushDATE,
                            'sound'=> 'default',
                            'click_action' => 'onNotification',
                            'icon' => 'fcm_push_icon'
                            );
          $extraData = array ("date"=> $newShiftDATE); 
                if(!empty($shiftUser['User']['device_token'])) {
                    $this->sendPN($shiftUser['User']['device_token'],$message, $extraData);
                }
    }

    function getDetailAccount($userID) {
        $loginUser = $this->User->find("first",array("conditions"=>array("User.id"=>$userID)));
        return $loginUser;
    }

    public function sendPN($devicID,$message,$extraData) {
        $this->autoRender = false;
         $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array ('to' => $devicID,'notification' => $message,'data' => $extraData);
        $fields = json_encode ($fields);
        $headers = array ('Authorization: key=AIzaSyABVV9_eS4uK1nylTW1j3xZLP-Hs8BFT1c','Content-Type: application/json');
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        $result = curl_exec ($ch);
      //  exit;
      //  return $result;   
        curl_close ($ch);
    }


    function get_app_detail($url) {
            $options = array(
                CURLOPT_RETURNTRANSFER => true,   // return web page
                CURLOPT_HEADER         => false,  // don't return headers
                CURLOPT_FOLLOWLOCATION => true,   // follow redirects
                CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
                CURLOPT_ENCODING       => "",     // handle compressed
                CURLOPT_USERAGENT      => "test", // name of client
                CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
                CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
                CURLOPT_TIMEOUT        => 120,    // time-out on response
            ); 

            $ch = curl_init($url);
            curl_setopt_array($ch, $options);

            $content  = curl_exec($ch);

            curl_close($ch);

            return $content;
    }

}
 