jQuery(document).ready(function() {






    jQuery("#billing_city").val('');
    jQuery("#sd_delivery_time_field").hide();
    jQuery("#wss_checkout_ajax_path").hide();
    var wss_checkout_ajax_path = jQuery("#wss_checkout_ajax_path").val();

    // Calling Ajax when page load first time and only one shipping method is available
    var shipping_method_id = jQuery('.shipping input[type="hidden"]').val();    
    if(shipping_method_id!=undefined){
        show_hide_time(wss_checkout_ajax_path, shipping_method_id);
    }

    // Calling Ajax when shipping method already checked and then reload the page
    var shipping_method_id2 = jQuery('#shipping_method input[type="radio"]:checked').val();    
    if(jQuery.isNumeric( shipping_method_id2 )) {
        show_hide_time(wss_checkout_ajax_path, shipping_method_id2);
    }
         
    // Calling Ajax when change shipping method
    jQuery(document).on('change', '#shipping_method input[type="radio"]', function() {
        var shipping_id = jQuery(this).val();
        show_hide_time(wss_checkout_ajax_path, shipping_id);
    });


    // Ajax functionality
    function show_hide_time(wss_checkout_ajax_path, shipping_method_id){
        jQuery.ajax({
            type : 'post',
            dataType: 'json',
            url : wss_checkout_ajax_path,
            data : {'shipping_id': shipping_method_id},
            success: function(response) {
                console.log(response);
                if(response.status == true){
                    jQuery('#sd_delivery_time').timepicker({ 
                        'minTime': response.shipping_start_time, 
                        'maxTime': response.shipping_end_time,
                        'disableTimeRanges': [
                            [response.shipping_start_time, response.city_current_time],
                        ]
                    });
                    jQuery("#sd_delivery_time_field").show(500);
                }
                else{
                    jQuery("#sd_delivery_time_field").hide(500);
                }
            }
        });  
        
    }


    jQuery('#sd_delivery_time').keydown(function(e) {
       e.preventDefault();
       return false;
    });


    /*jQuery("#billing_first_name").val("Gary");
    jQuery("#billing_last_name").val("Assoun");
    jQuery("#billing_company").val("Biocoiff");
    jQuery("#billing_email").val("gary@niocoiff.com");
    jQuery("#billing_phone").val("0650523141");
    jQuery("#billing_address_1").val("42 Boulevard henri 4");
    jQuery("#billing_address_2").val("Salon de coiffure biocoiff");
    jQuery("#billing_postcode").val("75004");*/
    //jQuery("#billing_city").val("paris");

    // Admin: 29 rue de Rivoli 75004 Paris
    /*jQuery("#billing_city").val('');
    jQuery("#billing_first_name").val("John");
    jQuery("#billing_last_name").val("Doe");
    jQuery("#billing_company").val("Marketing Mindz");
    jQuery("#billing_email").val("vinod@marketingmindz.in");
    jQuery("#billing_phone").val("+33628046019");
    jQuery("#billing_address_1").val("1 Rue du Pont Neuf");
    jQuery("#billing_postcode").val("75001");
    jQuery("#billing_city").val("paris");
    jQuery("#billing_address_2").val("Nirman nagar");

    var wss_checkout_ajax_path = jQuery("#wss_checkout_ajax_path").val();
    var billing_first_name = jQuery("#billing_first_name").val();
    
    //console.log(wss_checkout_ajax_path);
    
    jQuery("#shipping_method li:nth-child(2) input").css("display", "none");*/

    /*jQuery('#sd_delivery_time').timepicker({
        timeFormat: 'h:mma',
        interval: 15,
        minTime: '8',
        maxTime: '11:45pm',

    });*/

    

});



