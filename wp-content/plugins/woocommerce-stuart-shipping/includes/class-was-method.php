<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( class_exists( 'WAS_Advanced_Shipping_Method' ) ) return; // Stop if the class already exists

/**
 * Class WAS_Advanced_Shipping_Method.
 *
 * WooCommerce Advanced Shipping method class.
 *
 * @class		WAS_Advanced_Shipping_Method
 * @author		Marketing Mindz
 * @package		WooCommerce Stuart Shipping
 * @version		1.0.0
 */
class WAS_Advanced_Shipping_Method extends WC_Shipping_Method {


	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->id                 = 'stuart_shipping';
		$this->title              = __( 'Stuart (configurable per rate)', 'woocommerce-stuart-shipping' );
		$this->method_title       = __( 'Stuart Shipping', 'woocommerce-stuart-shipping' );
		$this->method_description = __( 'Configure WooCommerce Stuart Shipping', 'woocommerce-stuart-shipping' );

		$this->init();

		do_action( 'woocommerce_advanced_shipping_method_init' );

		// Save settings
		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

		// Allow setting WAS Shipping rates for priorities
		add_filter( 'option_woocommerce_shipping_method_selection_priority', array( $this, 'default_shipping_method_priority' ), 10, 1 );

		

	}

	/**
	 * Init.
	 *
	 * Initialize WAS shipping method.
	 *
	 * @since 1.0.0
	 */
	function init() {

		//die('Hiii');

		$this->init_form_fields();
		$this->init_settings();

		$this->enabled       = $this->get_option( 'enabled' );
		$this->hide_shipping = $this->get_option( 'hide_other_shipping_when_available' );

		// Hide shipping methods
		if ( version_compare( WC()->version, '2.1', '<' ) ) :
			//add_filter( 'woocommerce_available_shipping_methods', array( $this, 'hide_all_shipping_when_free_is_available' ) );
		else :
			//add_filter( 'woocommerce_package_rates', array( $this, 'hide_all_shipping_when_free_is_available' ) );
		endif;

	}


	/**
	 * Match methods.
	 *
	 * Checks all created WAS shipping methods have a matching condition group.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $package List of shipping package data.
	 * @return array          List of all matched shipping methods.
	 */
	public function was_match_methods( $package ) {

		$matched_methods = array();
		$methods         = get_posts( array( 'posts_per_page' => '-1', 'post_type' => 'was', 'orderby' => 'menu_order', 'order' => 'ASC', 'suppress_filters' => false ) );

		foreach ( $methods as $method ) :

			$condition_groups = get_post_meta( $method->ID, '_was_shipping_method_conditions', true );

			// Check if method conditions match
			$match = $this->was_match_conditions( $condition_groups, $package );

			// Add match to array
			if ( true == $match ) :
				$matched_methods[] = $method->ID;
			endif;

		endforeach;

		return $matched_methods;

	}


	/**
	 * Match conditions.
	 *
	 * Check if conditions match, if all conditions in one condition group
	 * matches it will return TRUE and the shipping method will display.
	 *
	 * @since 1.0.0
	 *
	 * @param  array $condition_groups List of condition groups containing their conditions.
	 * @param  array $package          List of shipping package data.
	 * @return BOOL                    TRUE if all the conditions in one of the condition groups matches true.
	 */
	public function was_match_conditions( $condition_groups = array(), $package = array() ) {

		if ( empty( $condition_groups ) ) return false;

		foreach ( $condition_groups as $condition_group => $conditions ) :

			$match_condition_group = true;

			foreach ( $conditions as $condition ) :

				$condition = apply_filters( 'was_match_condition_values', $condition );
				$match     = apply_filters( 'was_match_condition_' . $condition['condition'], false, $condition['operator'], $condition['value'], $package );

				if ( false == $match ) :
					$match_condition_group = false;
				endif;

			endforeach;

			// return true if one condition group matches
			if ( true == $match_condition_group ) :
				return true;
			endif;

		endforeach;

		return false;

	}


	/**
	 * Init fields.
	 *
	 * Add fields to the WAS shipping settings page.
	 *
	 * @since 1.0.0
	 */
	public function init_form_fields() {

		$this->form_fields = array(
			'enabled'                            => array(
				'title'   => __( 'Enable/Disable', 'woocommerce-stuart-shipping' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable Stuart Shipping', 'woocommerce-stuart-shipping' ),
				'default' => 'yes'
			),
			/*'hide_other_shipping_when_available' => array(
				'title'   => __( 'Hide other shipping', 'woocommerce-advanced-shipping' ),
				'type'    => 'checkbox',
				'label'   => __( 'Hide other shipping methods when free shipping is available', 'woocommerce-stuart-shipping' ),
				'default' => 'no'
			),*/
			'was_shipping_rates_table'           => array(
				'type' => 'was_shipping_rates_table',
			),
		);

	}


	/**
	 * Process and save options.
	 *
	 * Processes, validates and sanitizes options on the shipping page.
	 *
	 * @since 1.0.8
	 */
	public function process_admin_options( $post_data = array() ) {

		parent::process_admin_options();

		if ( isset( $_POST['method_priority'] ) ) :
			foreach ( $_POST['method_priority'] as $rate_id => $priority ) :
				update_post_meta( absint( $rate_id ), '_priority', absint( $priority ) );
			endforeach;
		endif;

	}




	/**
	 * Settings tab table.
	 *
	 * Load and render the table on the Advanced Shipping settings tab.
	 *
	 * @since 1.0.0
	 *
	 * @return string
	 */
	public function generate_was_shipping_rates_table_html() {

		ob_start();

			/**
			 * Load conditions table file
			 */
			require plugin_dir_path( __FILE__ ) . 'admin/views/shipping-rates-table.php';

		return ob_get_clean();

	}


	/**
	 * Calculate shipping.
	 *
	 * Calculate the shipping and set settings.
	 *
	 * @since 1.0.0
	 *
	 * @param array $package List containing all products for this method.
	 */
	public function calculate_shipping( $package = array() ) {

		global $woocommerce; 
		// print_r($_REQUEST); die;
		/*$a = new DateTime('now', new DateTimeZone('Europe/London')); 
		echo $a->format('g:ia');
		exit;*/

		if(is_checkout() == false) return;
		 
		error_reporting(0);
     	
		ob_start();
		session_start();
		
		$this->matched_methods = $this->was_match_methods( $package );
		$cart_total_weight = $woocommerce->cart->cart_contents_weight;

		$matchs_details = array();
		foreach ( $this->matched_methods as $methods_id ){
			$matchs_details = get_post_meta( $methods_id, '_was_shipping_method', true );
			$transport_id = $matchs_details['transport_id'];
		}


		//prd($matchs_details['min_amount']);

		if ( false == $this->matched_methods || ! is_array( $this->matched_methods ) || 'no' == $this->enabled ) return;
		// Check condition minimum amount


		if($package['contents_cost'] < $matchs_details['min_amount']) return;


	    //Check condition for cart weight
		$stu_transport_id = $matchs_details['stu_transport_id'];
 		$stuart_weight = $this->transportWeight(strtolower($package['destination']['city']), $stu_transport_id);
 		if ($cart_total_weight > $stuart_weight) return;



		// Check condition for working day
		$today = date('l', strtotime(current_time('mysql')));
		$sd_working_day = $matchs_details['sd_working_day'];
		if(in_array($today, $sd_working_day) == false) return;
		//die("Hi");


		$label = $matchs_details['stu_checkout_label'];
		$wss_api_mode = get_option("wss_api_mode");
		if($wss_api_mode == "Sandbox"){
			$end_point = get_option("wss_sandbox_url") . "/v1/jobs/quotes/types"; // For Job quote
			$access_token = get_option("wss_sandbox_api_oauth_token");
		} else {
			$end_point = get_option("wss_live_url") . "/v1/jobs/quotes/types"; // For Job quote
			$access_token = get_option("wss_live_api_oauth_token");
		}
		
		if(isset($_REQUEST['post_data'])){
			$query_string = $_REQUEST['post_data'];
			parse_str($query_string, $query_params);
			$destinationContactFirstname = $query_params['billing_first_name'];
			$destinationContactLastname = $query_params['billing_last_name'];
			$destinationContactEmail = $query_params['billing_email'];
			$destinationContactPhone = $query_params['billing_phone'];
			$destinationContactCompany = $query_params['shipping_company'];
			$sd_delivery_time = $query_params['sd_delivery_time'];
			$sd_delivery_time_unix = strtotime($sd_delivery_time);

		} 
		//if($sd_delivery_time == "") return;
		
		//prd($package);
		$destination = $package['destination']['address'] . ", " . $package['destination']['postcode'] . " " . $package['destination']['city'];
		$origin = $matchs_details['address_location'];
		$originContactPhone = $matchs_details['origin_contact'];

		// Transport type calculation
		
		$transport_id = $this->transportType(strtolower($package['destination']['city']), $cart_total_weight);
		
		//$transport_id = '2,3,4,5,7';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $end_point);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);

		curl_setopt($ch, CURLOPT_POSTFIELDS, "transportTypeIds=". $transport_id ."&destination=" . $destination . "&destinationContactCompany=" . $destinationContactCompany . "&destinationContactEmail=" . $destinationContactEmail . "&destinationContactFirstname=" . $destinationContactFirstname . "&destinationContactLastname=" . $destinationContactLastname . "&destinationContactPhone=" . $destinationContactPhone . "&origin=" . $origin . "&originContactPhone=" . $originContactPhone);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/x-www-form-urlencoded",
		  "Authorization: Bearer " . $access_token
		));

		$quote_response = curl_exec($ch);
		curl_close($ch);

		$quote_response_decoded = json_decode($quote_response, true);
		update_option("quote_array", $quote_response_decoded);

		$i = 0;
		$quote_id = '';
		foreach ($quote_response_decoded as $key => $value) {
			# code...
			if($i == 0){
				$quote_id = $value['id'];
		    $Amount = $value['finalAmount'];
			}
			$i++;
		}
	
      $stuart_fee_type = $matchs_details['stuart_fee_type'];
      $stuart_fee_value = $matchs_details['stuart_fee_value'];
      if($stuart_fee_type == 'percent'){
      	$p_amount = ($Amount * $stuart_fee_value) / 100;
      	$finalAmount = ($Amount + $p_amount);
       } else {
      	$finalAmount = ($Amount + $stuart_fee_value);
      }
      $_SESSION['wss_last_quote_id'] = $quote_id;
      update_option("wss_last_quote_id", $quote_id);



		foreach ( $this->matched_methods as $method_id ) :
			$rate = apply_filters( 'was_shipping_rate', array(
				'id'       => $method_id,
				'label'    => ( null == $label ) ? __( 'Shipping', 'woocommerce-stuart-shipping' ) : $label,
				'cost'     => $finalAmount,
				'taxes'    => false,
				'calc_tax' => 'per_order',
				'package'  => $package,
			), $package, $this );
			$this->add_rate( $rate );
		endforeach;

		
		ob_get_clean();
	}

	public function transportWeight($city, $stu_transport_id){

		$stuart_weight = '';

			 if($city == "paris"){

				if(in_array(2, $stu_transport_id)){
						$stuart_weight = 12;
				} 
				 if(in_array(3, $stu_transport_id)){
					$stuart_weight = 15;
				} 

				if(in_array(4, $stu_transport_id) || in_array(5, $stu_transport_id) || in_array(7, $stu_transport_id)) {
					$stuart_weight = 50;
				}
 
			}

			else if($city == "lyon")
			{
				if(in_array(2, $stu_transport_id)){
						$stuart_weight = 12;
				} 
			}

			else if($city == "barcelona"){

				if(in_array(2, $stu_transport_id)){
						$stuart_weight = 12;
				} 
				 if(in_array(3, $stu_transport_id)){
					$stuart_weight = 40;
				} 

				if(in_array(4, $stu_transport_id) || in_array(5, $stu_transport_id)) {
					$stuart_weight = 50;
				}
			}

			else if($city == "madrid"){

				if(in_array(2, $stu_transport_id)){
						$stuart_weight = 12;
				} 
				 if(in_array(3, $stu_transport_id)){
					$stuart_weight = 40;
				} 

				if(in_array(4, $stu_transport_id) || in_array(5, $stu_transport_id)) {
					$stuart_weight = 50;
				}
			}

			else if($city == "london"){

				if(in_array(2, $stu_transport_id)){
						$stuart_weight = 20;
				} 
				 if(in_array(3, $stu_transport_id)){
					$stuart_weight = 40;
				} 

				if(in_array(5, $stu_transport_id)) {
					$stuart_weight = 100;
				}

				if(in_array(4, $stu_transport_id)) {
					$stuart_weight = 150;
				}
				
			}

			return $stuart_weight;

	}


	public function transportType($city, $cart_total_weight){

		switch ($city) {
			case 'paris':	
				if($cart_total_weight <= 12){
					$transport_id = 2;
				} else if($cart_total_weight > 12 && $cart_total_weight <= 15){
					$transport_id = 3;
				} else {
					$transport_id = '4,5,7';
				}

			break;

			case 'lyon':

				if($cart_total_weight <= 12){
					$transport_id = 2;
				}

			break;

			case 'barcelona':	

				if($cart_total_weight <= 12){
					$transport_id = 2;
				} else if($cart_total_weight > 12 && $cart_total_weight <= 40){
					$transport_id = 3;
				} else {
					$transport_id = '4,5';
				}

			break;

			case 'madrid':	

				if($cart_total_weight <= 12){
					$transport_id = 2;
				} else if($cart_total_weight > 12 && $cart_total_weight <= 40){
					$transport_id = 3;
				} else {
					$transport_id = '4,5';
				}

			break;

			case 'london':	

				if($cart_total_weight <= 20){
					$transport_id = 2;
				} else if($cart_total_weight > 20 && $cart_total_weight <= 40){
					$transport_id = 3;
				} else if($cart_total_weight > 40 && $cart_total_weight <= 100){
					$transport_id = 5;
				} else {
					$transport_id = 4;
				}

			break;
			
			default:
				# code...
			break;
		}

		return $transport_id;
	}

	

}
