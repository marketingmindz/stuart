<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WSS meta box settings.
 *
 * Display the shipping settings in the meta box.
 *
 * @author		Marketing Mindz
 * @package		WooCommerce Stuart Shipping
 * @version		1.0.0
 */

wp_nonce_field( 'was_settings_meta_box', 'was_settings_meta_box_nonce' );

global $post;
$settings = get_post_meta( $post->ID, '_was_shipping_method', true );

?><div class='was was_settings was_meta_box was_settings_meta_box'>
<script type="text/javascript">
jQuery( document ).ready(function() {
  	jQuery('#shipping_start_time').timepicker();
  	jQuery('#shipping_end_time').timepicker();
  	
 });
</script>
	

	<table class="form-table">

		<tr>
		<th scope="row"><label for="shipping_start_time"><?php _e( 'Shipping Start Time', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[shipping_start_time]" type="text" id="shipping_start_time" value="<?php echo @$settings['shipping_start_time']; ?>" class="regular-text" placeholder='e.g. 10:00am'>
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="shipping_end_time"><?php _e( 'Shipping End Time', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[shipping_end_time]" type="text" id="shipping_end_time" value="<?php echo @$settings['shipping_end_time']; ?>" class="regular-text" placeholder='e.g. 11:00pm'>
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="min_amount"><?php _e( 'Minimum Amount for free Delivery', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[min_amount]" type="text" id="min_amount" value="<?php echo @$settings['min_amount']; ?>" class="regular-text" placeholder='e.g. 300'>
		<p class="description" id="tagline-description">
		<?php _e( 'Define minimum amount of Order allowing to choose Stuart Delivery Method for Free', 'woocommerce-stuart-shipping' ); ?></p></td>
		</tr>

		<tr>
		<th scope="row"><label for="min_order_time"><?php _e( 'Minimum order prepration time', 'woocommerce-stuart-shipping' ); ?><em>(In minute)</em></label></th>
		<td>
		<input name="_was_shipping_method[min_order_time]" type="text" id="min_order_time"  value="<?php echo @$settings['min_order_time']; ?>" class="regular-text" placeholder='e.g. 30'>
		<p class="description" id="tagline-description"><?php _e( 'Define minimum amount of time necessary to prepare the Order.', 'woocommerce-stuart-shipping' ); ?></p></td>
		</tr>

		<tr>
		<th scope="row"><label for="stuart_fee_type"><?php _e( 'Discount Fee Type', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input type="radio" name="_was_shipping_method[stuart_fee_type]" value="fixed" <?php echo @$settings['stuart_fee_type'] != '' && @$settings['stuart_fee_type'] == "fixed" ? 'checked="checked"' : ''; ?> > <?php _e( 'Fixed', 'woocommerce-stuart-shipping' ); ?><br>
		<input type="radio" name="_was_shipping_method[stuart_fee_type]" value="percent" <?php echo @$settings['stuart_fee_type'] != '' && @$settings['stuart_fee_type'] == "percent" ? 'checked="checked"' : ''; ?> > <?php _e( 'Percent (%)', 'woocommerce-stuart-shipping' ); ?> 
		
		</tr>

		<tr>
		<th scope="row"><label for="stuart_fee_value"><?php _e( 'Discount On Fee', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[stuart_fee_value]" type="number" id="stuart_fee_value" value="<?php echo @$settings['stuart_fee_value']; ?>" class="regular-text code" placeholder='e.g. -50% or -2'>
		<p class="description" id="tagline-description"><?php _e( 'Stuart shipping price. Plus(+) for increase or Minus(-) for decrease. Ex: -50% or -2', 'woocommerce-stuart-shipping' ); ?></p>
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="address_location"><?php _e( 'Address Location', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		
		<textarea name="_was_shipping_method[address_location]" id="address_location" class="large-text code" rows="3"><?php echo @$settings['address_location']; ?></textarea>
		<p class="description" id="tagline-description"><?php _e( 'Address location (warehouse) from where the order is shipped', 'woocommerce-stuart-shipping' ); ?></p>
		
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="origin_contact"><?php _e( 'Contact no.', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[origin_contact]" type="text" id="origin_contact" value="<?php echo @$settings['origin_contact']; ?>" class="regular-text code" placeholder='e.g. +33678374859'>
		<p class="description" id="tagline-description"><?php _e( 'Admin contact number.', 'woocommerce-stuart-shipping' ); ?></p>
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="stu_checkout_label"><?php _e( 'Label on the checkout page', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<input name="_was_shipping_method[stu_checkout_label]" type="text" id="stu_checkout_label" value="<?php echo @$settings['stu_checkout_label']; ?>" class="regular-text ltr" placeholder='e.g. Stuart Delivery' >
		<p class="description" id="stu-checkout"><?php _e( 'The “Label” of the Stuart method on the checkout page.', 'woocommerce-stuart-shipping' ); ?></p></td>
		</tr>

		<tr>
		<th scope="row"><label for="stu-checkout"><?php _e( 'Working Day', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<?php 
		$sd_working_day = @$settings['sd_working_day'];
		$day_array = array( __( 'Sunday', 'woocommerce-stuart-shipping' ), __( 'Monday', 'woocommerce-stuart-shipping' ), __( 'Tuesday', 'woocommerce-stuart-shipping' ), __( 'Wednesday', 'woocommerce-stuart-shipping' ), __( 'Thursday', 'woocommerce-stuart-shipping' ), __( 'Friday', 'woocommerce-stuart-shipping' ), __( 'Saturday', 'woocommerce-stuart-shipping' ));
		foreach($day_array as $day){ ?>
			<input name="_was_shipping_method[sd_working_day][]" type="checkbox" value="<?php echo $day ?>" class="regular-text ltr" <?php echo !empty($sd_working_day) && in_array($day, $sd_working_day) ? "checked='checked'" : ""; ?> > <?php echo $day; ?> <br/>
		<?php } ?>
		<p class="description" id="stu-checkout"><?php _e( 'Stuart delivery method will be available only during these days.', 'woocommerce-stuart-shipping' ); ?></p></td>
		</tr>

		<tr>
		<th scope="row"><label for="stu_transport_id"><?php _e( 'Transport Types', 'woocommerce-stuart-shipping' ); ?></label></th>
		<td>
		<?php 
		$stu_transport_id = @$settings['stu_transport_id'];
		$transport_type_fields = array( 2 => __( 'Bike', 'woocommerce-stuart-shipping' ), 3 => __( 'Motorbike', 'woocommerce-stuart-shipping' ), 4 => __( 'Car', 'woocommerce-stuart-shipping' ), 5 => __( 'Cargo Bike', 'woocommerce-stuart-shipping' ), 7 => __( 'Cargo Bike XL', 'woocommerce-stuart-shipping' ));
		foreach($transport_type_fields as $transport_id => $transport_title){ ?>
			<input name="_was_shipping_method[stu_transport_id][]" type="checkbox" value="<?php echo $transport_id ?>" class="regular-text ltr" <?php echo !empty($stu_transport_id) && in_array($transport_id, $stu_transport_id) ? "checked='checked'" : ""; ?> > <?php echo $transport_title; ?> <br/>

		<?php } ?>
		<p class="description" id="stu-checkout"><?php _e( 'Stuart handles different transport types, depending on the city', 'woocommerce-stuart-shipping' ); ?></p></td>
		</tr>

	</table>



	<?php do_action( 'was_after_meta_box_settings', $settings ); ?>
</div>
