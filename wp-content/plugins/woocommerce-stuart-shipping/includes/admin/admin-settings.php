<?php
/**
 * Setting section of Stuart Plugin.
 * 
 *
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/* Stuart activation */
// This is the secret key for API authentication. You configured it in the settings menu of the license manager plugin.
define('STUART_SPECIAL_SECRET_KEY', '592ffe86446a19.57120743'); //Rename this constant name so it is specific to your plugin or theme.

// This is the URL where API query request will be sent to. This should be the URL of the site where you have installed the main license manager plugin. Get this value from the integration help page.
define('STUART_LICENSE_SERVER_URL', 'http://dev.marketingmindz.com/wpthemes/licence_manager/'); //Rename this constant name so it is specific to your plugin or theme.

// This is a value that will be recorded in the license manager data so you can identify licenses for this item/product.
define('STUART_ITEM_REFERENCE', 'Stuart Plugin'); //Rename this constant name so it is specific to your plugin or theme.


$stuart_license_key_expiry = get_option('stuart_license_key_expiry');
if($stuart_license_key_expiry == ''){
	// Post URL
	$postURL = STUART_LICENSE_SERVER_URL;
	// The Secret key
	$secretKey = "592ffe86446976.21431580";
	/*$firstname = "";
	$lastname = "";
	$email = "";*/

	// prepare the data
	$data = array ();
	$data['secret_key'] = $secretKey;
	$data['slm_action'] = 'slm_create_new';
	//$data['first_name'] = $firstname;
	//$data['last_name'] = $lastname;
	//$data['email'] = $email;

	// send data to API post URL
	$ch = curl_init ($postURL);
	curl_setopt ($ch, CURLOPT_POST, true);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
	$returnValue = curl_exec ($ch);

	// Process the return values
	//var_dump($returnValue);
}


//add_action('admin_menu', 'slm_stuart_license_menu');

/*function slm_stuart_license_menu() {
    add_options_page('stuart License Activation Menu', 'stuart License', 'manage_options', __FILE__, 'stuart_license_management_page');
}*/
stuart_license_management_page();
function stuart_license_management_page() {
    echo '<div class="wrap">';
    echo '<h2>Stuart License Management</h2>';

    /*** License activate button was clicked ***/
    if (isset($_REQUEST['activate_license'])) {
        $license_key = $_REQUEST['stuart_license_key'];

        // API query parameters
        $api_params = array(
            'slm_action' => 'slm_activate',
            'secret_key' => STUART_SPECIAL_SECRET_KEY,
            'license_key' => $license_key,
            'registered_domain' => $_SERVER['SERVER_NAME'],
            'item_reference' => urlencode(STUART_ITEM_REFERENCE),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, STUART_LICENSE_SERVER_URL));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)){
            echo "Unexpected Error! The query returned with an error.";
        }

        //var_dump($response);//uncomment it if you want to look at the full response
        
        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));
      
        $current_date = date("Y-m-d H:i:s");
        $year_time = 365*24;
        $expiry_date = date( "Y-m-d H:i:s", strtotime( $current_date ) + $year_time * 3600 );
         
        // TODO - Do something with it.
        //var_dump($license_data);//uncomment it to look at the data
        
        if($license_data->result == 'success'){//Success was returned for the license activation
           
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
            
            //Save the license key in the options table

            update_option('stuart_license_key', $license_key); 

            $old_license_key = get_option('stuart_license_key');
            $stuart_license_key_expiry = get_option('stuart_license_key_expiry');
            if($stuart_license_key_expiry == '' ){
            	update_option('stuart_license_key_expiry', $expiry_date); 
            }else{
            	update_option('stuart_license_key_expiry', $stuart_license_key_expiry); 
            }
        }
        else{
            //Show error to the user. Probably entered incorrect license key.
            
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
        }
        
    }
    /*** End of license activation ***/
    
    /*** License activate button was clicked ***/
    if (isset($_REQUEST['deactivate_license'])) {
        $license_key = $_REQUEST['stuart_license_key'];

        // API query parameters
        $api_params = array(
            'slm_action' => 'slm_deactivate',
            'secret_key' => STUART_SPECIAL_SECRET_KEY,
            'license_key' => $license_key,
            'registered_domain' => $_SERVER['SERVER_NAME'],
            'item_reference' => urlencode(STUART_ITEM_REFERENCE),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, STUART_LICENSE_SERVER_URL));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)){
            echo "Unexpected Error! The query returned with an error.";
        }

        //var_dump($response);//uncomment it if you want to look at the full response
        
        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));
        
        // TODO - Do something with it.
        //var_dump($license_data);//uncomment it to look at the data
        
        if($license_data->result == 'success'){//Success was returned for the license activation
            
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
            
            //Remove the licensse key from the options table. It will need to be activated again.
            update_option('stuart_license_key', '');
        }
        else{
            //Show error to the user. Probably entered incorrect license key.
            
            //Uncomment the followng line to see the message that returned from the license server
            echo '<br />The following message was returned from the server: '.$license_data->message;
        }
        
    }
    /*** End of stuart license deactivation ***/
    
    ?>
    <p>Please enter the license key for this product to activate it. You were given a license key when you purchased this item.</p>
    <form action="" method="post">
        <table class="form-table">
            <tr>
                <th style="width:100px;"><label for="stuart_license_key">License Key</label></th>
                <td ><input class="regular-text" type="text" id="stuart_license_key" name="stuart_license_key"  value="<?php echo get_option('stuart_license_key'); ?>" ></td>
            </tr>
        </table>
        <p class="submit">
            <input type="submit" name="activate_license" value="Activate" class="button-primary" />
            <input type="submit" name="deactivate_license" value="Deactivate" class="button" />
        </p>
    </form>
    <?php
    
    echo '</div>';
}

/* end activation */

	$sd_massege = '';
    if (isset($_POST['setting_submit'])) {


       // API Settings
       update_option('wss_api_mode',$_REQUEST['wss_api_mode']);

       update_option('wss_sandbox_url',$_REQUEST['wss_sandbox_url']);
       update_option('wss_sandbox_api_username',$_REQUEST['wss_sandbox_api_username']);
       update_option('wss_sandbox_api_client_id',$_REQUEST['wss_sandbox_api_client_id']);
       update_option('wss_sandbox_api_secret',$_REQUEST['wss_sandbox_api_secret']);
       update_option('wss_sandbox_api_oauth_token',$_REQUEST['wss_sandbox_api_oauth_token']);
       update_option('wss_sandbox_api_client_id',$_REQUEST['wss_sandbox_api_client_id']);

       update_option('wss_live_url',$_REQUEST['wss_live_url']);
       update_option('wss_live_api_username',$_REQUEST['wss_live_api_username']);
       update_option('wss_live_api_client_id',$_REQUEST['wss_live_api_client_id']);
       update_option('wss_live_api_secret',$_REQUEST['wss_live_api_secret']);
       update_option('wss_live_api_oauth_token',$_REQUEST['wss_live_api_oauth_token']);
       update_option('wss_live_api_client_id',$_REQUEST['wss_live_api_client_id']);

  
       $sd_massege = '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> <p><strong>Settings saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

         
    }

	// API Settings
	$wss_api_mode = get_option('wss_api_mode');

	$wss_sandbox_url = get_option('wss_sandbox_url');
	$wss_sandbox_api_username = get_option('wss_sandbox_api_username');
	$wss_sandbox_api_client_id = get_option('wss_sandbox_api_client_id');
	$wss_sandbox_api_secret = get_option('wss_sandbox_api_secret');
	$wss_sandbox_api_oauth_token = get_option('wss_sandbox_api_oauth_token');
	$wss_sandbox_api_client_id = get_option('wss_sandbox_api_client_id');

	$wss_live_url = get_option('wss_live_url');
	$wss_live_api_username = get_option('wss_live_api_username');
	$wss_live_api_client_id = get_option('wss_live_api_client_id');
	$wss_live_api_secret = get_option('wss_live_api_secret');
	$wss_live_api_oauth_token = get_option('wss_live_api_oauth_token');
	$wss_live_api_client_id = get_option('wss_live_api_client_id');

	$lin_key = get_option('stuart_license_key');
	$stuart_license_key_expiry = get_option('stuart_license_key_expiry');
	$current_date = date("Y-m-d H:i:s");
	if($lin_key!= '' && $current_date < $stuart_license_key_expiry ){
		?>
		<script type="text/javascript">
			jQuery( document ).ready(function() {
				jQuery( ".st-module" ).show();
			});

		</script>
		<?php
	}
		/*if($current_date > $stuart_license_key_expiry){
			update_option('stuart_license_key_expiry', ''); 
		}*/
?>

<div class="wrap st-module" style="display:none;">

	<h1>Woocommerce Stuart Shipping</h1>
	<?php echo $sd_massege; ?>
	<form method="post" action="" novalidate="novalidate">


	    <table class="form-table">
		<tr>
		<th scope="row"><label for="wss_api_mode">API Mode</label></th>
		<td>
		<input type="radio" name="wss_api_mode" value="Sandbox" <?php echo $wss_api_mode != '' && $wss_api_mode == "Sandbox" ? 'checked="checked"' : ''; ?> > Sandbox<br>
		<input type="radio" name="wss_api_mode" value="Live" <?php echo $wss_api_mode != '' && $wss_api_mode == "Live" ? 'checked="checked"' : ''; ?> > Live
		
		</tr>
		</table>

		<h2 class="title">Sandbox API</h2>

		<table class="form-table">

			
			<tr>
			<th scope="row"><label for="wss_sandbox_url">End Point URL</label></th>
			<td>
			<input name="wss_sandbox_url" type="text" id="wss_sandbox_url" value="<?php echo $wss_sandbox_url; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_sandbox_api_username">API Username</label></th>
			<td>
			<input name="wss_sandbox_api_username" type="text" id="wss_sandbox_api_username" value="<?php echo $wss_sandbox_api_username; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_sandbox_api_client_id">API Client ID</label></th>
			<td>
			<input name="wss_sandbox_api_client_id" type="text" id="wss_sandbox_api_client_id" value="<?php echo $wss_sandbox_api_client_id; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_sandbox_api_secret">API Secret</label></th>
			<td>
			<input name="wss_sandbox_api_secret" type="text" id="wss_sandbox_api_secret" value="<?php echo $wss_sandbox_api_secret; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_sandbox_api_oauth_token">API OAuth token</label></th>
			<td>
			<input name="wss_sandbox_api_oauth_token" type="text" id="wss_sandbox_api_oauth_token" value="<?php echo $wss_sandbox_api_oauth_token; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_sandbox_api_client_id">Client ID</label></th>
			<td>
			<input name="wss_sandbox_api_client_id" type="text" id="wss_sandbox_api_client_id" value="<?php echo $wss_sandbox_api_client_id; ?>" class="regular-text">
			</td>
			</tr>

		</table>

		<h2 class="title">Live API</h2>

		<table class="form-table">

			<tr>
			<th scope="row"><label for="wss_live_url">End Point URL</label></th>
			<td>
			<input name="wss_live_url" type="text" id="wss_live_url" value="<?php echo $wss_live_url; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_live_api_username">API Username</label></th>
			<td>
			<input name="wss_live_api_username" type="text" id="wss_live_api_username" value="<?php echo $wss_live_api_username; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_live_api_client_id">API Client ID</label></th>
			<td>
			<input name="wss_live_api_client_id" type="text" id="wss_live_api_client_id" value="<?php echo $wss_live_api_client_id; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_live_api_secret">API Secret</label></th>
			<td>
			<input name="wss_live_api_secret" type="text" id="wss_live_api_secret" value="<?php echo $wss_live_api_secret; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_live_api_oauth_token">API OAuth token</label></th>
			<td>
			<input name="wss_live_api_oauth_token" type="text" id="wss_live_api_oauth_token" value="<?php echo $wss_live_api_oauth_token; ?>" class="regular-text">
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="wss_live_api_client_id">Client ID</label></th>
			<td>
			<input name="wss_live_api_client_id" type="text" id="wss_live_api_client_id" value="<?php echo $wss_live_api_client_id; ?>" class="regular-text">
			</td>
			</tr>

		</table>

	  
		<p class="submit"><input type="submit" name="setting_submit" id="setting_submit" class="button button-primary" value="Save Changes"></p>

	</form>

</div>





