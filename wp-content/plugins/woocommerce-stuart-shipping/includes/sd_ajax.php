<?php
require_once("../../../../wp-load.php");


$post = get_post($_REQUEST['shipping_id']);
if(isset($post)){
	if($post->post_type=='was'){

		$city_current_time = getCurrentTime(strtolower($post->post_title));
		  
		$matchs_details = get_post_meta( $post->ID, '_was_shipping_method', true );

		$shipping_start_time = $matchs_details['shipping_start_time'];

		// Calculating end time as per Minimum order prepration time (In minute)
		$endCityTime   = strtotime($matchs_details['shipping_end_time']);
		$min_order_time = get_option("min_order_time");
		$min_order_time_unix = (60 * $min_order_time);
		$delivery_final_time = $endCityTime - $min_order_time_unix;
		$shipping_end_time = date('g:ia', $delivery_final_time);

		$data['shipping_start_time'] = $shipping_start_time;
		$data['shipping_end_time'] = $shipping_end_time;
		$data['city_current_time'] = $city_current_time;
		$data['status'] = true;
	} 
} else {
	$data['status'] = false;
}


echo json_encode($data);



function getCurrentTime($city){

	if($city == 'paris' || $city == 'lyon' || $city == 'madrid' || $city == 'barcelona'){
		$a = new DateTime('now', new DateTimeZone('Europe/Paris'));
	}
	if($city == 'london'){
		$a = new DateTime('now', new DateTimeZone('Europe/London'));
	}
	$city_current_time = $a->format('g:ia');
	return $city_current_time;
}



?>