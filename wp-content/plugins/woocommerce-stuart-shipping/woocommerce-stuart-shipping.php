<?php
/**
Plugin Name: Stuart Delivery
Description: Woocommerce - Stuart Integration Plugin.
Author: Marketingmindz
Author URI: http://marketingmindz.com
Version: 1.0.0
**/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Check if WooCommerce is active
if ( ! WooCommerce_Stuart_Shipping::is_woocommerce_active() ) {

	add_action( 'admin_notices', 'stuart_delivery_render_wc_inactive_notice' );
	return;
}

/**
 * Renders a notice when WooCommerce in not activate
 *
 */
function stuart_delivery_render_wc_inactive_notice() {

	$message = sprintf(
		/* translators: %1$s and %2$s are <strong> tags. %3$s and %4$s are <a> tags */
		__( '%1$sStuart Delivery is inactive%2$s as it requires WooCommerce.', 'stuart-delivery' ),
		'<strong>',
		'</strong>'
	);
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Copyright Jeroen Sormani
 * Class WooCommerce_Stuart_Shipping
 *
 * Main WAS class, add filters and handling all other files
 *
 * @class		WooCommerce_Stuart_Shipping
 * @version		1.0.0
 * @author		Jeroen Sormani
 */
class WooCommerce_Stuart_Shipping {


	/**
	 * Version.
	 *
	 * @since 1.0.1
	 * @var string $version Plugin version number.
	 */
	public $version = '1.0.13';


	/**
	 * File.
	 *
	 * @since 1.0.5
	 * @var string $file Plugin __FILE__ path.
	 */
	public $file = __FILE__;


	/**
	 * Instance of WooCommerce_Stuart_Shipping.
	 *
	 * @since 1.0.1
	 * @access private
	 * @var object $instance The instance of WAS.
	 */
	private static $instance;


	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Check if WooCommerce is active
		/*if ( ! function_exists( 'is_plugin_active_for_network' ) ) :
			require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
		endif;

		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) :
			if ( ! is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) ) :
				return;
			endif;
		endif;*/

		// Initialize plugin parts
		$this->init();

		do_action( 'woocommerce_stuart_shipping_init' );

	}

	/**
	 * Checks if WooCommerce is active
	 */
	public static function is_woocommerce_active() {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
	}


	/**
	 * Instance.
	 *
	 * An global instance of the class. Used to retrieve the instance
	 * to use on other files/plugins/themes.
	 *
	 * @since 1.0.1
	 *
	 * @return object Instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$instance ) ) :
			self::$instance = new self();
		endif;

		return self::$instance;

	}


	/**
	 * Init.
	 *
	 * Initialize plugin parts.
	 *
	 * @since 1.0.1
	 */
	public function init() {

			

		/* Add style for front-end */
		add_action( 'wp_enqueue_scripts', array(&$this, 'stuart_delivery_front_style'), 20);
		add_action( 'admin_enqueue_scripts', array(&$this, 'stuart_delivery_admin_style'));

		
		

		// Load textdomain
		$this->load_textdomain();

		/**
		 * Require matching conditions hooks.
		 */
		require_once plugin_dir_path( __FILE__ ) . '/includes/class-was-match-conditions.php';
		$this->matcher = new WAS_Match_Conditions();

		/**
		 * Post Type class
		 */
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-was-post-type.php';
		$this->post_type = new WAS_Post_Type();

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) :

			/**
			 * Load ajax methods
			 */
			require_once plugin_dir_path( __FILE__ ) . '/includes/class-was-ajax.php';
			$this->ajax = new WAS_Ajax();

		endif;

		if ( is_admin() ) :

			/**
			 * Admin class.
			 */
			require_once plugin_dir_path( __FILE__ ) . '/includes/admin/class-was-admin.php';
			$this->admin = new WAS_Admin();

			require_once plugin_dir_path( __FILE__ ) . '/includes/admin/admin-functions.php';

		endif;


		

		add_action( 'init', array( $this, 'wss_init' ) );

		add_action( 'woocommerce_new_order', array( $this, 'wss_create_new_job_after_create_order' ) );
		$lin_key = get_option('stuart_license_key');
		$stuart_license_key_expiry = get_option('stuart_license_key_expiry');
		$current_date = date("Y-m-d H:i:s");
		if($lin_key!= '' && $current_date < $stuart_license_key_expiry ){
		// Initialize shipping method class
			add_action( 'woocommerce_shipping_init', array( $this, 'was_shipping_method' ) );
		}
		// Add shipping method
		add_action( 'woocommerce_shipping_methods', array( $this, 'add_shipping_method_class' ) );

		

		//add_action( 'woocommerce_checkout_after_customer_details', array( $this, 'my_custom_checkout_field' ) );

		
		add_action( 'woocommerce_review_order_before_payment', array( $this, 'my_custom_checkout_field' ) );


	}




	


	public function wss_init() {


		$args = array(
	      'public' => true,
	      'label'  => 'WSS Jobs',
	      'show_in_menu' => false,
	    );
	    register_post_type( 'wss_jobs', $args );




	}


	/**
	 *  Enqueue stylesheet For Front End
	 *
	**/

	public function stuart_delivery_front_style() {
		//wp_deregister_script('wc-checkout'); 
		wp_enqueue_style('sd_time_picker_style',plugin_dir_url( __FILE__ ).'assets/admin/css/timepicker.css');			
		wp_enqueue_script('sd_custom_script',plugin_dir_url( __FILE__ ).'assets/admin/js/custom.js');
		wp_enqueue_script('sd_time_picker_script', plugin_dir_url( __FILE__ ).'assets/admin/js/timepicker.js');
		//wp_enqueue_style('sd_ui_style',plugin_dir_url( __FILE__ ).'assets/admin/css/jquery-ui.css');			
		//wp_enqueue_script('sd_ui_script',plugin_dir_url( __FILE__ ).'assets/admin/js/jquery-ui.js');
		//wp_enqueue_script('wc-checkout', plugin_dir_url( __FILE__ ).'assets/admin/js/wss-checkout.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);
		


	}
	public function stuart_delivery_admin_style() {
		
		//wp_enqueue_style('sd_ui_style',plugin_dir_url( __FILE__ ).'assets/admin/css/jquery-ui.css');	
		//wp_enqueue_script('sd_ui_script',plugin_dir_url( __FILE__ ).'assets/admin/js/jquery-ui.js');
		wp_enqueue_script('sd_time_picker_script', plugin_dir_url( __FILE__ ).'assets/admin/js/timepicker.js');
		wp_enqueue_style('sd_time_picker_style',plugin_dir_url( __FILE__ ).'assets/admin/css/timepicker.css');


	}
	
	/*public function my_shipping_time( $checkout ) {
		
	}*/

	public function my_custom_checkout_field( $checkout ) {


		$wss_settings = get_option("woocommerce_stuart_shipping_settings");
		$ajax_path = plugin_dir_url( __FILE__ ) . 'includes/sd_ajax.php';
		if($wss_settings['enabled'] == "yes"){
			woocommerce_form_field("sd_delivery_time", array(
			    'type'              => 'text',
			    //'class'             => array('form-row-wid address-field timeDropDown'),
			    'class'             => array('form-row-wide validate-required'),
			    'label'             => __('Delivery Time', 'woocommerce-stuart-shipping'),
			    'placeholder'       => __('Select delivery time', 'woocommerce-stuart-shipping'),

			) );

			woocommerce_form_field("wss_checkout_ajax_path", array(
			    'type'              => 'text',
			    'class'             => array('form-row-wide'),

			), $ajax_path );
			
		}

	}

	

 




	public function wss_create_new_job_after_create_order($order_id){
		
		session_start();
		global $wpdb;
		$sql2 = "SELECT * FROM " . $wpdb->prefix . "woocommerce_order_items WHERE order_item_type = 'shipping' AND order_id = " . $order_id;
		$results = $wpdb->get_results($sql2);

		$shipping = $results->order_item_name;
		//update_option("wss_order_shipping", $shipping);
		$shipping = "Stuart Shipping";
		//update_option("wss_sssssss_sql", $sql2);
		/*update_option("wss_sssssss_result", count($results));
		update_option("wss_sssssss_order_id", $order_id);*/
		if($shipping == "Stuart Shipping"){
		    
		$wss_last_quote_id = $_SESSION['wss_last_quote_id'];
		update_option("wss_last_quote_id_session", $wss_last_quote_id);
		
		$wss_api_mode = get_option("wss_api_mode");
		if($wss_api_mode == "Sandbox"){
			$end_point_job = get_option("wss_sandbox_url") . "/v1/jobs"; // For Job
			$access_token = get_option("wss_sandbox_api_oauth_token");
		} else {
			$end_point_job = get_option("wss_live_url") . "/v1/jobs"; // For Job
			$access_token = get_option("wss_live_api_oauth_token");
		}   
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $end_point_job);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "jobQuoteId=" . $wss_last_quote_id);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/x-www-form-urlencoded",
		  "Authorization: Bearer " . $access_token
		));

		$response = curl_exec($ch);
		curl_close($ch);
		$response_decoded = json_decode($response, true);

		update_option("wss_job_array", $response_decoded);
		

		$job_id = $response_decoded['id'];
		$transportType = $response_decoded['transportType']['name'];
		$distance = $response_decoded['finalJobPrice']['jobQuote']['distance'];
		$duration = $response_decoded['finalJobPrice']['jobQuote']['duration'];
		$dest_street = $response_decoded['destinationPlace']['address']['street'];
		$dest_postcode = $response_decoded['destinationPlace']['address']['postcode'];
		$destination = $dest_street . ", " . $dest_postcode;
		$trackingUrl = $response_decoded['trackingUrl'];
		$time = current_time('mysql');
		$user_info = get_userdata(1);

		// Create post object
		$job_post = array(
		  'post_title'    => $job_id,
		  'post_status'   => 'publish',
		  'post_type' => 'wss_jobs'
		);
		$jid = wp_insert_post( $job_post );
		update_post_meta( $jid, "wc_order_id", $order_id);

		if($job_id != ""){

			$content = array(
			'<strong>Stuart delivery job created</strong>
				<table><tbody>
					<tr>
						<th scope="row">Transport Type:</th>
						<td>'. $transportType .'</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th scope="row">Distance:</th>
						<td>'. $distance .' KM</td>
					</tr>
					<tr>
						<th scope="row">Duration:</th>
						<td>'. $duration .' Min</td>
					</tr>
					<tr>
						<th scope="row">Destination:</th>
						<td>'. $destination .'</td>
					</tr>
				</tfoot></table>',
				'<strong>Stuart Tracking</strong><br>Click <a target="_blank" href="'. $trackingUrl .'">here</a> to track your Stuart Delivery.'
				);

			
			foreach($content as $key => $value){
				$note_data = array(
				    'comment_post_ID' => $order_id,
				    'comment_author' => $user_info->display_name,
				    'comment_author_email' => $user_info->user_email,
				    'comment_author_url' => '',
				    'comment_content' => $value,
				    'comment_type' => 'order_note',
				    'comment_parent' => 0,
				    'user_id' => 0,
				    'comment_date' => $job_createdAt,
				    'comment_approved' => 1,
				);
				$comment_id = wp_insert_comment($note_data);
				update_comment_meta( $comment_id, "is_customer_note", 1 );
			}

			
		}
		unset($_SESSION['wss_last_quote_id']); 



		}
		

		

	}



	




	/**
	 * Textdomain.
	 *
	 * Load the textdomain based on WP language.
	 *
	 * @since 1.0.1
	 */
	public function load_textdomain() {

		// Load textdomain
		load_plugin_textdomain( 'woocommerce-stuart-shipping', false, basename( dirname( __FILE__ ) ) . '/languages' );

	}


	/**
	 * Add shipping method.
	 *
	 * Configure and add all the shipping methods available.
	 *
	 * @since 1.0.0
	 */
	public function was_shipping_method() {

		/**
		 * Advanced shipping method
		 */
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-was-method.php';
		//$this->was_method = new WAS_Advanced_Shipping_Method();
		$GLOBALS['Stuart_Delivery'] = new WAS_Advanced_Shipping_Method();

	}


	/**
	 * Add shipping method.
	 *
	 * Add configured methods to available shipping methods.
	 *
	 * @since 1.0.0
	 */
	public function add_shipping_method_class( $methods ) {

		if ( class_exists( 'WAS_Advanced_Shipping_Method' ) ) :
			$methods[] = 'WAS_Advanced_Shipping_Method';
		endif;

		return $methods;

	}


}


/**
 * The main function responsible for returning the WooCommerce_Stuart_Shipping object.
 *
 * Use this function like you would a global variable, except without needing to declare the global.
 *
 * Example: <?php WooCommerce_Stuart_Shipping()->method_name(); ?>
 *
 * @since 1.0.1
 *
 * @return object WooCommerce_Stuart_Shipping class object.
 */
if ( ! function_exists( 'WooCommerce_Stuart_Shipping' ) ) :

	function WooCommerce_Stuart_Shipping() {
		return WooCommerce_Stuart_Shipping::instance();

	}


endif;

// Backwards compatibility
if ( ! function_exists( 'WAS' ) ) :
	function WAS() {
		return WooCommerce_Stuart_Shipping();

	}


endif;

WooCommerce_Stuart_Shipping();
