<?php
/**
Plugin Name: WP popup notification
Description: Display popup notification on bottom of the website.
Author: Vinod Kumawat
Version: 1.0.0
**/
if ( ! defined( 'ABSPATH' ) ) exit;	// Exit if accessed directly

if(!class_exists('ClassPopupNotification')) {
	class ClassPopupNotification{   		

		/**
		 * Construct The Plugin Object
		 *
		*/
		public function __construct() {
			/* Set the constants needed by the plugin. */
			add_action( 'plugins_loaded', array( &$this, 'popup_notification_constants' ), 1 );
			
			/* Load the admin files. */
			add_action( 'plugins_loaded', array( &$this, 'popup_notification_admin' ), 2 );	
			
			/* Add style for front-end */
			add_action( 'wp_enqueue_scripts', array(&$this, 'popup_notification_front_style'));

			/* Activation hook */
			register_activation_hook(__FILE__, array($this, 'popup_notification_activate'));
			
			/* Deactivation hook */
			register_deactivation_hook(__FILE__, array(&$this, 'popup_notification_deactivate'));
			
			/* Uninstall hook */
			register_uninstall_hook(__FILE__, 'popup_notification_uninstall');
		}
		
		
		/**
		 * Defines constants used by the plugin.
		 *
		*/
		function popup_notification_constants(){			
			
			/* Set constant path to the PN plugin directory. */
			define( 'POPUP_NOTIFICATION_DIR', trailingslashit(plugin_dir_path( __FILE__ )));
	
			/* Set constant path to the PN plugin URL. */
			define( 'POPUP_NOTIFICATION_URL', trailingslashit(plugin_dir_url( __FILE__ )));	

		}
		/**
		 * Loads the admin section files needed by the plugin.
		 *
		**/
		function popup_notification_admin() {
			/* Load the Files in Admin Section. */			
			require_once( POPUP_NOTIFICATION_DIR . 'admin/functions.php');
		}
		
		/**
		 *  Enqueue stylesheet For Front End
		 *
		**/
		function popup_notification_front_style() {
			wp_enqueue_style('notification_style', POPUP_NOTIFICATION_URL . 'css/notification.css');			
			wp_enqueue_script('notification_script', POPUP_NOTIFICATION_URL . 'js/notification.js');
			wp_enqueue_script('bootstrap_min', POPUP_NOTIFICATION_URL . 'js/bootstrap.min.js');
		}
		
		/**
		 *  Activate the plugin
		 *
		**/
		function popup_notification_activate() {

		}

		/**
		 *   deactivate Hook Of The Plugin
		 *
		**/		
		function popup_notification_deactivate() {
			 
		}
		/**
		 *   Uninstall Hook Of The Plugin
		 *
		**/
		function popup_notification_uninstall() {

		}
	}
}
/* Instantiate the Popup Notification class */
$ObSotre = new ClassPopupNotification();