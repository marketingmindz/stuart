<?php
add_action( 'init', 'notification_init' );
/**
 * Add custom Post Type for Notification
 */
function notification_init() {
    $labels = array(
        'name'               => _x( 'Notification', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Notification', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Notifications', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Notification', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Notification', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Notification', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Notification', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Notification', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Notifications', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Notifications', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Notifications:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No notifications found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No notifications found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'has_archive'        => true,
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'notification', $args );
}

/**
 * Create Meta Boxes For Notificatio Post Type
**/
function add_custom_meta_box_notification() {
    add_meta_box("n_location", "Location", "custom_meta_box_n_location", "notification", "normal", "high", null);
    add_meta_box("n_activity", "Activity", "custom_meta_box_n_activity", "notification", "normal", "high", null);
    add_meta_box("n_product_title", "Product Title", "custom_meta_box_n_product_title", "notification", "normal", "high", null);
    add_meta_box("n_product_url", "Product URL", "custom_meta_box_n_product_url", "notification", "normal", "high", null);
}
add_action("add_meta_boxes", "add_custom_meta_box_notification");


/**
 * Add Custom meta box for Location
**/
function custom_meta_box_n_location($object){
    global $post;
    $n_location = get_post_meta($post->ID, 'n_location', true);
?>
    <p>You can enter city, state or country.</p>
        <input type="text" value="<?php echo $n_location; ?>" name="n_location" id="n_location"  />
<?php }

/**
 * Add Custom meta box for Activity
**/
function custom_meta_box_n_activity($object){
    global $post;
    $n_activity = get_post_meta($post->ID, 'n_activity', true);
?>
    <p>You can enter what customer recently purchased. For e.g. Signed up for, Just purchase etc.</p>
        <input type="text" value="<?php echo $n_activity; ?>" name="n_activity" id="n_activity"  />
<?php }

/**
 * Add Custom meta box for Product Title
**/
function custom_meta_box_n_product_title($object){
    global $post;
    $n_product_title = get_post_meta($post->ID, 'n_product_title', true);
?>
    <p>You can enter Product name.</p>
        <input type="text" value="<?php echo $n_product_title; ?>" name="n_product_title" id="n_product_title"  />
<?php }

/**
 * Add Custom meta box for Product URL
**/
function custom_meta_box_n_product_url($object){
    global $post;
    $n_product_url = get_post_meta($post->ID, 'n_product_url', true);
?>
    <p>You can enter Product name. For e.g. http://www.example.com</p>
        <input type="text" value="<?php echo $n_product_url; ?>" name="n_product_url" id="n_product_url"  />
<?php }


/**
 * Save post hook
**/
function save_post_type_meta($post_id, $post) {
    $post_type = $post->post_type;
    $post_id = $post->ID;
    //prd($_REQUEST);
    
    #Save the Metabox Data for notification post type
    if( $post_type == 'notification' ){
        if(isset($_REQUEST['n_location'])){
            update_post_meta($post_id, 'n_location', $_REQUEST['n_location']);
        }
        if(isset($_REQUEST['n_activity'])){
            update_post_meta($post_id, 'n_activity', $_REQUEST['n_activity']);
        }
        if(isset($_REQUEST['n_product_title'])){
            update_post_meta($post_id, 'n_product_title', $_REQUEST['n_product_title']);
        }
        if(isset($_REQUEST['n_product_url'])){
            update_post_meta($post_id, 'n_product_url', $_REQUEST['n_product_url']);
        }
    }
}

add_action('save_post', 'save_post_type_meta', 1, 2); // save the custom fields

/**
 * Display custom columns on notification listing page
**/
/* Add action for custom column addition  */            
add_filter('manage_notification_posts_columns',  'notification_column');

/* Add action for manage custom column */   
add_action('manage_notification_posts_custom_column', 'notification_custom_column', 10, 2 );

/* Add action for manage sortable custom column */
add_filter('manage_edit-notification_sortable_columns', 'notification_register_sortable');

function notification_column($columns) {
    $date = $columns['date'];
    unset($columns['date']);
    $columns['n_location']  = 'Location';
    $columns['n_activity']  = 'Activity';
    $columns['n_product_title']  = 'Product Title';
    $columns['n_product_image']  = 'Product Image';
    $columns['date'] = $date;
    return $columns;
}
        
function notification_custom_column($column_name,$post_id ) {
    if($column_name == 'n_location') {
        echo $n_location = get_post_meta( $post_id, 'n_location', true );
    }
    if($column_name == 'n_activity') {
        echo $n_activity = get_post_meta( $post_id, 'n_activity', true );
    }
    if($column_name == 'n_product_title') {
        echo $n_product_title = get_post_meta( $post_id, 'n_product_title', true );
    }
    if($column_name == 'n_product_image') {
        $n_image_url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
        echo $n_image = "<img src='" . $n_image_url . "' width='70'>";
    }
}

function notification_register_sortable($columns) {
    $columns['n_location']  = 'n_location';
    $columns['n_activity']  = 'n_activity';
    $columns['n_product_title']  = 'n_product_title';
    $columns['n_product_image']  = 'n_product_image';
    return $columns;

}

/**
 * Change plac holder for post title
**/
function notification_change_title_text( $title ){
     $screen = get_current_screen();
 
     if  ( 'notification' == $screen->post_type ) {
          $title = 'Enter someone name or Customer name';
     }
 
     return $title;
}
 
add_filter( 'enter_title_here', 'notification_change_title_text' );

/**
 * Setting page
**/

add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
function wpdocs_register_my_custom_menu_page() {
    add_submenu_page('edit.php?post_type=notification', 'Settings', 'Settings', 'manage_options', 'n-settings', 'n_settings_callback');
              
}

function n_settings_callback() {

    require_once( POPUP_NOTIFICATION_DIR . 'admin/settings.php' );
}


add_action('wp_footer', 'wp_notification_popup_script');
function wp_notification_popup_script() {

$notifications = get_posts( array(
    'posts_per_page' => -1,
    'post_type'  => 'notification',
    'orderby'    => 'rand'
) );
$n_time_interval = get_option("n_time_interval");
$timeInterval = $n_time_interval == "" ? 10 * 1000 : $n_time_interval * 1000;

    if(count($notifications) > 0){
        ?>
        <script type="text/javascript">
            jQuery(function() {
                <?php
                $i = 1;
                foreach ($notifications as $notification) {

                    $n_image_url = wp_get_attachment_url( get_post_thumbnail_id($notification->ID) );
                    $n_customer_name = $notification->post_title;                       
                    $n_location = get_post_meta($notification->ID, "n_location", true);
                    $n_activity = get_post_meta($notification->ID, "n_activity", true);
                    $n_product_title = get_post_meta($notification->ID, "n_product_title", true);
                    $n_product_url = get_post_meta($notification->ID, "n_product_url", true);

                    $SP = " ";
                    $content = "<div class='notification-container'>";
                    $content = "<div class='img-wraper'>";
                    $content .= "<img src='" . $n_image_url . "'>";
                    $content .= "</div>"; 
                    $content .= "<div class='content-wraper'>";  
                    $content .= $n_customer_name . " in " . $n_location . $SP . $n_activity . $SP;
                    $content .= "<a href='". $n_product_url ."' target='_blank'>". $n_product_title ."</a>";
                    $content .= "</div>"; 
                    $content .= "</div>"; 

                    ?>
                    setTimeout(function() {
                        jQuery.bootstrapGrowl( "<?php echo $content; ?>", {                        
                          type: 'success', // (null, 'info', 'error', 'success')
                          offset: {from: 'bottom', amount: 20}, // 'top', or 'bottom'
                          align: 'left', // ('left', 'right', or 'center')
                          width: 380, // (integer, or 'auto')
                          delay: 3000,
                        });
                    }, <?php echo $timeInterval * $i; ?>);
                    <?php 
                    $i++;               
                }
                ?>
            });
        </script>
        <?php
    }

}
                
          


