<?php
/**
 * Setting section of Notification Plugin.
 * 
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if (isset($_POST['notification_settings'])) {

  //print_r($_REQUEST);
    
   update_option('n_time_interval', $_REQUEST['n_time_interval'] );

     echo $lc_massege = '<div class="updated settings-error notice is-dismissible" id="setting-error-settings_updated"> 
<p><strong>Settings saved.</strong></p><button class="notice-dismiss" type="button"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';


}
?>

    
<div class="wrap">
    <h2><?php echo __('Popup notification settings', 'notification'); ?></h2>

    <?php //echo $lc_massege; ?>


    <form novalidate="novalidate" action="" method="post" id="email_form">
        <table class="form-table" width="800">
            
           <tr>
                <th><?php echo __('Time interval', 'notification'); ?>
                 </th>
                <td><input type="text" placeholder="For e.g. 15" class="regular-text" name="n_time_interval" value="<?php echo get_option('n_time_interval') ?>">
                <p class="description">Please enter time interval in seconds (By default it 10) </p>
                </td>
            </tr>
           
             
            <tr>
                <td>
                <input type="submit" value="Save Setting" class="button button-primary" id="notification_settings" name="notification_settings" >
                </td>
            </tr>
        </table>
    </form>
</div>
   
